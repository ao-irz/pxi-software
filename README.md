# README #

Каждый проект нужно класть в отдельную папку, чтобы в root каталоге репозитория были только каталоги проектов


### Структура проектов ###

Пока вырисовываются 5 условно независимых проектов:    
 - Редактор Циклограмм    
 - Управлюящий сервис PXI    
 - Пользовательский интерфейс (UI)  PXI    
 - Библиотека управения платами (VISA based library)    
 - ПО ПЛК

### Visa ###
Можно взять тут:    
https://www.ni.com/ru-ru/support/downloads/drivers/download.ni-visa.html#346210    
Необходимо устапновить базовый комплкт и потом их менеждера пакетов добавить
еще модуль работы с генераторами и модуль работы с источниками питания (niFgenerator, niDCpower)
Можно управлять установленными пакетами из менеджера пакетов NI, который можно запустить из
стандартного приложения Windows (Установленные компоненты)

Иметь ввиду, что NI packet manager устанавливает одинаковые lib файлы и в "Program Files" и в Program Files (x86)
Но не все модули это делают. (HSDIO устанавливает только в одно место)
Однако в каждом из каталогов имеется и 32-ая и 64-ая версия файлов.
Запись $$(IVIROOTDIR32)/lib_x64 выглядит глупо, но тем не менее...

## Required NI Components ##

LabVIEW Runtime (32-bit) National Instruments Programming Environments 2019 SP1 f1 LabVIEW Runtime (32-bit) National Instruments Programming Environments 2018 SP1 f4 LabVIEW Runtime (64-bit) National Instruments Programming Environments 2018 SP1 f4 LabWindows/CVI Shared Runtime National Instruments Runtime 2019
NI Device Monitor National Instruments Utilities 19.0.0
NI I/O Trace National Instruments Utilities 20.0.0
NI License Manager National Instruments Utilities 4.6.0
NI Measurement & Automation Explorer National Instruments Utilities 20.0.0
NI Package Manager National Instruments Utilities 20.5.0
NI Update Service National Instruments Utilities 19.0.0
NI Web Server National Instruments Utilities 20.0.0
NI-DAQmx National Instruments Drivers 20.1.0
NI-DCPower National Instruments Drivers 20.1.0
NI-FGEN National Instruments Drivers 20.0.0
NI-HSDIO National Instruments Drivers 20.0.0
NI-SWITCH National Instruments Drivers 20.0.0
NI-VISA National Instruments Drivers 20.0.0
