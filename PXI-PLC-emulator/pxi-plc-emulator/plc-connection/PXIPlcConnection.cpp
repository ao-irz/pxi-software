#include "PXIPlcConnection.h"
#include <QVector>
#include <QVariant>
#include <QRandomGenerator>

PXIPlcConnection::PXIPlcConnection(QObject *parent) :
    QObject(parent),
    m_hostAddress(PLC_DESTINATION_ADDRESS),
    m_udpSocket(new QUdpSocket(this))
{
    //    for(int i=0; i<PLC_STATE_FIELDS_NUMBER; ++i)
    //        m_statesPackage.append( QVariant::fromValue(StateData(i));

    //m_statesPackage.replace(8, QVariant::fromValue(StateData(16)));

    m_statesPackage.append(QVariant::fromValue(StateData("Код аварии камеры", 34, 0)));

    m_statesPackage.append(QVariant::fromValue(StateData("Текущая темп.", 38, 96.6)));
    m_statesPackage.append(QVariant::fromValue(StateData("Скольз.уставка темп.", 46, 96.6)));
    m_statesPackage.append(QVariant::fromValue(StateData("Режим", 50, 1)));
    m_statesPackage.append(QVariant::fromValue(StateData("Темп. уставка", 52, 123.4)));
    m_statesPackage.append(QVariant::fromValue(StateData("Огр. скорости изм.темп.", 56, 0.0)));

    m_statesPackage.append(QVariant::fromValue(StateData("Темп. стойки №1", 258, 56.3)));
    m_statesPackage.append(QVariant::fromValue(StateData("Темп. стойки №2", 262, 43.3)));
    m_statesPackage.append(QVariant::fromValue(StateData("Темп. стойки №3", 266, 33.7)));
    m_statesPackage.append(QVariant::fromValue(StateData("Темп. стойки №4", 270, 59.9)));
    m_statesPackage.append(QVariant::fromValue(StateData("Темп. стойки №5", 274, 56.3)));
    m_statesPackage.append(QVariant::fromValue(StateData("Темп. стойки №6", 278, 43.3)));
    m_statesPackage.append(QVariant::fromValue(StateData("Темп. стойки №7", 282, 33.7)));
    m_statesPackage.append(QVariant::fromValue(StateData("Темп. стойки №8", 286, 59.9)));

    m_statesPackage.append(QVariant::fromValue(StateData("Датчик №1", 290, 1)));
    m_statesPackage.append(QVariant::fromValue(StateData("Датчик №2", 292, 1)));
    m_statesPackage.append(QVariant::fromValue(StateData("Датчик №3", 294, 1)));
    m_statesPackage.append(QVariant::fromValue(StateData("Датчик №4", 296, 1)));
    m_statesPackage.append(QVariant::fromValue(StateData("Датчик №1", 298, 1)));
    m_statesPackage.append(QVariant::fromValue(StateData("Датчик №2", 300, 1)));
    m_statesPackage.append(QVariant::fromValue(StateData("Датчик №3", 302, 1)));
    m_statesPackage.append(QVariant::fromValue(StateData("Датчик №4", 304, 1)));

    m_statesPackage.append(QVariant::fromValue(StateData("Средн. темп. в стойке", 322, 44.1)));

    m_statesPackage.append(QVariant::fromValue(StateData("Частота вент-ра", 338, 1378.1)));
    m_statesPackage.append(QVariant::fromValue(StateData("Авария вентилятора", 342, 1)));
    m_statesPackage.append(QVariant::fromValue(StateData("UPS (ИБП)", 378, 0xFFFF)));

    m_udpSocket->bind(QHostAddress::Any, PLC_DESTINATION_PORT);
    connect(m_udpSocket, &QUdpSocket::readyRead, this, &PXIPlcConnection::socketHandler);
}

void PXIPlcConnection::setVariantValue(int index, QVariant value)
{
    if (value.toString().contains("."))
        ((StateData *)(m_statesPackage[index].data()))->m_variantValue = value.toDouble();
    else
        ((StateData *)(m_statesPackage[index].data()))->m_variantValue = value.toInt();
    emit statesPackageChanged(m_statesPackage);
}

void PXIPlcConnection::logStatesPackage()
{
    for(const auto & a : m_statesPackage)
        qDebug() << a.value<StateData>().m_variantValue.type();
}

void PXIPlcConnection::socketHandler()
{
    while (m_udpSocket->hasPendingDatagrams()) {
        QNetworkDatagram datagram = m_udpSocket->receiveDatagram();
        switch (datagram.data().size()) {

        case PLC_STATE_REQUEST_LENGTH:
            stateHandler(datagram);
            break;
        case PLC_CONTROL_REQUEST_LENGTH:
            controlHandler(datagram);
            break;
        case PLC_PARAMETERS_REQUEST_LENGTH:
            paramenterHandler(datagram);
            break;
        default:
            qDebug() << "Error: wrong pachage length ";
        }
    }
}

void PXIPlcConnection::stateHandler(const QNetworkDatagram & datagram)
{   
    qint16 cmd = 0;
    memcpy(&cmd, (const void *)&datagram.data().data()[0], sizeof(qint16));
    if (cmd != 1) {
        qDebug() << "Error: wrong command";
        return;
    }

    QByteArray stateArray(PLC_STATE_RESPONSE_LENGHT, 0x00);

    for(const auto & cell : m_statesPackage) {
        StateData currentCell = cell.value<StateData>();

        switch (currentCell.m_variantValue.type()) {

        case QVariant::Double: {
            float f = currentCell.m_variantValue.toFloat();
            memcpy(&stateArray.data()[currentCell.m_address], (const void *)&f, sizeof(float));
        } break;

        case QVariant::Int: {
            int ui = currentCell.m_variantValue.toUInt();
            memcpy(&stateArray.data()[currentCell.m_address], (const void *)&ui, sizeof(qint16));
        } break;
        default:
            break;
        }
    }

    if (m_udpSocket->writeDatagram(datagram.makeReply(stateArray)) != PLC_STATE_RESPONSE_LENGHT) {
        qDebug() << "Error: can't send UDP state response package";
        return;
    }
}

void PXIPlcConnection::controlHandler(const QNetworkDatagram &datagram)
{
    //! Режим loop
    // Установить температуру камеры равной температурне уставки
    float temp = 0;
    memcpy(&temp, (const void *)&datagram.data().data()[14], sizeof(float));

    quint16 mode = 0;
    memcpy(&mode, (const void *)&datagram.data().data()[0], sizeof(quint16));

    for(auto & a : m_statesPackage) {
        if (a.value<StateData>().m_address == 38) {
            a = QVariant::fromValue( StateData("Текущая темп.", 38, (double)temp));
            continue;
        }
        if (a.value<StateData>().m_address == 52) {
            a = QVariant::fromValue( StateData("Темп. уставка", 52, (double)temp) );
            continue;
        }
        if (a.value<StateData>().m_address == 50) {
            a = QVariant::fromValue( StateData("Режим", 50, (quint16)mode) );
            continue;
        }
    }

    emit statesPackageChanged(m_statesPackage);
}

void PXIPlcConnection::paramenterHandler(const QNetworkDatagram &datagram)
{
    Q_UNUSED(datagram)

}
