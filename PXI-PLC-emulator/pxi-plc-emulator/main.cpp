#include <QApplication>
#include <QQmlContext>
#include <QQmlApplicationEngine>
#include "plc-connection/PXIPlcConnection.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);
    app.setOrganizationName("IRZ");
    app.setOrganizationDomain("irz.ru");
    app.setApplicationName("pxi-plc-emulator");

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("plcConnection", PXIPlcConnection::getInstance(qobject_cast<QObject *>(&app)));

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
