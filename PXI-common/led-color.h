#ifndef LEDCOLOR_H
#define LEDCOLOR_H

#include "common.h"

enum LedColor
{
    NoColor = LED_NO_COLOR,
    WhiteLed = LED_WHITE_COLOR,
    BlueLed = LED_BLUE_COLOR,
    GreenLed = LED_GREED_COLOR,
    YellowLed = LED_YELLOW_COLOR,
    RedLed = LED_RED_COLOR,
    BlinkRedLed = LED_BLINK_RED_COLOR
};

#endif // LEDCOLOR_H
