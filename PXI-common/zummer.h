#ifndef ZUMMER_H
#define ZUMMER_H

#include "common.h"

enum Zummer
{
    ZummerOff = ZUMMER_OFF,
    ZummerContinuos = ZUMMER_CONTINUOS,
    ZummerShortBlink = ZUMMER_SHORT_BLINK,
    ZummerLongBlink = ZUMMER_LONG_BLINK
};

#endif // ZUMMER_H
