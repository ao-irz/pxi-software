﻿#include <QFile>
#include <QDebug>
#include <QCoreApplication>
#include "Settings.h"

#define INI_FILE_NAME       QStringLiteral("settings.ini")

Settings::Settings(const QString &selfPath, QObject * parent) :
    QSettings(selfPath + "/" + INI_FILE_NAME, QSettings::IniFormat, parent),
    m_selfPath(selfPath),
    m_checkSum(0),
    m_settingsOk(true)
{
    checkFileCorrectness();

    QFile settingsFile(selfPath + "/" + INI_FILE_NAME);
    if (settingsFile.open(QIODevice::ReadOnly)) {
        QByteArray byteArray = settingsFile.readAll();
        m_checkSum = qChecksum(byteArray.data(), byteArray.size());
        settingsFile.close();
    }
}

Settings::~Settings()
{
    qDebug() << "Settings::~Settings()";
}

Settings::ChannelSettings Settings::channel(const QString &identifier)
{
    if (!childGroups().contains(identifier))
        return ChannelSettings();

    ChannelSettings result;
    beginGroup(identifier);

    result.number = group().toUInt();
    result.name = value("channelName", QString()).toString();
    result.url = value("url", QString()).toString();
    result.scte35 = value("scte35", QString()).toString();
    (value("type").toString() == "alsa") ? (result.type = Alsa) : (result.type = Stream);

    endGroup();
    return result;
}

Settings::ChannelSettings Settings::channel(const quint32 identifier)
{
    return channel(QString::number(identifier));
}

bool Settings::syncSettings()
{
    sync();
    checkFileCorrectness();

    QFile settingsFile(INI_FILE_NAME);
    quint16 checkSum = 0;
    if (settingsFile.open(QIODevice::ReadOnly)) {
        QByteArray byteArray = settingsFile.readAll();
        checkSum = qChecksum(byteArray.data(), byteArray.size());
        settingsFile.close();
    }

    if (m_checkSum != checkSum) {
        m_checkSum = checkSum;
        return true;
    }

    return false;
}

void Settings::checkFileCorrectness()
{
    m_settingsOk = true;

    if (status() != QSettings::NoError) {
        m_settingsOk = m_settingsOk && false;
        qDebug() << "!QSettings::NoError" << status();
    }

    m_settingsOk = m_settingsOk && (numberOfChannels() == channelsNumber() && (channelsNumber() != 0));
    if (numberOfChannels() != channelsNumber())
        qDebug() << "numberOfChannels() != channelsNumber()";

    if (channelsNumber() == 0)
        qDebug() << "channelsNumber() == 0";

    for(auto groupName : childGroups()) {
        bool ok;
        if (groupName.isEmpty() || (!groupName.toUInt(&ok)) || (!ok)) {
            m_settingsOk = m_settingsOk && false;
            qDebug() << "!ok";
        }
    }

    if (childGroups().removeDuplicates()) {
        m_settingsOk = m_settingsOk && false;
        qDebug() << "childGroups().removeDuplicates() != 0";
    }
}

