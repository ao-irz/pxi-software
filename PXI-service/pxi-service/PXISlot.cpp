#include <QJsonArray>
#include <QJsonObject>
#include "PXISlot.h"

/*! Опасения этого класса:
 * 1. Все ф-ии setXXX должны снабжаться syncState() так ка могут привести к изменению статуса на Alarm
 * 2. Все ф-ии setStateXXX должны быть с проверкой текущего состояния, ия пока не уверен, что все они эти проверки выставлены правильно
 * 3. Все значимые поля должны быть снабжены компонентом isValid, потому что железо отвечать может не сразу
 * 4. syncState() должна содержать проверки не только значимых полей и их валидности
 */

QJsonObject PXISlot::stateToJson() const
{
    QJsonObject result;
    result.insert("slotNumber", static_cast<qint32>(m_slotNumber));
    result.insert("name", m_chipsName);
    result.insert("batch", m_chipsBatch);
    result.insert("dateInput", m_chipsDateInput.toString(DB_DATETIME_FORMAT));
    result.insert("dateOutput", m_chipsDateOutput.toString(DB_DATETIME_FORMAT));
    if ((m_state == AlarmSlotState) || (m_state == HardwareErrorSlotState))
        result.insert("emergencyStopTime", m_emergencyStopTime.toString(DB_DATETIME_FORMAT));

    QJsonArray psArray;
    for (quint8 i=0; i<POWER_SUPPLY_NUMBER; i++) {
        QJsonObject psObject;
        psObject.insert("powerSupplyNumber", i);
        if (m_chipsVoltage[i].isValid) {
            psObject.insert("voltage", m_chipsVoltage[i].value);
            psObject.insert("voltageMin", m_voltageMin[i]); // привязываю к m_chipsVoltage.isValid, потому что сначала уст. допуск, а только потому пойдут значения
            psObject.insert("voltageMax", m_voltageMax[i]);
        }
        if (m_chipsCurrent[i].isValid) {
            psObject.insert("current", m_chipsCurrent[i].value);
            psObject.insert("currentMin", m_currentMin[i]); // привязываю к m_chipsCurrent.isValid, потому что сначала уст. допуск, а только потому пойдут значения
            psObject.insert("currentMax", m_currentMax[i]);
        }
        if (m_chipsVoltage[i].isValid || m_chipsCurrent[i].isValid)
            psArray.append(psObject);
    }
    result.insert("powerSupplies", psArray);

    result.insert("state", m_state);
    if (m_chipsTemperature.isValid) {
        result.insert("chipTemperature", m_chipsTemperature.value);
        result.insert("chipTemperatureMax", m_chipsTemperatureMax);
        result.insert("chipTemperatureSensorOk", /*m_chipsTemperatureSensorOk.value*/ true); //! TODO: пока совершенно не ясно, будет ли этот параметре bool или int, потому что термопару не раскачивали
    }
    if (m_generatorAmplitude.isValid) {
        result.insert("generatorAmplitude", m_generatorAmplitude.value);
        result.insert("generatorFrequency", m_generatorFrequency.value);
        result.insert("generatorDutycycle", m_generatorDutycycle.value);
        result.insert("generatorImpedance", m_generatorImpedance.value);
    }
    if (m_hsdioOk.isValid) {
        result.insert("dioOk", m_hsdioOk.value);
        result.insert("dioVoltageLogic", m_hsdioVoltageLogic);
        result.insert("dioHex", static_cast<qint32>(m_hsdioHex));
    }
    if (m_smuCurrent.isValid) {
        result.insert("smuCurrent", m_smuCurrent.value);
        result.insert("smuVoltage", m_smuVoltage.value);
    }

    return result;
}

void PXISlot::setTestTimeFrame(const QDateTime &start, const QDateTime &stop)
{
    m_chipsDateInput = start;
    m_chipsDateOutput = stop;
    m_lastDateOutput = stop;
    m_emergencyStopTime = start; // ну чему-то же оно должно равняться для базового сценария
}

void PXISlot::extendOutputTime(qint64 extendTimeMs)
{
    if (m_state == PauseSlotState)
        m_chipsDateOutput = m_lastDateOutput.addMSecs(extendTimeMs);
}

void PXISlot::setChipVoltage(quint32 nPowerSupply, double v, double min, double max)
{
    if (nPowerSupply >= POWER_SUPPLY_NUMBER)
        return;
    m_chipsVoltage[nPowerSupply].setValue(v);
    m_voltageMin[nPowerSupply] = min;
    m_voltageMax[nPowerSupply] = max;
    syncState();
}

void PXISlot::setChipCurrent(quint32 nPowerSupply, double c, double min, double max)
{
    if (nPowerSupply >= POWER_SUPPLY_NUMBER)
        return;
    m_chipsCurrent[nPowerSupply].setValue(c);
    m_currentMin[nPowerSupply] = min;
    m_currentMax[nPowerSupply] = max;
    syncState();
}

void PXISlot::disableVoltageCurrent(quint32 nPowerSupply)
{
    m_chipsCurrent[nPowerSupply].isValid = false;
    m_chipsVoltage[nPowerSupply].isValid = false;
}

void PXISlot::setChipTemperature(double t, double max)
{
    m_chipsTemperature.setValue(t);
    m_chipsTemperatureMax = max;
    syncState();
}

void PXISlot::disableTemperature()
{
    m_chipsTemperature.isValid = false;
}

void PXISlot::setGeneratorOk(bool f)
{
    m_generatorOk.setValue(f);
    syncState();
}

void PXISlot::setGeneratorInfo(double amplitude, double frequency, double dutycycle, double impedance)
{
    m_generatorAmplitude.setValue(amplitude);
    m_generatorFrequency.setValue(frequency);
    m_generatorDutycycle.setValue(dutycycle);
    m_generatorImpedance.setValue(impedance);
}

void PXISlot::disableGenerator()
{
    m_generatorAmplitude.isValid = false;
}

void PXISlot::setHsdioState(bool ok, double logic, quint32 hex)
{
    m_hsdioOk.setValue(ok);
    m_hsdioHex = hex;
    m_hsdioVoltageLogic = logic;
    syncState();
}

void PXISlot::disableHsdio()
{
    m_hsdioOk.isValid = false;
}

void PXISlot::setSmuCurrent(double c, double max)
{
    m_smuCurrent.setValue(c);
    m_smuCurrentMax = max;

    syncState();
}

void PXISlot::setSmuVoltage(double v, bool useVoltage, double min, double max)
{
    m_smuVoltage.setValue(v);
    m_smuVoltageMax = max;
    m_smuVoltageMin = min;
    m_useVoltage = useVoltage;

    if (useVoltage)
        syncState();
}

void PXISlot::disableSmu()
{
    m_smuCurrent.isValid = false;
    m_smuVoltage.isValid = false;
}

void PXISlot::completeTest()
{
    if ((m_state != TestCompletedState) && (m_state != AlarmSlotState) && (m_state != HardwareErrorSlotState)) {
        m_state = TestCompletedState;
        m_emergencyStopTime = m_chipsDateOutput; // на всякий случай
        m_logger->log(QString("Slot: %1. Test completed").arg(m_slotNumber+1));
    }
}

void PXISlot::harwareErrorTest()
{
    if (m_state == TestCompletedState)
        return;

    m_emergencyStopTime = QDateTime::currentDateTime();
    m_state = HardwareErrorSlotState;
    m_logger->log(QString("Slot: %1. HardwareError accured").arg(m_slotNumber+1));
}

void PXISlot::pauseTest()
{
    if (m_state == NormalSlotState) {
        m_state = PauseSlotState;
        m_lastDateOutput = m_chipsDateOutput;
        m_logger->log(QString("Slot: %1. Test paused ").arg(m_slotNumber+1));
    }
}

void PXISlot::resumeTest()
{
    if (m_state == PauseSlotState) {
        m_state = NormalSlotState;
        m_logger->log("PXI Slot " + QString::number(m_slotNumber+1) + " return to normal state");
    }
}

void PXISlot::syncState()
{
    //! TODO: В этом же месте, возможно, будет и возможная интерполяция/усреднение показаний измерений
    //! TODO: дополнять по мере появления новых полей, характеризующий состояние слота и определяющих его совокупное состояние

    //! то есть во время паузы данные по слоту обновляться будут, но состояние в alarm не свалится
    if ((m_state != NormalSlotState) && (m_state != WaitingForStartState))
        return;

    bool generatorAlarmCondition;
    (m_generatorOk.isValid) ? (generatorAlarmCondition = !m_generatorOk.value) :
        (generatorAlarmCondition = false);

    bool chipTemperatureAlarmCondition;
    (m_chipsTemperature.isValid) ? (chipTemperatureAlarmCondition = (m_chipsTemperature.value > m_chipsTemperatureMax)) :
        (chipTemperatureAlarmCondition = false);

    bool hsdioAlarmCondition;
    (m_hsdioOk.isValid) ? (hsdioAlarmCondition = !m_hsdioOk.value) :
        (hsdioAlarmCondition = false);

    bool smuCurrentAlarmCondition;
    bool smuVoltageAlarmCondition;
    (m_smuCurrent.isValid) ? (smuCurrentAlarmCondition = (m_smuCurrent.value > m_smuCurrentMax)) :
        smuCurrentAlarmCondition = false;
    (m_smuVoltage.isValid && m_useVoltage) ? (smuVoltageAlarmCondition = (m_smuVoltage.value < m_smuVoltageMin) || (m_smuVoltage.value > m_smuVoltageMax)) :
        smuVoltageAlarmCondition = false;


    bool voltageAlarmCondition[POWER_SUPPLY_NUMBER];
    bool currentAlarmCondition[POWER_SUPPLY_NUMBER];
    bool vCommon = false;
    bool cCommon = false;
    for (quint8 i=0; i<POWER_SUPPLY_NUMBER; i++) {
        (m_chipsVoltage[i].isValid) ? (voltageAlarmCondition[i] = (m_chipsVoltage[i].value < m_voltageMin[i]) || (m_chipsVoltage[i].value > m_voltageMax[i])) :
            (voltageAlarmCondition[i] = false);

        (m_chipsCurrent[i].isValid) ? (currentAlarmCondition[i] = (m_chipsCurrent[i].value < m_currentMin[i]) || (m_chipsCurrent[i].value > m_currentMax[i])) :
            (currentAlarmCondition[i] = false);

        vCommon = vCommon || voltageAlarmCondition[i];
        cCommon = cCommon || currentAlarmCondition[i];
        if (voltageAlarmCondition[i])
            m_logger->log(QString("Voltage alarm in PowerSupply=%1 V=%2 but Min=%3 Max=%4").arg(i).arg(m_chipsVoltage[i].value).arg(m_voltageMin[i]).arg(m_voltageMax[i]));
        if (currentAlarmCondition[i])
            m_logger->log(QString("Current alarm in PowerSupply=%1 I=%2 but Min=%3 Max=%4").arg(i).arg(m_chipsCurrent[i].value).arg(m_currentMin[i]).arg(m_currentMax[i]));
    }

    if (vCommon || cCommon || generatorAlarmCondition || chipTemperatureAlarmCondition || hsdioAlarmCondition || smuCurrentAlarmCondition || smuVoltageAlarmCondition) {
        m_emergencyStopTime = QDateTime::currentDateTime();
        m_state = AlarmSlotState;
    }
    else
        //! Пускай это место будет единственным местом, где возможно установить NormalSlotState (из паузы сюда попасть нельзя)
        m_state = NormalSlotState;
}
