QT += network sql serialport
QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

TARGET = pxiservice

include(qtservice/src/qtservice.pri)

QMAKE_LFLAGS += -static

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

DEFINES += PXI_LIBRARY

VISA_INCLUDE_DIR = $$quote($$(VXIPNPPATH)/WinNT/Include)
INCLUDEPATH += $$VISA_INCLUDE_DIR

# NI packet manager устанавливает одинаковые lib файлы и в "Program Files" и в Program Files (x86)
# Но не все модули это делают. (HSDIO устанавливает только в одно место)
# Однако в каждом из каталогов имеется и 32-ая и 64-ая версия файлов.
# Запись $$(IVIROOTDIR32)/lib_x64 выглядит глупо, но тем не менее...
# Поэтому я не вижу особлй разницы из какого каталога брать lib.

#IVI_INCLUDE_DIR = $$quote($$(IVIROOTDIR32)/Include)
##IVI_INCLUDE_DIR = $$quote($$(IVIROOTDIR64)/Include)
#IVI_LIB_DIR = $$quote($$(IVIROOTDIR32)/lib/msc)
##IVI_LIB_DIR = $$quote($$(IVIROOTDIR64)/lib_x64/msc)
IVI_INCLUDE_DIR = $$quote($$(IVIROOTDIR32)/Include)
IVI_LIB_DIR = $$quote($$(IVIROOTDIR32)/lib_x64/msc)
INCLUDEPATH += $$IVI_INCLUDE_DIR

NIDAQMX_INCLUDE_DIR = $$quote($$(NIEXTCCOMPILERSUPP)/include)
INCLUDEPATH += $$NIDAQMX_INCLUDE_DIR
NIDAQMX_LIB_DIR = $$quote($$(NIEXTCCOMPILERSUPP)/lib64/msvc)

LIBS +=-L$${IVI_LIB_DIR} -lnidcpower
LIBS +=-L$${IVI_LIB_DIR} -lnifgen
LIBS +=-L$${IVI_LIB_DIR} -lnihsdio
LIBS +=-L$${IVI_LIB_DIR} -lniswitch
LIBS +=-L$${NIDAQMX_LIB_DIR} -lNIDAQmx



SOURCES += \
    ../pxi-emulator/PxiEmulator.cpp \
    ../warm-pxi/pxi/bugs.cpp \
    ../warm-pxi/pxi/exceptions.cpp \
    ../warm-pxi/pxi/fgen.cpp \
    ../warm-pxi/pxi/power.cpp \
    ../warm-pxi/pxi/hsdio.cpp \
    ../warm-pxi/pxi/thermocouple.cpp \
    ../warm-pxi/pxi/relay.cpp \
    ../warm-pxi/pxi/pxi.cpp \
    Logger.cpp \
    PXIService.cpp \
    PXIServiceParsingEssential.cpp \
    PXIServiceParsingThermo.cpp \
    PXISlot.cpp \
    Settings.cpp \
    main.cpp \
    plc-connection/PXIPlcConnection.cpp \
    switch-connection/PXISwitchConnection.cpp \
    schedule-components/PXIScheduleBase.cpp \
    schedule-components/PXIScheduleEquipment.cpp \
    schedule-components/PXIScheduleGenerator.cpp \
    schedule-components/PXISchedulePlc.cpp \
    schedule-components/PXISchedulePowerSupply.cpp \
    #schedule-components/PXIScheduleSMU.cpp \
    schedule-components/PXIScheduleThermoCouple.cpp \
    #schedule-components/PXIScheduleDIO.cpp \
    database/PXIDb.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    ../../PXI-common/common.h \
    ../../PXI-common/led-color.h \
    ../../PXI-common/zummer.h \
    ../pxi-emulator/PxiEmulator.h \
    ../warm-pxi/pxi/bugs.h \
    ../warm-pxi/pxi/exceptions.h \
    ../warm-pxi/pxi/fgen.h \
    ../warm-pxi/pxi/power.h \
    ../warm-pxi/pxi/hsdio.h \
    ../warm-pxi/pxi/thermocouple.h \
    ../warm-pxi/pxi/relay.h \
    ../warm-pxi/pxi/pxi.h \
    ../warm-pxi/pxi/pxi_global.h \
    Logger.h \
    PXIService.h \
    PXISlot.h \
    Settings.h \
    ValueTypes.h \
    config.h \
    plc-connection/PXIPlcConnection.h \
    switch-connection/PXISwitchConnection.h \
    schedule-components/PXIScheduleBase.h \
    schedule-components/PXIScheduleEquipment.h \
    schedule-components/PXIScheduleGenerator.h \
    schedule-components/PXISchedulePlc.h \
    schedule-components/PXISchedulePowerSupply.h \
    #schedule-components/PXIScheduleSMU.h \
    schedule-components/PXIScheduleThermoCouple.h \
    #schedule-components/PXIScheduleDIO.h \
    database/PXIDb.h


