#ifndef LOGGER_H
#define LOGGER_H

#include <QObject>
#include <QFile>
#include "Settings.h"

class Logger : public QObject
{
    Q_OBJECT
public:
    explicit Logger(const Settings *settings, QObject * parent, void *serviceObject);

    void log(const QString & msg);
    void dbgLog(const QString & msg);

    void logChamberTemperature(float temp, int t);
    void logTemperature(float peakTemp, float avgTemp, float tempSet);
    void logPowerSupply(quint32 nSlot, quint32 instance, double v, double c);
    void logPowerSupplySwitch(quint32 nSlot, quint32 instance, bool b);
    void logGenerator(quint32 nSlot, double a, double o, double f, double d, double i);
    void logGeneratorSwitch(quint32 nSlot, bool b);
    void logSmu(quint32 nSlot, double v);
    void logSmuSwitch(quint32 nSlot, bool b);

signals:

public slots:

private:
    const Settings * m_settings;
    void * m_serviceObject{nullptr};

    QFile logFile;
    QFile dbgLogFile;

    QString fileNameByCreation; // for situation when LogDaily=False, we mast logging to only one file

    QString logPath();
};

#endif // LOGGER_H
