#ifndef PXIPLCCONNECTION_H
#define PXIPLCCONNECTION_H

/*! Протокольная, сетевая часть взаимодействия с ПЛК !*/

#include <QObject>
#include <QPointer>
#include <QUdpSocket>
#include <QTimer>
#include <QNetworkDatagram>
#include <QQueue>
#include "ValueTypes.h"
#include "config.h"
#include "../../PXI-common/led-color.h"
#include "../../PXI-common/zummer.h"

class Logger;

class PXIPlcConnection : public QObject
{
    Q_OBJECT

public:
    PXIPlcConnection(const QPointer<Logger> &logger, std::function<void()> pauseFunction, QObject * parent = nullptr);

    struct AlarmData
    {
        AlarmData(ShortValue id, ShortValue instanceNumber, ShortValue errorCode, FloatValue param) :
            moduleId(id), instanceNumber(instanceNumber), errorCode(errorCode), parameter(param)
        {   }
        ShortValue moduleId;
        ShortValue instanceNumber;  // есть подозрение, что будет всегда "0", потому что все модулей по одной штуке
        ShortValue errorCode;
        FloatValue parameter;
    };

    enum ThermoprofileState { // нет смысла вводить новые define
        ThermoprofileNoState = NO_SLOT_STATE,
        ThermoprofileNormalState = NORMAL_SLOT_STATE,
        ThermoprofilePauseState = PAUSE_SLOT_STATE,
        ThermoprofileProblemState = ALARM_SLOT_STATE
    };

    bool sendStateRequest();                        // запрос текущего состояния
    bool sendParametersRequest();                   // установка основных параметров работы камеры по старту
    bool sendParametersRequest(const float tempHH, const float tempLL, const quint16 autostop, const float tempAutostop,
                               const float rackSP, const float rackHH, const float rackH, const float rackL, const float rackLL); // установка основных параметров работы камеры
    bool sendControlRequest(float temperature, float maxTemperatureChangeRate);    // управление изменяемыми параметрами
    bool sendStandByRequest(bool enable);                      // остановить камеру через поле MODE // по сути надо бы унести ее в private, но есть одно исключение для PXISchedulePlc

    // Вероятнее всего здесь появятся ф-ии предназначенные для внутреннего использования
    bool sendRGBRequest(LedColor color);
    bool sendBuzzerRequest(Zummer zummer);
    bool sendControlPackage(); // костыль по просьбе Юры - метод будет вызываться периодически и просто передавать в ПЛК буфер в текущем его состоянии
    //bool sendEmergencyStopRequest(); // через поле MODE

    bool sendAlarmReset(); // через поле RESET_ALARM

    void changeCheckInterval(qint64 newInterval);

    void pauseChamber();
    void resumeChamber();
    void resumeThermoprofileState();
    void beginThermoprofile();
    void endThermoprofile();

    const ShortValue & systemInfoId() const
    { return m_systemInfoId; }
    const ShortValue & systemInfoVersion() const
    { return m_systemInfoVersion; }
    const ShortValue & systemInfoErrorCode() const
    { return m_systemInfoErrorCode; }
    const ShortValue & systemInfoDeviceVersion() const
    { return m_systemInfoDeviceVersion; }
    const ShortValue & systemInfoHWVersion() const
    { return m_systemInfoHWVersion; }
    const ShortValue & systemInfoSWVersion() const
    { return m_systemInfoSWVersion; }
    const ShortValue & systemInfoSerialNumber() const
    { return m_systemInfoSerialNumber; }

    const ShortValue & thermalChamberId() const
    { return m_thermalChamberId; }
    const ShortValue & thermalChamberErrorCode() const
    { return m_thermalChamberErrorCode; }
    const FloatValue & mainTemperature() const
    { return m_thermalChamberTemperature; }
    const FloatValue & thermalChamberSlidedTemperatureSet() const
    { return m_thermalChamberSlidedTemperatureSet; }
    ShortValue thermalChamberMode() const
    { return m_thermalChamberMode; }
    const FloatValue & thermalChamberTemperatureSet() const
    { return m_thermalChamberTemperatureSet; }
    const FloatValue & thermalChamberTemperatureSpeed() const
    { return m_thermalChamberTemperatureSpeed; }

    const float & chamberParamTempHH() const
    { return m_tempHH; }
    const float & chamberParamTempLL() const
    { return m_tempLL; }
    bool chamberParamCoolAutostop() const
    { return ((bool)m_coolAutostopEnable); }
    const float & chamberParamTempAutostop() const
    { return m_tempCoolAutostop; }
    const float & chamberParamRackSP() const
    { return m_rackSP; }
    const float & chamberParamRackHH() const
    { return m_rackHH; }
    const float & chamberParamRackH() const
    { return m_rackH; }
    const float & chamberParamRackL() const
    { return m_rackL; }
    const float & chamberParamRackLL() const
    { return m_rackLL; }

    const ShortValue & accessoriesChamberId() const
    { return m_accessoriesChamberId; }
    const ShortValue & accessoriesErrorCode() const
    { return m_accessoriesErrorCode; }

    const ShortValue & optionsChamberId() const
    { return m_optionsChamberId; }
    const ShortValue & optionsErrorCode() const
    { return m_optionsErrorCode; }

    const ShortValue & alarmChamberId() const
    { return m_alarmChamberId; }
    const ShortValue & alarmErrorCode() const
    { return m_alarmErrorCode; }

    const ShortValue & alarmErrorsNumber() const
    { return m_alarmErrorsNumber; }

    const QList<AlarmData> & alarmDataList() const
    { return m_alarmDataList; }

    const ShortValue & extDI() const
    { return m_extDI; }
    const ShortValue & extDO() const
    { return m_extDO; }

    const QList<QPair<FloatValue,ShortValue>> & peripheralTemperatures()
    { return m_sensorTemperatures; }

    ShortValue upsLink() const
    { return m_upsLink; }
    ShortValue cameraLink() const
    { return m_cameraLink; }

    const FloatValue & calculatedRackTemerature() const
    { return m_calculatedRackTemerature; }
    const FloatValue & fanFrequency() const
    { return m_fanFrequency; }
    const ShortValue & fanErrorCode() const
    { return m_fanErrorCode; }

    const ShortValue & currentInputUPS() const
    { return m_currentInputUPS; }
    const ShortValue & currentOutputUPS() const
    { return m_currentOutputUPS; }
    const ShortValue & ratingPowerUPS() const
    { return m_ratingPowerUPS; }
    const ShortValue & outputVoltageUPS() const
    { return m_outputVoltageUPS; }
    const ShortValue & batteryVoltageUPS() const
    { return m_batteryVoltageUPS; }
    const ShortValue & loadPercentUPS() const
    { return m_loadPercentUPS; }
    const ShortValue & inputVoltageFrequencyUPS() const
    { return m_inputVoltageFrequencyUPS; }
    const ShortValue & outputVoltageFrequencyUPS() const
    { return m_outputVoltageFrequencyUPS; }
    const ShortValue & workingTimeUPS() const
    { return m_workingTimeUPS; }
    const ShortValue & chargeUPS() const
    { return m_chargeUPS; }
    const ShortValue & inputVoltageUPS() const
    { return m_inputVoltageUPS; }

    const ShortValue & bitsStatusUPS() const
    { return m_bitsStatusUPS; }


    //! эти функции (как и поля) неплохо было бы перенести в специально созданный 9-ый объект от PXISlot
    float averageMainTemperature();
    void setTemperatureToWindow(float preferedTemperature, float temperatureTolerance, bool doTemperatureCheck, bool append);
    void syncState(bool doTemperatureChack);
    ThermoprofileState thermoprofileState() const
    { return m_thermoprofileState; }

    bool linkState() const
    { return m_linkState; }

private slots:
    void socketHandler();

private:
    void stateHandler(const QNetworkDatagram &datagram);

    std::function<void()> m_pauseFunction;

private:
    QPointer<Logger> m_logger;

    const QHostAddress m_hostAddress;
    QByteArray m_stateRequest;
    QByteArray m_controlRequest;
    QByteArray m_parametersRequest;
    QPointer<QUdpSocket> m_udpSocket;
    QPointer<QTimer> m_checkTimer;
    QPointer<QTimer> m_watchDogTimer;

    bool m_linkState{false};
    // SYSTEM_INFO
    ShortValue m_systemInfoId{0};
    ShortValue m_systemInfoVersion{0};
    //IntValue m_systemAccessMode{0};
    ShortValue m_systemInfoErrorCode{0};                   //! errorCode - возможно не активен
    ShortValue m_systemInfoDeviceVersion{0};
    //IntValue m_systemInfoProtocolVersion{0};
    ShortValue m_systemInfoHWVersion{0};
    ShortValue m_systemInfoSWVersion{0};
    ShortValue m_systemInfoSerialNumber{0};

    ShortValue m_thermalChamberId{0};
    //IntValue m_thermalChamberVersion{0};
    //IntValue m_thermalChamberAccessMode{0};
    ShortValue m_thermalChamberErrorCode{0};               //! errorCode
    FloatValue m_thermalChamberTemperature{0.0};             // самая главная температура термокамеры (иметь ввиду, что они одни и те же для всех слотов)
    FloatValue m_thermalChamberSlidedTemperatureSet{0.0};    // cкользящая уставка температуры
    ShortValue m_thermalChamberMode{0};                    // режим камеры
    FloatValue m_thermalChamberTemperatureSet{0.0};          // текущая уставка температуры в рабочем объеме
    FloatValue m_thermalChamberTemperatureSpeed{0.0};        // скорость изменения температуры в минуту (по видимому речь идет об установленном ограничительном параметре)

    // эти параметры не получаются сервисом из пакета состояния, а существуют по умолчанию и вводятся пользователем
    float m_tempHH{PLC_PARAMETER_TEMP_HH};
    float m_tempLL{PLC_PARAMETER_TEMP_LL};
    quint16 m_coolAutostopEnable{PLC_PARAMETER_COOL_AUTOSTOP_ENABLE};
    float m_tempCoolAutostop{PLC_PARAMETER_TEMP_COOL_AUTOSTOP};
    float m_rackSP{PLC_PARAMETER_RACK_SP};
    float m_rackHH{PLC_PARAMETER_RACK_HH};
    float m_rackH{PLC_PARAMETER_RACK_H};
    float m_rackL{PLC_PARAMETER_RACK_L};
    float m_rackLL{PLC_PARAMETER_RACK_LL};

    ShortValue m_accessoriesChamberId{0};
    //IntValue m_accessoriesVersion;
    //IntValue m_accessoriesAccessMode;
    ShortValue m_accessoriesErrorCode{0};                  //! errorCode - возможно не активен

    ShortValue m_optionsChamberId{0};
    //IntValue m_optionsVersion;
    //IntValue m_optionsAccessMode;
    ShortValue m_optionsErrorCode{0};                      //! errorCode - возможно не активен

    ShortValue m_alarmChamberId{0};
    //IntValue m_alarmVersion;
    //IntValue m_alarmAccessMode;
    ShortValue m_alarmErrorCode{0};                        //! errorCode - возможно не активен

    ShortValue m_alarmErrorsNumber{0};                     // число активных аварий
    QList<AlarmData> m_alarmDataList;                   // список из 8 (или меньше) элементов, характеризующих отказы модулей

    ShortValue m_extDI{0};
    ShortValue m_extDO{0};

    QList<QPair<FloatValue,ShortValue>> m_sensorTemperatures {
        {FloatValue(),ShortValue()},{FloatValue(),ShortValue()},{FloatValue(),ShortValue()},{FloatValue(),ShortValue()},
        {FloatValue(),ShortValue()},{FloatValue(),ShortValue()},{FloatValue(),ShortValue()},{FloatValue(),ShortValue()}
    };

    ShortValue m_upsLink{1};
    ShortValue m_cameraLink{1};

    FloatValue m_calculatedRackTemerature{0.0};              // расчитанное значение температуры в стойке, усредненное
    //IntValue m_calculatedRackTemeratureStatus;        // статус средней температуры в стойке (а нафиг оно нужно?)

    //FloatValue m_fanFrequencySet;                     // ?
    FloatValue m_fanFrequency{0.0};                          // Текущая частота вращения вентилятора оборотов/минуту
    ShortValue m_fanErrorCode{0};                          //! Авария вентилятора (1-нет/2-да)

    ShortValue m_currentInputUPS{0};
    ShortValue m_currentOutputUPS{0};
    ShortValue m_ratingPowerUPS{0};
    ShortValue m_outputVoltageUPS{0};
    ShortValue m_batteryVoltageUPS{0};
    ShortValue m_loadPercentUPS{0};
    ShortValue m_inputVoltageFrequencyUPS{0};
    ShortValue m_outputVoltageFrequencyUPS{0};
    ShortValue m_workingTimeUPS{0};
    ShortValue m_chargeUPS{0};
    ShortValue m_inputVoltageUPS{0};

    ShortValue m_bitsStatusUPS{0};

    ///-----------------------------------------------------------------------------------------------------------------

    QQueue<float> m_measuredTemperatures;
    float m_preferedTemperature;
    float m_temperatureTolerance;
    ThermoprofileState m_thermoprofileState{ThermoprofileNoState};

};

#endif // PXIPLCCONNECTION_H
