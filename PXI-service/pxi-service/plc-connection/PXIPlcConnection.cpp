#include "config.h"
#include "PXIPlcConnection.h"
#include <QVector>
#include "Logger.h"
#include <QRandomGenerator>

/// Иметь ввиду, поля в пакетах ДВУХбайтные и ЧЕТЫРЕХбайтные (и Бог его знает какие еще могут потом добавиться)
/// Но подготовленные пакеты будут лежать в классе как массивы байт.
/// Порядок байт в полях выбран согласно intel-формату. (хотя для float это утверждение почти теряет смысл)
/// В общем как в памяти переменная лежит, так он в массив и будет класться, без переворачивания порядка следования байт

PXIPlcConnection::PXIPlcConnection(const QPointer<Logger> &logger, std::function<void()> pauseFunction, QObject *parent) :
    QObject(parent),
    m_pauseFunction(pauseFunction),
    m_logger(logger),
    m_hostAddress(PLC_DESTINATION_ADDRESS),
    m_stateRequest(PLC_STATE_REQUEST_LENGTH, 0x00),
    m_controlRequest(PLC_CONTROL_REQUEST_LENGTH, 0x00),
    m_parametersRequest(PLC_PARAMETERS_REQUEST_LENGTH, 0x00),
    m_udpSocket(new QUdpSocket(this)),
    m_checkTimer(new QTimer(this)),
    m_watchDogTimer(new QTimer(this))
{
    const quint16 manualMode = PLC_STANDBY_MODE;
    memcpy(&m_controlRequest.data()[0], (const void *)(&manualMode), sizeof(qint16));

    qint16 cmd = PLC_STATE_REQUEST_CMD;
    memcpy(&m_stateRequest.data()[0], (const void *)(&cmd), sizeof(qint16));

    //!TODO: Предварительная конфигурация пакета параметров. Пока константы не выданы заказчиком. Это лишь неподтвержденная догадка, что нижеследующие парамерты будут const
    memcpy(&m_parametersRequest.data()[0], (const void *)(&m_tempHH), sizeof(float));
    memcpy(&m_parametersRequest.data()[4], (const void *)(&m_tempLL), sizeof(float));
    memcpy(&m_parametersRequest.data()[8], (const void *)(&m_coolAutostopEnable), sizeof(quint16));
    memcpy(&m_parametersRequest.data()[10], (const void *)(&m_tempCoolAutostop), sizeof(float));
    memcpy(&m_parametersRequest.data()[74], (const void *)(&m_rackSP), sizeof(float));
    memcpy(&m_parametersRequest.data()[78], (const void *)(&m_rackHH), sizeof(float));
    memcpy(&m_parametersRequest.data()[82], (const void *)(&m_rackH), sizeof(float));
    memcpy(&m_parametersRequest.data()[86], (const void *)(&m_rackL), sizeof(float));
    memcpy(&m_parametersRequest.data()[90], (const void *)(&m_rackLL), sizeof(float));

    connect(m_checkTimer.data(), &QTimer::timeout, [this](){
        static bool selector = false;
        if (selector) {
            sendStateRequest();
            selector = false;
        } else {
            // костыль по просьбе Юры - метод будет вызываться периодически и просто передавать в ПЛК буфер в текущем его состоянии
            sendControlPackage();
            selector = true;
        }
    });
    //!m_checkTimer->start(PLC_POLL_INTERVAL);

    connect(m_watchDogTimer, &QTimer::timeout, [this](){
        m_linkState = false;
        m_logger->log("Error: no link to PLC");
    });
    //!m_watchDogTimer->start(PLC_WATCHDOG_INTERVAL);
    changeCheckInterval(PLC_POLL_INTERVAL);

    m_udpSocket->bind(QHostAddress::Any, PLC_SOURCE_PORT);
    connect(m_udpSocket, &QUdpSocket::readyRead, this, &PXIPlcConnection::socketHandler);
}

bool PXIPlcConnection::sendStateRequest()
{
    if (m_udpSocket->writeDatagram(QNetworkDatagram(m_stateRequest, m_hostAddress, PLC_DESTINATION_PORT)) != PLC_STATE_REQUEST_LENGTH) {
        m_logger->log(QStringLiteral("Error: can't send UDP state request"));
        return false;
    }

    return true;
}

bool PXIPlcConnection::sendParametersRequest()
{
    // все значения по умолчанию и константы уже внесены в массив пакета в конструкторе

    if (m_udpSocket->writeDatagram(QNetworkDatagram(m_parametersRequest, m_hostAddress, PLC_DESTINATION_PORT)) != PLC_PARAMETERS_REQUEST_LENGTH) {
        m_logger->log(QStringLiteral("Error: can't send UDP parameter request"));
        return false;
    }

    return true;
}

bool PXIPlcConnection::sendParametersRequest(const float tempHH, const float tempLL, const quint16 autostop, const float tempAutostop,
                                             const float rackSP, const float rackHH, const float rackH, const float rackL, const float rackLL)
{
    m_tempHH = tempHH;
    m_tempLL = tempLL;
    m_coolAutostopEnable = autostop;
    m_tempCoolAutostop = tempAutostop;

    m_rackSP = rackSP;
    m_rackHH = rackHH;
    m_rackH = rackH;
    m_rackL = rackL;
    m_rackLL = rackLL;

    memcpy(&m_parametersRequest.data()[0], (const void *)(&m_tempHH), sizeof(float));
    memcpy(&m_parametersRequest.data()[4], (const void *)(&m_tempLL), sizeof(float));
    memcpy(&m_parametersRequest.data()[8], (const void *)(&m_coolAutostopEnable), sizeof(quint16));
    memcpy(&m_parametersRequest.data()[10], (const void *)(&m_tempCoolAutostop), sizeof(float));

    memcpy(&m_parametersRequest.data()[74], (const void *)(&m_rackSP), sizeof(float));
    memcpy(&m_parametersRequest.data()[78], (const void *)(&m_rackHH), sizeof(float));
    memcpy(&m_parametersRequest.data()[82], (const void *)(&m_rackH), sizeof(float));
    memcpy(&m_parametersRequest.data()[86], (const void *)(&m_rackL), sizeof(float));
    memcpy(&m_parametersRequest.data()[90], (const void *)(&m_rackLL), sizeof(float));

    if (m_udpSocket->writeDatagram(QNetworkDatagram(m_parametersRequest, m_hostAddress, PLC_DESTINATION_PORT)) != PLC_PARAMETERS_REQUEST_LENGTH) {
        m_logger->log(QStringLiteral("Error: can't send UDP parameter request"));
        return false;
    }

    return true;
}

bool PXIPlcConnection::sendControlRequest(float temperature, float maxTemperatureChangeRate = 0.0)
{
    const quint16 manualMode = PLC_CHAMBER_MANUAL_MODE;

    //! TODO: Входные параметры нигде не сохраняются (только в массиве самого пакета). И на данном этапе я не знаю надо ли вообще их сохранять.
    memcpy(&m_controlRequest.data()[0], (const void *)(&manualMode), sizeof(qint16));
    memcpy(&m_controlRequest.data()[14], (const void *)(&temperature), sizeof(float));
    memcpy(&m_controlRequest.data()[18], (const void *)(&maxTemperatureChangeRate), sizeof(float));

    if (m_udpSocket->writeDatagram(QNetworkDatagram(m_controlRequest, m_hostAddress, PLC_DESTINATION_PORT)) != PLC_CONTROL_REQUEST_LENGTH) {
        m_logger->log(QStringLiteral("Error: can't send UDP control request"));
        return false;
    }

    return true;
}

bool PXIPlcConnection::sendStandByRequest(bool standByEnable)
{
    quint16 standByMode;
    (standByEnable) ? (standByMode = PLC_STANDBY_MODE) : (standByMode = PLC_CHAMBER_MANUAL_MODE);

    memcpy(&m_controlRequest.data()[0], (const void *)(&standByMode), sizeof(qint16));

    if (m_udpSocket->writeDatagram(QNetworkDatagram(m_controlRequest, m_hostAddress, PLC_DESTINATION_PORT)) != PLC_CONTROL_REQUEST_LENGTH) {
        m_logger->log(QStringLiteral("Error: can't send UDP standby request"));
        return false;
    }
    //m_udpSocket->waitForBytesWritten(); //не уверен что это нужно и чем-то поможет

    return true;
}

bool PXIPlcConnection::sendRGBRequest(LedColor color) // передалать, навреное, на общий тип надо ?
{
    const quint16 rgb = color;
    memcpy(&m_controlRequest.data()[48], (const void *)(&rgb), sizeof(qint16));

    m_logger->log(QStringLiteral("Changing RGB color to 0x") + QString::number(rgb, 16));

    if (m_udpSocket->writeDatagram(QNetworkDatagram(m_controlRequest, m_hostAddress, PLC_DESTINATION_PORT)) != PLC_CONTROL_REQUEST_LENGTH) {
        m_logger->log(QStringLiteral("Error: can't send UDP RGB led request"));
        return false;
    }

    return true;
}

bool PXIPlcConnection::sendBuzzerRequest(Zummer zummer)
{
//    // Проверка текущего значения... но не ясно, как все-таки отключать постоянный земмер
//    quint16 currentValue = 0;
//    memcpy(&currentValue, (const void *)&m_controlRequest.data()[60], sizeof(qint16));

//    if (currentValue == Zummer::ZummerContinuos)
//        return;

    memcpy(&m_controlRequest.data()[60], (const void *)(&zummer), sizeof(qint16));

    if (m_udpSocket->writeDatagram(QNetworkDatagram(m_controlRequest, m_hostAddress, PLC_DESTINATION_PORT)) != PLC_CONTROL_REQUEST_LENGTH) {
        m_logger->log(QStringLiteral("Error: can't send UDP RGB led request"));
        return false;
    }

    return true;
}

bool PXIPlcConnection::sendControlPackage()
{
    if (m_udpSocket->writeDatagram(QNetworkDatagram(m_controlRequest, m_hostAddress, PLC_DESTINATION_PORT)) != PLC_CONTROL_REQUEST_LENGTH) {
        m_logger->log(QStringLiteral("Error: can't send controll request"));
        return false;
    }

    return true;
}

bool PXIPlcConnection::sendAlarmReset()
{
    quint16 alarmResetCommand = PLC_ALARM_RESET;
    //! TODO: Входные параметры нигде не сохраняются (только в массиве самого пакета). И на данном этапе я не знаю надо ли вообще их сохранять.
    memcpy(&m_controlRequest.data()[2], (const void *)(&alarmResetCommand), sizeof(qint16));

    if (m_udpSocket->writeDatagram(QNetworkDatagram(m_controlRequest, m_hostAddress, PLC_DESTINATION_PORT)) != PLC_CONTROL_REQUEST_LENGTH) {
        m_logger->log(QStringLiteral("Error: can't send UDP control request"));

        alarmResetCommand = PLC_ALARM_NORESET;
        memcpy(&m_controlRequest.data()[2], (const void *)(&alarmResetCommand), sizeof(qint16));
        return false;
    }

    alarmResetCommand = PLC_ALARM_NORESET;
    memcpy(&m_controlRequest.data()[2], (const void *)(&alarmResetCommand), sizeof(qint16));
    return true;
}

void PXIPlcConnection::changeCheckInterval(qint64 newInterval)
{
    m_checkTimer->start(newInterval);
    m_watchDogTimer->start(newInterval + 3000); // интервал срабатывания watchDog должен всегда быть больше интевала посылки команд
}

void PXIPlcConnection::pauseChamber()
{
    if (m_thermoprofileState != ThermoprofileNormalState)
        return;

    if (sendStandByRequest(true))
        m_thermoprofileState = ThermoprofilePauseState;
    else
        m_thermoprofileState = ThermoprofileProblemState;
}

void PXIPlcConnection::resumeChamber()
{
    /// Не знаю точно, как, но буду возобновлять работу камеры через перевод ее режима в manual_mode
    /// Хотя возможно, что надо просто делать уставку температуры пониже

    if (m_thermoprofileState != ThermoprofilePauseState)
        return;

    if (!sendStandByRequest(false))
        m_thermoprofileState = ThermoprofileProblemState;
    // в ThermoprofileNormalState она вернется только когда температура устаканится (см. ниже)
}

void PXIPlcConnection::resumeThermoprofileState()
{
    if (m_thermoprofileState == ThermoprofilePauseState)
        m_thermoprofileState = ThermoprofileNormalState;
}

void PXIPlcConnection::beginThermoprofile()
{
    if (sendAlarmReset())
        m_thermoprofileState = ThermoprofileNormalState;
    else
        m_thermoprofileState = ThermoprofileProblemState;
}

void PXIPlcConnection::endThermoprofile()
{
    sendStandByRequest(true);
    m_thermoprofileState = ThermoprofileNoState;

}

float PXIPlcConnection::averageMainTemperature()
{
    float averageTemperature = 0;
    for(const auto & t : m_measuredTemperatures)
        averageTemperature += t;
    return averageTemperature / m_measuredTemperatures.size();
}

void PXIPlcConnection::setTemperatureToWindow(float preferedTemperature, float temperatureTolerance, bool doTemperatureCheck, bool append)
{
    if (!mainTemperature().isValid)
        return;

    if (append) {
        m_measuredTemperatures.enqueue(mainTemperature().value);
        if (m_measuredTemperatures.size() > 5)
            m_measuredTemperatures.dequeue();
    }

    m_preferedTemperature = preferedTemperature;
    m_temperatureTolerance = temperatureTolerance;

    syncState(doTemperatureCheck);
}

void PXIPlcConnection::syncState(bool doTemperatureChack)
{
    if ((m_thermoprofileState != ThermoprofileNormalState) && (m_thermoprofileState != ThermoprofileProblemState))
        return;

    bool temperatureProblemCondition = false;
    if (doTemperatureChack && (m_measuredTemperatures.size() >= 5)) {

        float averageTemperature = averageMainTemperature();

        // Реализация опции не учета времени при понижении температуры ниже уставки
        if ((m_thermalChamberTemperatureSet.isValid) && (averageTemperature < (m_thermalChamberTemperatureSet.value - m_temperatureTolerance/2))) {
            m_pauseFunction();
        }

        if (m_thermalChamberTemperatureSet.isValid && (averageTemperature > (m_thermalChamberTemperatureSet.value + m_temperatureTolerance/2)) /*|| (averageTemperature < (m_preferedTemperature - m_temperatureTolerance/2))*/){
            m_logger->log(QString("Error: Chamber temperature (%1) out of range %2").arg(averageTemperature).arg(m_thermalChamberTemperatureSet.value));
            temperatureProblemCondition = true;
        }
    }

    // для любого блока (нагрев/не нагрев)
    bool alarmChamberCondition = false;
    if (alarmDataList().size()) {
        m_logger->log("Error: One of the plc module has alarm");
        for (auto a : alarmDataList()) {
            m_logger->log("module = "  + QString::number(a.moduleId.value) + " errorCode = " + QString::number(a.errorCode.value) + " param = " + QString::number(a.parameter.value));
        }
        alarmChamberCondition = true;
    }

    if (m_upsLink.isValid && (!m_upsLink.value)) {
        m_logger->log("Warning: PLC miss link to UPS");
    }
    if (m_cameraLink.isValid && (!m_cameraLink.value)) {
        m_logger->log("Warning: PLC miss link to camera");
    }

    if (temperatureProblemCondition || alarmChamberCondition)
        m_thermoprofileState = ThermoprofileProblemState;
    else
        m_thermoprofileState = ThermoprofileNormalState;
}

void PXIPlcConnection::socketHandler()
{
    while (m_udpSocket->hasPendingDatagrams()) {
        QNetworkDatagram datagram = m_udpSocket->receiveDatagram();
        stateHandler(datagram);
        // datagram.makeReply(QByteArray("answer")); // очень удобная функция
    }
}

void PXIPlcConnection::stateHandler(const QNetworkDatagram & datagram)
{
    /// Вопрос: А PXIPlcConnection сам будет хранить акуальное состояние камеры и отдавать то что у него в кеше по запросу от планировщика?
    /// Или плаировщик будет тупо ждать ответа по udp ?
    /// (Просто, по плану, планировщик сам будет звать sendStateRequest())
    /// или через сигнал-слоты все сделать?

    if (datagram.data().size() != PLC_STATE_RESPONSE_LENGHT) {
        m_logger->log(QStringLiteral("Error: wrong UDP state datagram length = ") + QString::number(datagram.data().size()));
        return;
    }

    m_linkState = true;
    m_watchDogTimer->start();

    memcpy(&m_systemInfoId.value, (const void *)&datagram.data().data()[0], sizeof(qint16));
    memcpy(&m_systemInfoVersion.value, (const void *)&datagram.data().data()[2], sizeof(qint16));
    memcpy(&m_systemInfoErrorCode.value, (const void *)&datagram.data().data()[6], sizeof(qint16));
    memcpy(&m_systemInfoDeviceVersion.value, (const void *)&datagram.data().data()[8], sizeof(qint16));
    memcpy(&m_systemInfoHWVersion.value, (const void *)&datagram.data().data()[12], sizeof(qint16));
    memcpy(&m_systemInfoSWVersion.value, (const void *)&datagram.data().data()[14], sizeof(qint16));
    memcpy(&m_systemInfoSerialNumber.value, (const void *)&datagram.data().data()[16], sizeof(qint16));

    memcpy(&m_thermalChamberId.value, (const void *)&datagram.data().data()[28], sizeof(qint16));
    memcpy(&m_thermalChamberErrorCode.value, (const void *)&datagram.data().data()[34], sizeof(qint16));
    memcpy(&m_thermalChamberTemperature.value, (const void *)&datagram.data().data()[38], sizeof(float));
    memcpy(&m_thermalChamberSlidedTemperatureSet.value, (const void *)&datagram.data().data()[46], sizeof(float));
    memcpy(&m_thermalChamberMode.value, (const void *)&datagram.data().data()[50], sizeof(qint16));
    memcpy(&m_thermalChamberTemperatureSet.value, (const void *)&datagram.data().data()[52], sizeof(float));
    memcpy(&m_thermalChamberTemperatureSpeed.value, (const void *)&datagram.data().data()[56], sizeof(float));

    memcpy(&m_accessoriesChamberId.value, (const void *)&datagram.data().data()[114], sizeof(qint16));
    memcpy(&m_accessoriesErrorCode.value, (const void *)&datagram.data().data()[120], sizeof(qint16));

    memcpy(&m_optionsChamberId.value, (const void *)&datagram.data().data()[132], sizeof(qint16));
    memcpy(&m_optionsErrorCode.value, (const void *)&datagram.data().data()[138], sizeof(qint16));

    memcpy(&m_alarmChamberId.value, (const void *)&datagram.data().data()[150], sizeof(qint16));
    memcpy(&m_alarmErrorCode.value, (const void *)&datagram.data().data()[156], sizeof(qint16));

    memcpy(&m_alarmErrorsNumber.value, (const void *)&datagram.data().data()[158], sizeof(qint16));

    m_alarmDataList.clear();
    for (qint32 i=0; ((i<m_alarmErrorsNumber.value) && (i<8)); ++i) {
        qint16 id, instance, errorCode;
        float param;
        memcpy(&id,         (const void *)&datagram.data().data()[160+i*10], sizeof(qint16));
        memcpy(&instance,   (const void *)&datagram.data().data()[162+i*10], sizeof(qint16));
        memcpy(&errorCode,  (const void *)&datagram.data().data()[164+i*10], sizeof(qint16));
        memcpy(&param,      (const void *)&datagram.data().data()[166+i*10], sizeof(float));
        m_alarmDataList.append(AlarmData(ShortValue(id), ShortValue(instance), ShortValue(errorCode), FloatValue(param)));
    }

    memcpy(&m_extDI.value, (const void *)&datagram.data().data()[250], sizeof(qint16));
    memcpy(&m_extDO.value, (const void *)&datagram.data().data()[252], sizeof(qint16));

    float sensorTemp0{0},sensorTemp1{0},sensorTemp2{0},sensorTemp3{0},sensorTemp4{0},sensorTemp5{0},sensorTemp6{0},sensorTemp7{0};
    qint16 sensorOk0{0},sensorOk1{0},sensorOk2{0},sensorOk3{0},sensorOk4{0},sensorOk5{0},sensorOk6{0},sensorOk7{0};
    memcpy(&sensorTemp0, (const void *)&datagram.data().data()[258], sizeof(float));
    memcpy(&sensorTemp1, (const void *)&datagram.data().data()[262], sizeof(float));
    memcpy(&sensorTemp2, (const void *)&datagram.data().data()[266], sizeof(float));
    memcpy(&sensorTemp3, (const void *)&datagram.data().data()[270], sizeof(float));
    memcpy(&sensorTemp4, (const void *)&datagram.data().data()[274], sizeof(float));
    memcpy(&sensorTemp5, (const void *)&datagram.data().data()[278], sizeof(float));
    memcpy(&sensorTemp6, (const void *)&datagram.data().data()[282], sizeof(float));
    memcpy(&sensorTemp7, (const void *)&datagram.data().data()[286], sizeof(float));

    memcpy(&sensorOk0, (const void *)&datagram.data().data()[290], sizeof(qint16));
    memcpy(&sensorOk1, (const void *)&datagram.data().data()[292], sizeof(qint16));
    memcpy(&sensorOk2, (const void *)&datagram.data().data()[294], sizeof(qint16));
    memcpy(&sensorOk3, (const void *)&datagram.data().data()[296], sizeof(qint16));
    memcpy(&sensorOk4, (const void *)&datagram.data().data()[298], sizeof(qint16));
    memcpy(&sensorOk5, (const void *)&datagram.data().data()[300], sizeof(qint16));
    memcpy(&sensorOk6, (const void *)&datagram.data().data()[302], sizeof(qint16));
    memcpy(&sensorOk7, (const void *)&datagram.data().data()[304], sizeof(qint16));

    m_sensorTemperatures[0].first.setValue(sensorTemp0); m_sensorTemperatures[0].second.setValue(sensorOk0);
    m_sensorTemperatures[1].first.setValue(sensorTemp1); m_sensorTemperatures[1].second.setValue(sensorOk1);
    m_sensorTemperatures[2].first.setValue(sensorTemp2); m_sensorTemperatures[2].second.setValue(sensorOk2);
    m_sensorTemperatures[3].first.setValue(sensorTemp3); m_sensorTemperatures[3].second.setValue(sensorOk3);
    m_sensorTemperatures[4].first.setValue(sensorTemp4); m_sensorTemperatures[4].second.setValue(sensorOk4);
    m_sensorTemperatures[5].first.setValue(sensorTemp5); m_sensorTemperatures[5].second.setValue(sensorOk5);
    m_sensorTemperatures[6].first.setValue(sensorTemp6); m_sensorTemperatures[6].second.setValue(sensorOk6);
    m_sensorTemperatures[7].first.setValue(sensorTemp7); m_sensorTemperatures[7].second.setValue(sensorOk7);

    memcpy(&m_upsLink.value, (const void *)&datagram.data().data()[318], sizeof(qint16));
    memcpy(&m_cameraLink.value, (const void *)&datagram.data().data()[320], sizeof(qint16));

    memcpy(&m_calculatedRackTemerature.value, (const void *)&datagram.data().data()[322], sizeof(float));

    memcpy(&m_fanFrequency.value, (const void *)&datagram.data().data()[338], sizeof(float));
    memcpy(&m_fanErrorCode.value, (const void *)&datagram.data().data()[342], sizeof(qint16));

    memcpy(&m_currentInputUPS.value, (const void *)&datagram.data().data()[344], sizeof(qint16));
    memcpy(&m_currentOutputUPS.value, (const void *)&datagram.data().data()[346], sizeof(qint16));
    memcpy(&m_ratingPowerUPS.value, (const void *)&datagram.data().data()[348], sizeof(qint16));
    memcpy(&m_outputVoltageUPS.value, (const void *)&datagram.data().data()[350], sizeof(qint16));
    memcpy(&m_batteryVoltageUPS.value, (const void *)&datagram.data().data()[352], sizeof(qint16));
    memcpy(&m_loadPercentUPS.value, (const void *)&datagram.data().data()[362], sizeof(qint16));
    memcpy(&m_inputVoltageFrequencyUPS.value, (const void *)&datagram.data().data()[368], sizeof(qint16));
    memcpy(&m_outputVoltageFrequencyUPS.value, (const void *)&datagram.data().data()[370], sizeof(qint16));
    memcpy(&m_workingTimeUPS.value, (const void *)&datagram.data().data()[372], sizeof(qint16));
    memcpy(&m_chargeUPS.value, (const void *)&datagram.data().data()[374], sizeof(qint16));
    memcpy(&m_inputVoltageUPS.value, (const void *)&datagram.data().data()[376], sizeof(qint16));

    memcpy(&m_bitsStatusUPS.value, (const void *)&datagram.data().data()[378], sizeof(qint16));
    //m_logger->log(QStringLiteral("UPS status bits = ") + QString::number(m_bitsStatusUPS.value)); //! только для отладки порядка бит
    //! Видимо в это же месте нужно ставить посылку команды redBlink,
    //! Хотя можно и в syncState), но я считаю, что авария питания может произойти и без термоциклограммы
}




