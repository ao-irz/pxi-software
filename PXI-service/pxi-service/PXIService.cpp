#include <QCoreApplication>
#include <QThread>
#include <QLocalServer>
#include <QLocalSocket>
#include <QDateTime>
#include <QTimer>
#include <QFile>
#include <QSettings>
#include <QDir>
#include <QSettings>
#include <QJsonDocument>
#include <QJsonArray>
#include "config.h"
#include "PXIService.h"
#include "schedule-components/PXISchedulePowerSupply.h"
#include "schedule-components/PXIScheduleGenerator.h"
#include "schedule-components/PXIScheduleThermoCouple.h"

PXIService::PXIService(int argc, char **argv, const QString & selfPath) :
    QtService<QCoreApplication>(argc, argv, SERVICE_NAME),
    m_selfPath(selfPath)
{    
    //! application() here is not valid

    setServiceDescription("Service for managing PXI modules");
    setStartupType(QtServiceController::AutoStartup);

//    Settings dbgSettings(m_selfPath, nullptr);
//    qDebug() << dbgSettings.logPath();
//    qDebug() << dbgSettings.value("LogPath");
//    //dbgSettings.sync();
}

PXIService::~PXIService()
{
    //! do not clear variables here, because destructor called
}

void PXIService::start()
{
    logMessage("PXIService started");

    m_serviceApp = application();
    m_settings = new Settings(m_selfPath, m_serviceApp);
    m_logger = new Logger(m_settings, m_serviceApp, this);
    m_db = PXIDb::getInstance(m_logger, m_settings, m_serviceApp);

    // ================ Starting logging  ================
    logMessageString(QStringLiteral("Service started"));

    // ================ Local socket for UI ================
    m_localServer = new QLocalServer(m_serviceApp);
    m_localServer->setSocketOptions(QLocalServer::WorldAccessOption); // it is very imortant, because service live under administrator privelegie level
    QObject::connect(m_localServer.data(), &QLocalServer::newConnection, [this]() {
        QLocalSocket * socket = m_localServer->nextPendingConnection();
        QObject::connect(socket, &QLocalSocket::readyRead, [=]() {
            socketHandler(socket);
        });

        QObject::connect(socket, &QLocalSocket::disconnected, socket, &QLocalSocket::deleteLater);

        //TODO: maybe add timer for every connection
    });

    // ================ Unknown socket for PLC connection ================
    m_plcConnection = new PXIPlcConnection(m_logger, std::bind(&PXIService::pauseByTemperatureReason, this), m_serviceApp);

    m_plcConnection->sendParametersRequest();
    //m_plcConnection->sendRGBRequest(setCurrentLedColor(LedColor::WhiteLed));
    changeLedColor();


    // ================ RS-232 ================
    m_switchConnection = new PXISwitchConnection(m_logger, m_serviceApp);

    // ================ PXI Hardware ================
    Bugs bugs;
    m_logger->log(QStringLiteral("----- PXI Hardware init ------"));
    try {
#ifdef PXI_EMULATOR_ENABLE
        m_hardware.reset(new PxiEmulator(m_serviceApp, m_selfPath));
#else
        m_hardware.reset(new Pxi());
#endif
        bugs = m_hardware->init();
    } catch (PxiException & e) {
        m_logger->log(e.msg);
        disableTests();
    }
    if (!bugs.isEmpty()) {
        logBugs(bugs);
        disableTests();
    }
    m_logger->log(QStringLiteral("----- PXI Hardware init complete ------"));

    // ================ Schedule timer ================
    m_scheduleTimer = new QTimer(m_serviceApp);
    QObject::connect(m_scheduleTimer.data(), &QTimer::timeout, [this](){
        schedulerHandler();
    });
    m_scheduleTimer->setInterval(SCHEDULER_PERIOD);
    m_scheduleTimer->start();

    // ==================== Pause timer ====================
    m_tryResumeTimer = new QTimer(m_serviceApp);
    QObject::connect(m_tryResumeTimer.data(), &QTimer::timeout, [this](){
        tryResumeTests();
    });
    m_tryResumeTimer->setInterval(CHECK_END_PAUSE_PERIOD);

    // ================ Listen ================
    if (m_localServer->listen(PIPE_NAME))
        m_logger->dbgLog("Service named pipe is successfully listening");
    else
        m_logger->dbgLog("Service named pipe is not listeding");

}

void PXIService::stop()
{
    m_switchConnection->switchOff(); // !не уверен, что это правильно

    m_hardware->close();
    m_hardware.clear();
    //emergencyBreakPxiModules(); // полагаю, что предыдущей строчки будет вполне достаточно
    emergencyBreakThermoprofile(); // не уверен, что команда успеет отправиться в порт

    delete m_settings;
    delete m_localServer;
    delete m_plcConnection;
    delete m_switchConnection;
    delete m_logger;
    delete m_db;
    delete m_scheduleTimer;
    delete m_tryResumeTimer;
    m_socketFlag = false;
    m_socketCommand = 0;
    m_socketCommandLength = 0;

    m_logger->log("PXIService  stopped");
    logMessage("PXIService  stopped");
}

void PXIService::pause()
{

}

void PXIService::resume()
{

}

void PXIService::processCommand(int code)
{
    if (code == 1) {
        m_logger->log("Service stopping");
    }
}

void PXIService::socketHandler(QLocalSocket * socket)
{
    //! Format specification: 1byte(command) + 4byte(length of payload) + Nbyte(payload)
    qint64 availabeBytes = socket->bytesAvailable();
    while (availabeBytes >= HEADER_LENGTH) {
        if (!m_socketFlag) {
            socket->getChar(reinterpret_cast<char *>(&m_socketCommand));
            QByteArray lenBA = socket->read(LENGTH_OF_PAYLOAD);
            m_socketCommandLength = *(reinterpret_cast<quint32 *>(lenBA.data()));
            m_socketFlag = true;
            availabeBytes -= HEADER_LENGTH;
        }

        if (availabeBytes < m_socketCommandLength)
            return;

        QByteArray meat;
        switch(m_socketCommand)
        {
        case StateRequest:
            stateRequestHandler(socket);
            cleanSocketCommand();
            break;
        case RunCyclogramCommand:
            meat = socket->read(m_socketCommandLength);
            runCyclogramCommandHandler(meat);
            cleanSocketCommand();
            break;
        case BreakCyclogramCommand:
            meat = socket->read(m_socketCommandLength);
            breakCyclogramCommandHandler(meat);
            cleanSocketCommand();
            break;
        case SetRackTemperatureCommand:
            meat = socket->read(m_socketCommandLength);
            setRackTemperatureCommandHandler(meat);
            cleanSocketCommand();
            break;
        case AlarmResetCommand:
            alarmResetCommandHandler();
            cleanSocketCommand();
            break;
        case SetLedColorCommand:
            meat = socket->read(m_socketCommandLength);
            setLedColorCommandHandler(meat);
            cleanSocketCommand();
            break;
        case PauseCommand:
            pauseCommandHandler();
            cleanSocketCommand();
            break;
        case ResumeCommand:
            meat = socket->read(m_socketCommandLength);
            resumeCommandHandler(meat);
            cleanSocketCommand();
            break;
        default:
            meat = socket->read(m_socketCommandLength);
            cleanSocketCommand();
        }

        availabeBytes = socket->bytesAvailable();
    }
}

void PXIService::cleanSocketCommand()
{
    m_socketFlag = false;
    m_socketCommandLength = 0;
    m_socketCommand = 0;
}

void PXIService::stateRequestHandler(QLocalSocket *socket)
{
    //m_logger->log("received state request");
    sendResponseState(socket);
}

void PXIService::runCyclogramCommandHandler(const QByteArray &meat)
{
    quint32 nSlot = *reinterpret_cast<quint32 *>(meat.left(NSLOT_LENGTH).data());
    QByteArray jsonCyclogram = meat.right(meat.size() - NSLOT_LENGTH);

    if (m_slots.contains(nSlot)) {
        m_logger->log("Error: Attempt to start a cyclogram. This slot is busy");
        return;
    }

    m_logger->log("---- begin cyclogram ---- slot = " + QString::number(nSlot+1));
    m_logger->log(jsonCyclogram);
    m_logger->log("---- end cyclogram ---- ");

    QJsonParseError error;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonCyclogram, &error);
    if (error.error != QJsonParseError::NoError) {
        m_logger->log(QStringLiteral("Error: Cyclogram json error: ") + error.errorString() + " at " + QString::number(error.offset));
        return;
    }

    if (nSlot == THERMO_SLOT_NUMBER) {
        parseCyclogramThermo(jsonDoc.object());
        //! TODO: сделать валидацию на подобие postParsing()
        m_plcConnection->beginThermoprofile();
        return;
    }

    //m_hardware->sample(nSlot)->close();
    //m_hardware->sample(nSlot)->init();
    parseCyclogram(nSlot, jsonDoc.object());
    changeLedColor();
}

void PXIService::breakCyclogramCommandHandler(const QByteArray &meat)
{
    quint32 nSlot = *reinterpret_cast<quint32 *>(meat.left(NSLOT_LENGTH).data());

    if (nSlot == THERMO_SLOT_NUMBER) {
        if (!m_pxiSchedules.contains(nSlot)) {
            m_logger->log(QString("Service: (Error) Attempt to stop empty thermoprofile"));
            return;
        }
        emergencyBreakThermoprofile();
        //! Здесь по хорошему бы завершать все тесты...
        m_logger->log(QString("Service: Received break command for thermoprofile"));
        return;
    }

    m_logger->log(QString("Service: Received break command for slot %1").arg(nSlot+1));

    if (!m_slots.contains(nSlot)) {
        m_logger->log(QString("Service: (Error) Attempt to stop empty slot %1").arg(nSlot+1));
        return;
    }

    if (m_slots.value(nSlot)->state() == PXISlot::TestCompletedState) {
        logMessageString(QString("Service: Slot %1 removed normally").arg(nSlot+1));
    } else if ((m_slots.value(nSlot)->state() == PXISlot::AlarmSlotState) || (m_slots.value(nSlot)->state() == PXISlot::HardwareErrorSlotState)) {
        logMessageString(QString("Service: Slot %1 removed (slot had alarm or hardware error)").arg(nSlot+1));
    } else {
        logMessageString(QString("Service: Slot %1 removed (test forsed stopped)").arg(nSlot+1));
        // Загасить всё в PXI вне планировщика
        emergencyBreakPxiModules(nSlot);
    }

    m_slots.remove(nSlot);
    changeLedColor();
    m_pxiSchedules[nSlot].clear();
    m_pxiSchedules.remove(nSlot);
}

void PXIService::setRackTemperatureCommandHandler(const QByteArray &meat)
{
    float tempHH = *reinterpret_cast<float *>(meat.left(REAL_PARAM_LENGTH).data());
    float tempLL = *reinterpret_cast<float *>(meat.mid(REAL_PARAM_LENGTH, REAL_PARAM_LENGTH).data());
    quint16 autostop = *reinterpret_cast<quint32 *>(meat.mid(2*REAL_PARAM_LENGTH, REAL_PARAM_LENGTH).data()); // а вообще тут бул, который сокарщается до uint16
    float tempAutostop = *reinterpret_cast<float *>(meat.mid(3*REAL_PARAM_LENGTH, REAL_PARAM_LENGTH).data());

    float rackSP = *reinterpret_cast<float *>(meat.mid(4*REAL_PARAM_LENGTH, REAL_PARAM_LENGTH).data());
    float rackHH = *reinterpret_cast<float *>(meat.mid(5*REAL_PARAM_LENGTH, REAL_PARAM_LENGTH).data());
    float rackH = *reinterpret_cast<float *>(meat.mid(6*REAL_PARAM_LENGTH, REAL_PARAM_LENGTH).data());
    float rackL = *reinterpret_cast<float *>(meat.mid(7*REAL_PARAM_LENGTH, REAL_PARAM_LENGTH).data());
    float rackLL = *reinterpret_cast<float *>(meat.mid(8*REAL_PARAM_LENGTH, REAL_PARAM_LENGTH).data());

    m_plcConnection->sendParametersRequest(tempHH, tempLL, autostop, tempAutostop, rackSP, rackHH, rackH, rackL, rackLL);

    m_logger->log(QString("Set chamber parameters: tempHH = %1,tempLL = %2,autostop = %3,tempAutostop = %4,rackSP = %5,rackHH = %6,rackH = %7,rackL = %8,rackLL = %9")
                  .arg(tempHH).arg(tempLL).arg(autostop).arg(tempAutostop).arg(rackSP).arg(rackHH).arg(rackH).arg(rackL).arg(rackLL));
}

void PXIService::alarmResetCommandHandler()
{
    m_plcConnection->sendAlarmReset();

    m_logger->log("Chamber alarm reset");
}

void PXIService::setLedColorCommandHandler(const QByteArray &meat)
{
    quint16 colorInt = *reinterpret_cast<quint32 *>(meat.left(LEDCOLOR_LENGTH).data());
    LedColor color = (LedColor)colorInt;

    //! отключил для отладки эту проверку
    //!if ( (color != LedColor::NoColor) && (color != LedColor::WhiteLed) && (color != LedColor::BlueLed) ) {
    //!    m_logger->log(QStringLiteral("Error: trying to set the unacceptable led's color"));
    //!    return;
    //!}

    m_plcConnection->sendRGBRequest(color);
    m_logger->log("Set led color");
}

void PXIService::pauseCommandHandler()
{
    if (!m_slots.size()) {
        m_logger->log("Warning: There are no active slots for pause");
        return;
    }

    // Забить паузу во все слоты, которые активны. Думаю это должно быть сделано раньше чем в пауза пойдет в планировщик
    for (auto & pxiSlot : m_slots)
        pxiSlot->pauseTest();

    // Забить паузу во текущее задания
    for (const auto & slot : m_pxiSchedules) {
        for (const auto & sch : slot) {
            if (sch->stage() == PXIScheduleBase::ScheduleRun)
                sch->pauseSchedule();
        }
    }

    // Забить паузу в камеру
    m_plcConnection->pauseChamber();

    // Планировщик тоже должен быть приостановлен, но на самом деле внутри начнет выполняться другая ветка
    m_beginPauseTime = QDateTime::currentDateTime();
    m_isPauseOn = true;

    m_logger->log("All tests are paused");
}

void PXIService::pauseByTemperatureReason()
{
    if (m_isPauseOn)
        return;

    if (!m_slots.size())
        return;

    for (auto & pxiSlot : m_slots)
        pxiSlot->pauseTest();

    for (const auto & slot : m_pxiSchedules) {
        for (const auto & sch : slot) {
            if (sch->stage() == PXIScheduleBase::ScheduleRun)
                sch->pauseSchedule();
        }
    }

    //m_plcConnection->pauseChamber(); // не знаю, нужно ли стопорить камеру и потом сразу ее запускать ?

    m_beginPauseTime = QDateTime::currentDateTime();
    m_isPauseOn = true;

    m_logger->log("All tests are paused by low chamber temperature reason");

    //! ----  второе деййствие ----

    //m_plcConnection->resumeChamber();
    m_tryResumeTimer->start();

    m_logger->log("Now waiting for required temperature");}

void PXIService::resumeCommandHandler(const QByteArray &meat)
{
    quint32 parameter = *reinterpret_cast<quint32 *>(meat.left(PARAMETER_LENGTH).data());
    Q_UNUSED(parameter)

    if (!m_slots.size()) {
        m_logger->log("Warning: There are no active slots for resume tests");
        // return;
    }

    // Прием этой команды должен всего лишь разрешать ожидание слотами достижения камерой нужной температуры продолжения теста
    m_plcConnection->resumeChamber();
    m_tryResumeTimer->start();

    m_logger->log("Try to resume tests. Waiting for required temperature");
}

void PXIService::tryResumeTests()
{
    //! Тут подумать. Карточка: https://trello.com/c/Jcfw0Die
    float averageTemp = m_plcConnection->averageMainTemperature();
    if (!m_plcConnection->mainTemperature().isValid || !m_plcConnection->thermalChamberTemperatureSet().isValid
            || (abs(averageTemp - m_plcConnection->thermalChamberTemperatureSet().value) > TEMPERATURE_TOLERANCE/2) ) {

        return;
    }

    m_plcConnection->resumeThermoprofileState();

    //! Увеличить все ключи-временные-метки в m_pxiSchedules на CHECK_END_PAUSE_PERIOD * X
    //! TODO: оттестировать
    qint64 timeLength = m_beginPauseTime.msecsTo(QDateTime::currentDateTime());
    swapScheduler(timeLength);

    m_tryResumeTimer->stop();
    m_isPauseOn = false;

    // Снять c паузы в слотых
    //! Проверка состояния слота должна быть!!! Везде!!!
    // В слотах время финиша изменяется постоянно, но дискретно поэтому стоит привести все "к общему знаменателю"
    for (auto & pxiSlot : m_slots) {
        pxiSlot->extendOutputTime( timeLength );
        pxiSlot->resumeTest();
    }

    // Снять паузу со всех приостановленных текущих заданий
    for (auto & slot : m_pxiSchedules) {
        for (auto & sch : slot)
            sch->resumeSchedule();
    }

    // Снять паузу с планировщика (внутри планировщика по флагу будет выполняться другая ветка)

    m_logger->log("All tests resume");
}

void PXIService::emergencyBreakPxiModules(quint32 nSlot)
{
    //! TODO: Любое добавление нового оборудования в перечень PXI (того которое умеет выключаться), должно попадать и сюда

    Bugs bugss = m_hardware->sample(nSlot)->neutral();
    if (!bugss.isEmpty())
        logBugs(bugss);

    // Вернуть в исх. состояние все пины (на всякий случай)
    for (quint32 i=0; i<HSDIO_PINS_NUMBER; ++i)
        m_switchConnection->switchOff(nSlot*HSDIO_PINS_NUMBER + i + 1);

//    Bugs bugs;
//    try {
//        bugs += m_hardware->sample(nSlot)->fgen->off();

//        for(quint32 i=0; i<POWER_SUPPLY_NUMBER; ++i) {
//            auto powerSupply = m_hardware->sample(nSlot)->power(i);
//            bugs += powerSupply->off();
//        }

//    } catch (PxiException & e) {
//        m_logger->log(e.msg);
//        return;
//    }
//    if (!bugs.isEmpty())
//        logBugs(bugs);
}

void PXIService::emergencyBreakThermoprofile()
{
    m_pxiSchedules[THERMO_SLOT_NUMBER].clear();
    m_pxiSchedules.remove(THERMO_SLOT_NUMBER);

    m_plcConnection->endThermoprofile();
}

void PXIService::disableTests()
{
    //! TODO: ЗДЕСЬ необходимо предусмотреть какое-то действие блокирующее выполнение работу всех тестов
    //! И это же действие нужно сделать в ::start()
}

void PXIService::interruptTest(quint32 nSlot, PXISlot::SlotState state)
{
    emergencyBreakPxiModules(nSlot);
    changeLedColor();
    if (state == PXISlot::HardwareErrorSlotState) {
        //m_plcConnection->sendRGBRequest(LedColor::RedLed);
        m_plcConnection->sendBuzzerRequest(Zummer::ZummerContinuos);
    } else if (state == PXISlot::AlarmSlotState) {
        //m_plcConnection->sendRGBRequest(LedColor::YellowLed);
        m_plcConnection->sendBuzzerRequest(Zummer::ZummerShortBlink);
    }
}

void PXIService::changeLedColor()
{
    std::function<void(LedColor)> changing = [this](LedColor newColor){
        if (m_currentLedColor != newColor) {
            m_currentLedColor = newColor;
            m_plcConnection->sendRGBRequest(m_currentLedColor);
        }
    };

    if (m_slots.isEmpty()) {
        changing(LedColor::WhiteLed);
        return;
    }

    bool hardwareError{false};
    bool alarm{false};
    bool testCompleted{false};
    for (const auto & pxiSlot : m_slots) {
        if (pxiSlot->state() == PXISlot::HardwareErrorSlotState) {
            hardwareError = true;
            break;
        }
        if (pxiSlot->state() == PXISlot::AlarmSlotState)
            alarm = true;
        if (pxiSlot->state() == PXISlot::TestCompletedState)
            testCompleted = true;
    }

    if (hardwareError) {
        changing(LedColor::RedLed);
        return;
    }
    if (alarm) {
        changing(LedColor::YellowLed);
        return;
    }
    if (testCompleted) {
        changing(LedColor::BlueLed);
        return;
    }

    changing(LedColor::GreenLed);
}

void PXIService::sendResponseState(QLocalSocket *socket)
{
    QJsonDocument jsonDoc;

    QJsonObject rootObject;

    // --------- Блок температур от ПЛК ---------(все атрибуты переносятся в одном пакете состояния, поэтому мтожно проверить isValid только для одного поля)
    rootObject.insert("pauseOn", m_isPauseOn);
    rootObject.insert("plcLink", m_plcConnection->linkState());
    rootObject.insert("thermoprofileState", m_plcConnection->thermoprofileState());

    // эти параметры не получаются сервисом из пакета состояния, а существуют по умолчанию и вводятся пользователем
    rootObject.insert("chamberParamTempHH",m_plcConnection->chamberParamTempHH());
    rootObject.insert("chamberParamTempLL", m_plcConnection->chamberParamTempLL());
    rootObject.insert("chamberParamAutostop", m_plcConnection->chamberParamCoolAutostop());
    rootObject.insert("chamberParamTempAutostop", m_plcConnection->chamberParamTempAutostop());
    rootObject.insert("chamberParamRackSP", m_plcConnection->chamberParamRackSP());
    rootObject.insert("chamberParamRackHH", m_plcConnection->chamberParamRackHH());
    rootObject.insert("chamberParamRackH", m_plcConnection->chamberParamRackH());
    rootObject.insert("chamberParamRackL", m_plcConnection->chamberParamRackL());
    rootObject.insert("chamberParamRackLL", m_plcConnection->chamberParamRackLL());

    if (m_plcConnection->linkState()) {
        rootObject.insert("systemInfoId", m_plcConnection->systemInfoId().value);
        rootObject.insert("systemInfoVersion", m_plcConnection->systemInfoVersion().value);
        rootObject.insert("systemInfoDeviceVersion", m_plcConnection->systemInfoDeviceVersion().value);
        rootObject.insert("systemInfoHWVersion", m_plcConnection->systemInfoHWVersion().value);
        rootObject.insert("systemInfoSWVersion", m_plcConnection->systemInfoSWVersion().value);
        rootObject.insert("systemInfoSerialNumber", m_plcConnection->systemInfoSerialNumber().value);
        rootObject.insert("thermalChamberId", m_plcConnection->thermalChamberId().value);
        rootObject.insert("thermalChamberErrorCode", m_plcConnection->thermalChamberErrorCode().value);
        rootObject.insert("mainTemperature", m_plcConnection->mainTemperature().value); // фактически в UI отправляется не устредненное значение, возможно стоит поменять на ->averageMainTemperature()
        rootObject.insert("thermalChamberSlidedTemperatureSet", m_plcConnection->thermalChamberSlidedTemperatureSet().value);
        rootObject.insert("thermalChamberMode", m_plcConnection->thermalChamberMode().value);
        rootObject.insert("thermalChamberTemperatureSet", m_plcConnection->thermalChamberTemperatureSet().value);
        rootObject.insert("thermalChamberTemperatureSpeed", m_plcConnection->thermalChamberTemperatureSpeed().value);

        rootObject.insert("accessoriesChamberId", m_plcConnection->accessoriesChamberId().value);
        rootObject.insert("optionsChamberId", m_plcConnection->optionsChamberId().value);
        rootObject.insert("alarmChamberId", m_plcConnection->alarmChamberId().value);

        QJsonArray alarmArray;
        for (const auto &a : qAsConst(m_plcConnection->alarmDataList())) { // qAsConst not needed but for it's for remaind
            QJsonObject alarmObject;
            alarmObject.insert("moduleId", a.moduleId.value);
            alarmObject.insert("instanceNumber", a.instanceNumber.value);
            alarmObject.insert("errorCode", a.errorCode.value);
            alarmObject.insert("parameter", a.parameter.value);
            alarmArray.append(alarmObject);
        }
        rootObject.insert("modulesAlarms", alarmArray);

        rootObject.insert("extDI", m_plcConnection->extDI().value);
        rootObject.insert("extDO", m_plcConnection->extDO().value);

        QJsonArray peripheralArray;
        for (const auto &t : qAsConst(m_plcConnection->peripheralTemperatures())) { // qAsConst not needed but for it's for remaind
            QJsonObject peripheralObject;

            peripheralObject.insert("number", peripheralArray.size());
            peripheralObject.insert("temperature", t.first.value);
            peripheralObject.insert("sensorOk", t.second.value);
            peripheralArray.append(peripheralObject);
        }
        rootObject.insert("peripheralTemperature", peripheralArray);

        rootObject.insert("calculatedRackTemerature", m_plcConnection->calculatedRackTemerature().value);
        rootObject.insert("fanFrequency", m_plcConnection->fanFrequency().value);
        rootObject.insert("fanErrorCode", m_plcConnection->fanErrorCode().value);

        rootObject.insert("currentInputUPS", m_plcConnection->currentInputUPS().value);
        rootObject.insert("currentOutputUPS", m_plcConnection->currentOutputUPS().value);
        rootObject.insert("ratingPowerUPS", m_plcConnection->ratingPowerUPS().value);
        rootObject.insert("outputVoltageUPS", m_plcConnection->outputVoltageUPS().value);
        rootObject.insert("batteryVoltageUPS", m_plcConnection->batteryVoltageUPS().value);
        rootObject.insert("loadPercentUPS", m_plcConnection->loadPercentUPS().value);
        rootObject.insert("inputVoltageFrequencyUPS", m_plcConnection->inputVoltageFrequencyUPS().value);
        rootObject.insert("outputVoltageFrequencyUPS", m_plcConnection->outputVoltageFrequencyUPS().value);
        rootObject.insert("workingTimeUPS", m_plcConnection->workingTimeUPS().value);
        rootObject.insert("chargeUPS", m_plcConnection->chargeUPS().value);
        rootObject.insert("inputVoltageUPS", m_plcConnection->inputVoltageUPS().value);

        rootObject.insert("bitsStatusUPS", m_plcConnection->bitsStatusUPS().value);

        rootObject.insert("upsLink", m_plcConnection->upsLink().value);
        rootObject.insert("cameraLink", m_plcConnection->cameraLink().value);
    }

    // --------- Блок сосотяний слотов ---------
    QJsonArray slotsArray;
    for (auto pxiSlot : m_slots)
        slotsArray.append(pxiSlot->stateToJson());

    rootObject.insert("Slots", slotsArray);
    jsonDoc.setObject(rootObject);

    QByteArray payload = jsonDoc.toJson();
    quint32 len = payload.size();
    QByteArray response;
    response.append(STATE_RESPONSE);
    response.append(reinterpret_cast<char *>(&len), LENGTH_OF_PAYLOAD);
    response.append(payload);

    socket->write(response);
}

void PXIService::logMessageString(const QString & head)
{
    m_logger->log(head);
}

void PXIService::logBugs(const Bugs &bugs)
{
    for (auto bug : bugs) {
        m_logger->log(bug.message + " type = " + QString::number(bug.type) + " slot = " + QString::number(bug.slot));
    }
}

void PXIService::schedulerHandler()
{
    if (m_isPauseOn) {
        // Во время паузы время завершения слота должно ползти вперед (а progressBar оставаться на месте)
        for (auto & pxiSlot : m_slots)
            pxiSlot->extendOutputTime( m_beginPauseTime.msecsTo(QDateTime::currentDateTime()) );

        return;
    }

    for (auto slot = m_pxiSchedules.begin(); slot != m_pxiSchedules.end(); ++slot) {
        QMultiMap<QDateTime, QSharedPointer<PXIScheduleBase> > & slotSchedule = slot.value();

        // Условие завершения теста
        if (!slotSchedule.size()) {
            if (m_slots.contains(slot.key())) {
                emergencyBreakPxiModules(slot.key()); // добавлен на тот случай, если циклограмма не будет  содержать концевых блоков
                m_slots.value(slot.key())->completeTest();
                changeLedColor();
            }
            else if (slot.key() == THERMO_SLOT_NUMBER) {
                emergencyBreakThermoprofile();
                m_logger->log(QString("Service: Thermoprofile successfully completed"));
                //m_pxiSchedules.remove(THERMO_SLOT_NUMBER);
            }
            continue;
        }

        auto i = slotSchedule.begin();
        while (i != slotSchedule.end()) {

            // Специально для темпоциклограммы
            if ((slot.key() == THERMO_SLOT_NUMBER) &&
                    ((i.value()->stage() == PXIScheduleBase::ScheduleError) || (m_plcConnection->thermoprofileState() == PXIPlcConnection::ThermoprofileProblemState))) {

                for (auto & pxiSlot : m_slots)
                    pxiSlot->harwareErrorTest();
                emergencyBreakThermoprofile();
                i = slotSchedule.end();
                logMessageString(QString("Service: Erasing all tasks for all slots by chamber problem"));
                continue;
            }

            // По моим расчетам следующий код выполнится в случае чего только 1 раз
            if ((slot.key() != THERMO_SLOT_NUMBER) &&
                    ((i.value()->stage() == PXIScheduleBase::ScheduleError) || (m_slots.value(slot.key())->state() == PXISlot::AlarmSlotState) || (m_slots.value(slot.key())->state() == PXISlot::HardwareErrorSlotState))) {

                m_pxiSchedules[slot.key()].clear(); // стереть список заданий, но саму запись оставить, она удалиться в обработчике команды break. а алг. еще пройдет через "Условие завершения теста", но это в данном случае не важно
                i = slotSchedule.end();
                interruptTest(slot.key(), m_slots.value(slot.key())->state());
                logMessageString(QString("Service: Erasing all tasks for slot %1 by reason %2").arg(slot.key()+1).arg(m_slots.value(slot.key())->state()));
                continue;
            }

            if (i.value()->stage() == PXIScheduleBase::ScheduleCompleted) {
                i = slotSchedule.erase(i);
                continue;
            }

            if (i.value()->stage() == PXIScheduleBase::ScheduleRun) {
                ++i;
                continue;
            }

            if ((i.key() > QDateTime::currentDateTime()))
                break;

            //! А не нужно ли в этой проверке проверять еще и на i.value()->stage() == PXIScheduleBase::ScheduleWaitingStart ???
            if (i.key().secsTo(QDateTime::currentDateTime()) > 3) {
                logMessageString(QString("Warning: Detected outdated schedule's task in slot %1").arg(slot.key()+1) );
                logMessageString(QString("Warning: SecsTo = %1").arg( i.key().secsTo(QDateTime::currentDateTime()) ));
                logMessageString(QString("Warning: startTime = %1").arg( i.key().toString(DB_DATETIME_FORMAT) ));
                logMessageString(QString("Warning: stage = %1").arg( i.value()->stage() ));
                i = slotSchedule.erase(i);
                continue;
            }

            // Выполнение задания
            if (i.value()->stage() == PXIScheduleBase::ScheduleWaitingStart)
                i.value()->execSchedule();

            ++i;
        }
    }
}

void PXIService::swapScheduler(qint64 timeLength)
{
    m_logger->log("---Old schedule---");
    for (auto slot = m_pxiSchedules.constBegin(); slot != m_pxiSchedules.constEnd(); ++slot) {
        for (auto sch = slot.value().constBegin(); sch != slot.value().constEnd(); ++sch) {
            m_logger->log(QStringLiteral("startTime = ") + sch.key().toString(DB_DATETIME_FORMAT) + " stage = " + QString::number(sch.value()->stage()));
        }
    }
    m_logger->log("-----------------");

    QHash<quint32, QMultiMap<QDateTime, QSharedPointer<PXIScheduleBase> > > newPxiSchedules;

    m_logger->log("offset length time = " + QString::number(timeLength));
    for (auto slot = m_pxiSchedules.constBegin(); slot != m_pxiSchedules.constEnd(); ++slot) {

        QMultiMap<QDateTime, QSharedPointer<PXIScheduleBase> > newSlotSchedules;

        for (auto sch = slot.value().constBegin(); sch != slot.value().constEnd(); ++sch) {
            // Для тех, кто еще не нечался, необходимо перенести время старта и финиша "вперед"
            switch(sch.value()->stage()) {

            case PXIScheduleBase::ScheduleWaitingStart: {
                QDateTime newTime1 = sch.key().addMSecs(timeLength);
                QDateTime newTime2 = sch.value()->stopTime().addMSecs(timeLength);
                sch.value()->setStartStopTime(newTime1, newTime2);
                newSlotSchedules.insert(newTime1, sch.value());
                m_logger->log(QStringLiteral("Service: SwapSchedulre: Waiting schedule. Inserted startTime = ") + newSlotSchedules.last()->startTime().toString(DB_DATETIME_FORMAT) + " stage = " + QString::number(newSlotSchedules.last()->stage()) + " key " + newSlotSchedules.lastKey().toString(DB_DATETIME_FORMAT)  + " stopTime = " + sch.value()->stopTime().toString(DB_DATETIME_FORMAT));
            } break;

            case PXIScheduleBase::SchedulePause: {
                QDateTime newTime1 = sch.key();
                QDateTime newTime2 = sch.value()->stopTime().addMSecs(timeLength);
                sch.value()->setStartStopTime(newTime1, newTime2);
                newSlotSchedules.insert( newTime1, sch.value() );
                m_logger->log(QStringLiteral("Service: SwapSchedulre: Paused schedule. Inserted startTime = ") + sch.value()->startTime().toString(DB_DATETIME_FORMAT) + " stage = " + QString::number(newSlotSchedules.last()->stage()) + " key = " + sch.key().toString(DB_DATETIME_FORMAT) + " stopTime = " + sch.value()->stopTime().toString(DB_DATETIME_FORMAT));
            } break;

            case PXIScheduleBase::ScheduleRun:
                newSlotSchedules.insert(sch.key(), sch.value());
                m_logger->log(QStringLiteral("Service: Warning: SwapSchedulre: Running schedule appears. It's impossible"));
                break;
            case PXIScheduleBase::ScheduleError:
                newSlotSchedules.insert(sch.key(), sch.value());
                m_logger->log(QStringLiteral("Service: Warning: SwapSchedulre: ErrorSchedule appears. It's possible, but strange...."));
                break;
            case PXIScheduleBase::ScheduleCompleted: {
                newSlotSchedules.insert(sch.key(), sch.value());
                m_logger->log(QStringLiteral("Service: SwapSchedulre: Completed schedule appears. ") + sch.value()->startTime().toString(DB_DATETIME_FORMAT) + " stage = " + QString::number(newSlotSchedules.last()->stage()) + " key = " + sch.key().toString(DB_DATETIME_FORMAT) + " stopTime = " + sch.value()->stopTime().toString(DB_DATETIME_FORMAT));
            } break;

            default:
                newSlotSchedules.insert(sch.key(), sch.value());
                m_logger->log(QStringLiteral("Service: Warning: SwapSchedulre: DefaultSchedule. It means, that we have the new undefined ScheduleState") + sch.value()->startTime().toString(DB_DATETIME_FORMAT) + " stage = " + QString::number(newSlotSchedules.last()->stage()) + " key = " + sch.key().toString(DB_DATETIME_FORMAT) + " stopTime = " + sch.value()->stopTime().toString(DB_DATETIME_FORMAT));
            }
        }

        newPxiSchedules.insert(slot.key(), newSlotSchedules);
    }
    m_pxiSchedules.swap(newPxiSchedules);

    m_logger->log("---New schedule---");
    for (auto slot = m_pxiSchedules.constBegin(); slot != m_pxiSchedules.constEnd(); ++slot) {
        for (auto sch = slot.value().constBegin(); sch != slot.value().constEnd(); ++sch) {
            m_logger->log(QStringLiteral("startTime = ") + sch.value()->startTime().toString(DB_DATETIME_FORMAT) + " stage = " + QString::number(sch.value()->stage()) + "key = " + sch.key().toString(DB_DATETIME_FORMAT));
        }
    }
    m_logger->log("-----------------");
}
