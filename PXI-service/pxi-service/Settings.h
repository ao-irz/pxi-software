﻿#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>
#include <QUuid>

class Settings : public QSettings
{
    Q_OBJECT

public:
    enum ChannelType
    {
        Stream,
        Alsa
    };

    struct ChannelSettings
    {
        quint32 number;
        QString name;
        QString url;
        QString scte35;
        ChannelType type;
    };

    Settings(const QString &selfPath, QObject *parent);
    virtual ~Settings();

    QString selfPath() const
    { return m_selfPath; }

    QString logPath() const
    { return value("LogPath", "").toString(); }

    qint32 numberOfChannels()
    { return childGroups().size(); }

    QString dbAddress() const
    { return value("dbAddress", QString()).toString(); }

    qint32 dbPort() const
    { return value("dbPort", 0).toInt(); }

    QString dbName() const
    { return value("dbName", QString()).toString(); }

    QString dbUser() const
    { return value("dbUser", QString()).toString(); }

    QString dbPassword() const
    { return value("dbPassword", QString()).toString(); }

    quint32 limitRecords() const
    { return value("limitRecords", 100).toUInt(); }

    bool processFeature() const
    { return value("processFeature", false).toBool(); }

    bool insertToDB() const
    { return value("insertToDb", false).toBool(); }

    QUuid instanceUUID() const
    { return value("instanceUuid", QUuid()).toUuid(); }

    qint32 timeZoneCorrection() const
    { return value("timeZoneCorrection", 0).toInt(); }

    QStringList channels() const
    { return childGroups(); }

    ChannelSettings channel(const QString & identifier);
    ChannelSettings channel(const quint32 identifier);

    bool syncSettings();

    bool isSettingsOk() const
    { return m_settingsOk; }

private:
    void checkFileCorrectness();

    qint32 channelsNumber() const
    { return value("channelsNumber", 0).toInt(); }

    QString m_selfPath;
    quint16 m_checkSum{0};
    bool m_settingsOk{false};
};

#endif // SETTINGS_H
