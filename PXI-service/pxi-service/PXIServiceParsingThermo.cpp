#include <QJsonDocument>
#include <QJsonArray>
#include "config.h"
#include "PXIService.h"
#include "schedule-components/PXISchedulePlc.h"
#include "../../PXI-common/common.h"


void PXIService::parseCyclogramThermo(const QJsonObject & cyclogramObject)
{
    const QDateTime currentTime(QDateTime::currentDateTime());
    QDateTime stopTime(currentTime);

    for (auto generatorObject : cyclogramObject.value("thermoProfile").toArray()) {
        QJsonObject scheduleObject = generatorObject.toObject();

        bool isInfinite = false;
        qint64 duration = 0;
        if (scheduleObject.value("duration").isUndefined()) {
            isInfinite = true;
            duration = INT64_MAX/2;
        } else
            duration = scheduleObject.value("duration").toString().toULongLong();
        if ( scheduleObject.value("enable").isUndefined() ) {
            logMessageString("Warning: (Thermoprofile) enable attribute not defined");
            continue;
        }
        if ( scheduleObject.value("duration").isUndefined() && scheduleObject.value("type").toString() != "maintaining" ) {
            logMessageString("Warning: (Thermoprofile) infinite block couldn't be heating or cooling");
            continue;
        }
        if ( (duration==0) && scheduleObject.value("enable").toBool()) {
            logMessageString("Warning: (Thermoprofile) duration==0 not correspond to enable=false");
            continue;
        }

        QDateTime startTime;
        (scheduleObject.value("startTime").isUndefined()) ? // для нулевого элемента это значит "начать прямо сейчас", для остальных "начать с конца предыдущего"
                    (startTime = stopTime) :
                    (startTime = QDateTime::fromString(scheduleObject.value("startTime").toString(), DB_DATETIME_FORMAT));

        stopTime = startTime.addMSecs(duration);

        if (scheduleObject.value("enable").isUndefined() || scheduleObject.value("enable").toBool())
            m_pxiSchedules[THERMO_SLOT_NUMBER].insert(startTime,
                                         QSharedPointer<PXISchedulePlc>(
                                             new PXISchedulePlc(
                                                 m_plcConnection, m_logger,
                                                 startTime, stopTime,
                                                 scheduleObject.value("temperature").toDouble(),
                                                 scheduleObject.value("maxTemperatureChangeRate").toDouble(),
                                                 scheduleObject.value("type").toString(),
                                                 isInfinite )));
        else
            m_pxiSchedules[THERMO_SLOT_NUMBER].insert(startTime,
                                         QSharedPointer<PXISchedulePlc>(
                                             new PXISchedulePlc(
                                                 m_plcConnection, m_logger,
                                                 startTime, stopTime,
                                                 PXIScheduleBase::ScheduleActionOff,
                                                 isInfinite )));

        // После блока с бесконечной длительностью уже не должно быть других блков
        if (isInfinite)
            break;
    }
}
