#include <QJsonDocument>
#include <QJsonArray>
#include "config.h"
#include "PXIService.h"
#include "schedule-components/PXISchedulePowerSupply.h"
#include "schedule-components/PXIScheduleGenerator.h"
#include "schedule-components/PXIScheduleThermoCouple.h"
#include "schedule-components/PXIScheduleDIO.h"
#include "schedule-components/PXIScheduleSMU.h"

void PXIService::parseCyclogram(quint32 nSlot, const QJsonObject & cyclogramObject)
{
    /* Пока сделаны следующие допущения и ограничения
     * 0. допущение №0 - элементы массива должны быть упорядоченны по возростанию времни. Без этого никак!
     * 1. duration - обязательное поле
     * 2. startTime - может отсутствовать, и это отсутствие будет означать для 0-го элемента "стартуй сейчас", для всех остальных "стартуй сразу после предыдущего"
     * 3. enable - может отсутствовать, если в предыдущем элементе сущестовал как true, а в случае =false присутствие обязательно
     * 4. все элементы, у которых enable  отсутствует или true все равно будут приводить к вызову on() (для PS и FG)
     * 5. обязательным является наличие в массивах последнего элемента, у которого enable = false, duration = 0, что бы выключать PS и FG
     * 6. При помощи пустых (off) блоков можно сделать прерывистое тестировани. Но off-блоки должны будут иметь не нулевой duration
     * 7. Настоятельно хочется отметить, что бы закрывающий блок с "duration=0,enable=false имел бы право появлсять только в конце массива. Во избежании
     *    перепутывания блоков с одинаковой датой. (Это вообще гворя нужно проверить - поведение QMap-овского итератора в этих случаях)
     * 8. Термопарам закрывающий блок не нужен. Зачем он им, вообще?
     */

    if (m_pxiSchedules.value(nSlot).size()) {
        m_logger->log("Error: Tring to insert schedule to an uncompleted slot");
        return;
    }

    //! TODO: Предусмотреть вариант, когда циклограмма на столько больная, что не возможно пускать ее в работу
    //! В этом случае уже созданный PXISlot должне быть уничтожен
    QSharedPointer<PXISlot> currentPXISlot(new PXISlot(m_logger, nSlot, cyclogramObject.value("chipsName").toString(), cyclogramObject.value("chipsBatch").toString()));
    m_slots.insert(nSlot, currentPXISlot);

    const QDateTime currentTime(QDateTime::currentDateTime());
    for (auto powerSupplyObject : cyclogramObject.value("powerSupplies").toArray()) {
        quint32 nPowerSupply = powerSupplyObject.toObject().value("number").toInt();
        if (nPowerSupply >= POWER_SUPPLY_NUMBER) {
            logMessageString("Warning: (Power Supply) power supply number (" + QString::number(nPowerSupply) + ") out of range. That power supply will be skipped");
            continue;
        }
        QDateTime stopTime(currentTime);
        for (auto powerSupplySchedule : powerSupplyObject.toObject().value("schedule").toArray()) {
            QJsonObject scheduleObject = powerSupplySchedule.toObject();

            qint64 duration = scheduleObject.value("duration").toString().toULongLong();
            if (scheduleObject.value("duration").isUndefined()
                    || scheduleObject.value("enable").isUndefined()
                    || ((duration == 0) && scheduleObject.value("enable").toBool())
                    ) {
                logMessageString("Warning: (Power Supply) duration or enable attribute not defined or duration==0 not correspond to enable=false");
                continue;
            }

            QDateTime startTime;
            (scheduleObject.value("startTime").isUndefined()) ? // для нулевого элемента это значит "начать прямо сейчас", для остальных "начать с конца предыдущего"
                        (startTime = stopTime) :
                        (startTime = QDateTime::fromString(scheduleObject.value("startTime").toString(), DB_DATETIME_FORMAT));

            stopTime = startTime.addMSecs(duration);
            if (scheduleObject.value("enable").isUndefined() || scheduleObject.value("enable").toBool())
                m_pxiSchedules[nSlot].insert(
                            startTime,
                            QSharedPointer<PXISchedulePowerSupply>(
                                new PXISchedulePowerSupply(
                                    currentPXISlot,
                                    m_hardware,
                                    m_logger,
                                    scheduleObject.value("pollInterval").toInt(),
                                    startTime,
                                    stopTime,
                                    nPowerSupply,
                                    scheduleObject.value("voltage").toDouble(), scheduleObject.value("voltageTolerancePlus").toDouble(), scheduleObject.value("voltageToleranceMinus").toDouble(),
                                    scheduleObject.value("current").toDouble(), scheduleObject.value("currentTolerancePlus").toDouble(), scheduleObject.value("currentToleranceMinus").toDouble()
                                    )));
            else
                m_pxiSchedules[nSlot].insert(
                            startTime,
                            QSharedPointer<PXISchedulePowerSupply>(
                                new PXISchedulePowerSupply(currentPXISlot, m_hardware, m_logger, startTime, stopTime, nPowerSupply, PXIScheduleBase::ScheduleActionOff)));
        }
    }

    QDateTime stopTime(currentTime);
    for (auto generatorObject : cyclogramObject.value("generator").toArray()) {
        QJsonObject scheduleObject = generatorObject.toObject();

        qint64 duration = scheduleObject.value("duration").toString().toULongLong();
        if (scheduleObject.value("duration").isUndefined()
                || scheduleObject.value("enable").isUndefined()
                || ((duration == 0) && scheduleObject.value("enable").toBool()) ) {
            logMessageString("Warning: (Generator) duration or enable attribute not defined or duration==0 not correspond to enable=false");
            continue;
        }

        QDateTime startTime;
        (scheduleObject.value("startTime").isUndefined()) ?
                    (startTime = stopTime) :
                    (startTime = QDateTime::fromString(scheduleObject.value("startTime").toString(), DB_DATETIME_FORMAT));

        stopTime = startTime.addMSecs(duration);
        if (scheduleObject.value("enable").isUndefined() || scheduleObject.value("enable").toBool())
            m_pxiSchedules[nSlot].insert(
                        startTime,
                        QSharedPointer<PXIScheduleGenerator>(
                            new PXIScheduleGenerator(
                                currentPXISlot,
                                m_hardware,
                                m_logger,
                                scheduleObject.value("pollInterval").toInt(),
                                startTime,
                                stopTime,
                                scheduleObject.value("type").toString(),
                                scheduleObject.value("amplitude").toDouble(),
                                scheduleObject.value("offset").toDouble(),
                                scheduleObject.value("frequency").toDouble(),
                                scheduleObject.value("dutycycle").toDouble(),
                                scheduleObject.value("impedance").toDouble(DEFAULT_IMPEDANCE)
                                )));
        else
            m_pxiSchedules[nSlot].insert(
                        startTime,
                        QSharedPointer<PXIScheduleGenerator>(
                            new PXIScheduleGenerator(currentPXISlot, m_hardware, m_logger, startTime, stopTime, PXIScheduleBase::ScheduleActionOff)));
    }

    stopTime = currentTime;
    for (auto thermocoupleObject : cyclogramObject.value("thermocouple").toArray()) {
        QJsonObject scheduleObject = thermocoupleObject.toObject();

        qint64 duration = scheduleObject.value("duration").toString().toULongLong();
        if (scheduleObject.value("duration").isUndefined()
                || scheduleObject.value("enable").isUndefined()
                || ((duration == 0) && scheduleObject.value("enable").toBool()) ) {
            logMessageString("Warning: (Thermocouple) duration or enable attribute not defined or duration==0 not correspond to enable=false");
            continue;
        }

        QDateTime startTime;
        (scheduleObject.value("startTime").isUndefined()) ?
                    (startTime = stopTime) :
                    (startTime = QDateTime::fromString(scheduleObject.value("startTime").toString(), DB_DATETIME_FORMAT));

        stopTime = startTime.addMSecs(duration);
        if (scheduleObject.value("enable").isUndefined() || scheduleObject.value("enable").toBool())
            m_pxiSchedules[nSlot].insert(
                        startTime,
                        QSharedPointer<PXIScheduleThermoCouple>(
                            new PXIScheduleThermoCouple(
                                currentPXISlot,
                                m_hardware,
                                m_logger,
                                scheduleObject.value("pollInterval").toInt(),
                                startTime,
                                stopTime,
                                scheduleObject.value("temperatureMin").toDouble(),
                                scheduleObject.value("temperatureMax").toDouble())));
        else
            m_pxiSchedules[nSlot].insert(
                        startTime,
                        QSharedPointer<PXIScheduleThermoCouple>(
                            new PXIScheduleThermoCouple(currentPXISlot, m_hardware, m_logger, startTime, stopTime, PXIScheduleBase::ScheduleActionOff)));
    }

    //! временно удалено. Работа с СМУ временно отключена. СМУ становится снова обычным источником
    //!parseDio(currentTime, nSlot, currentPXISlot, cyclogramObject);
    //!parseSmu(currentTime, nSlot, currentPXISlot, cyclogramObject);

    // Установка длительности тестирования в данному cлоте
    auto current = m_pxiSchedules.find(nSlot);
    if (!current->isEmpty()) {
        QDateTime start = current->firstKey();
        QDateTime stop = current->lastKey().addMSecs( current->last()->duration() );
        currentPXISlot->setTestTimeFrame(start, stop);
    }

    //! TODO: Предусмотреть вариант, когда циклограмма на столько больная, что не возможно пускать ее в работу
    //! В этом случае уже созданный PXISlot должне быть уничтожен
    if (postParsing())
        logMessageString(QStringLiteral("Error: intersection"));
}

/*void PXIService::parseDio(const QDateTime & currentTime, const quint32 nSlot, const QSharedPointer<PXISlot> & currentPXISlot, const QJsonObject & cyclogramObject)
{
    QDateTime stopTime(currentTime);
    for (auto generatorObject : cyclogramObject.value("dio").toArray()) {
        QJsonObject scheduleObject = generatorObject.toObject();

        qint64 duration = scheduleObject.value("duration").toString().toULongLong();
        if (scheduleObject.value("duration").isUndefined() || (duration == 0)) {
            logMessageString("Warning: (DIO) duration attribute not defined or duration==0");
            continue;
        }

        QDateTime startTime;
        (scheduleObject.value("startTime").isUndefined()) ?
                    (startTime = stopTime) :
                    (startTime = QDateTime::fromString(scheduleObject.value("startTime").toString(), DB_DATETIME_FORMAT));

        stopTime = startTime.addMSecs(duration);

        double voltageLogicDouble = scheduleObject.value("voltageLogic").toDouble();
        Hsdio::LogicVoltage voltageLogic;
        if (voltageLogicDouble == 5.0)
            voltageLogic = Hsdio::v5_0;
        else if (voltageLogicDouble == 3.3)
            voltageLogic = Hsdio::v3_3;
        else if (voltageLogicDouble == 2.5)
            voltageLogic = Hsdio::v2_5;
        else if (voltageLogicDouble == 1.8)
            voltageLogic = Hsdio::v1_8;
        else {
            logMessageString("Warning: (DIO) Voltage Logic is not suitable for any level");
            break;
        }

        QList<PXIScheduleDIO::PinState> pins;
        for (auto pin : scheduleObject.value("pins").toArray()) {
            QJsonObject pinObject = pin.toObject();

            qint32 number = pinObject.value("number").toInt();
            QString modeStr = pinObject.value("mode").toString();
            bool value = pinObject.value("value").toBool();
            Hsdio::PinState mode;
            if (modeStr == "read")
                mode = Hsdio::read;
            else if (modeStr == "write")
                mode = Hsdio::write;
            else
                mode = Hsdio::none;

            pins.append({number, mode, value});
        }

        m_pxiSchedules[nSlot].insert(
                    startTime,
                    QSharedPointer<PXIScheduleDIO>(
                        new PXIScheduleDIO(
                            currentPXISlot,
                            m_hardware,
                            m_logger,
                            scheduleObject.value("pollInterval").toInt(),
                            startTime,
                            stopTime,
                            voltageLogic,
                            pins
                            )));
    }
}

void PXIService::parseSmu(const QDateTime &currentTime, const quint32 nSlot, const QSharedPointer<PXISlot> &currentPXISlot, const QJsonObject &cyclogramObject)
{
    QDateTime stopTime(currentTime);
    for (auto smuObject : cyclogramObject.value("smu").toArray()) {
        QJsonObject scheduleObject = smuObject.toObject();

        qint64 duration = scheduleObject.value("duration").toString().toULongLong();
        quint8 pinNumber = scheduleObject.value("pinNumber").toInt();
        if (scheduleObject.value("duration").isUndefined()
                || scheduleObject.value("enable").isUndefined()
                || ((duration == 0) && scheduleObject.value("enable").toBool())
                ) {
            logMessageString("Warning: (Power Supply) duration or enable attribute not defined or duration==0 not correspond to enable=false");
            continue;
        }

        QDateTime startTime;
        (scheduleObject.value("startTime").isUndefined()) ? // для нулевого элемента это значит "начать прямо сейчас", для остальных "начать с конца предыдущего"
                    (startTime = stopTime) :
                    (startTime = QDateTime::fromString(scheduleObject.value("startTime").toString(), DB_DATETIME_FORMAT));

        stopTime = startTime.addMSecs(duration);
        if (scheduleObject.value("enable").isUndefined() || scheduleObject.value("enable").toBool())
            m_pxiSchedules[nSlot].insert(
                    startTime,
                    QSharedPointer<PXIScheduleSMU>(
                        new PXIScheduleSMU(
                            currentPXISlot,
                            m_hardware,
                            m_switchConnection,
                            m_logger,
                            scheduleObject.value("pollInterval").toInt(),
                            startTime,
                            stopTime,
                            pinNumber,
                            scheduleObject.value("useVoltage").toBool(),
                            scheduleObject.value("voltage").toDouble(), scheduleObject.value("voltageTolerancePlus").toDouble(), scheduleObject.value("voltageToleranceMinus").toDouble(),
                            scheduleObject.value("current").toDouble(), scheduleObject.value("currentTolerancePlus").toDouble()
                            )));
        else
            m_pxiSchedules[nSlot].insert(
                        startTime,
                        QSharedPointer<PXIScheduleSMU>(
                            new PXIScheduleSMU(currentPXISlot, m_hardware, m_switchConnection, m_logger, startTime, stopTime, pinNumber, PXIScheduleBase::ScheduleActionOff)));
    }
}*/

bool PXIService::postParsing()
{
    /*
     * Возможно, что этот метод понадобится, если придется в "ручном" режиме дорабатывать неточности циклограммы
     * Т.е. здесь можно будет дописать в график, допустим, закрывающий "off" для источника/генератора
     * Здесь можно будет дописать открывающие "on", если что-то поменяется в идеологии работы hardware
     * Здесь можно будет дописать закрывающий "off" для всех айтемов графика, после которых пустота в расписании
     */

    //!TODO: добавить проверку логических уровней hsdio для разных слотов в одной группе [0,1,2,3][4,5,6,7]

    for (auto slot = m_pxiSchedules.constBegin(); slot != m_pxiSchedules.constEnd(); ++slot) {
        auto schedule = slot.value();
        for (auto i = schedule.constBegin(); i != schedule.constEnd(); ++i) {
            QDateTime start = i.value()->startTime();
            QDateTime stop = i.value()->stopTime();

            for (auto j = schedule.constBegin(); j != schedule.constEnd(); ++j) {
                bool sameFlag = (i.value()->instanceNumber() == j.value()->instanceNumber()); // специально для того что бы не путать разные PS
                if (i.value() == j.value())
                    continue;
                if (((start > j.value()->startTime()) && (start < j.value()->stopTime()) && sameFlag)
                        || ((stop > j.value()->startTime()) && (stop < j.value()->stopTime()) && sameFlag)
                        || ((start <= j.value()->startTime()) && (stop >= j.value()->stopTime()) && sameFlag)
                        )
                    return false;
            }
        }
    }
    return false;
}
