#include <QVariant>
#include <QSqlQuery>
#include <QSqlError>
#include "PXIDb.h"
#include "config.h"


//! Создать таблицу operation (set/get/log/что-то еще)
//! Создать таблицу sourceDevice (PS/G/TC/DIO/Chamber)
//! DIO пока не добавлял, потому что хрен знает как его добавлять

//! Таблица sourceDevices
//! ps0 = 1
//! ps1 = 2
//! ps2 = 3
//! ps3 = 4
//! gen = 2
//! couple = 3
//! plc = 4
//! dio = 5
//! smu = 6
//! log = 7

//! Таблица operation
//! set = 1 автоматически подразумевает включение устройства
//! off = 2 выключение устройства
//! get = 2 запрос параметров устройства
//! log = 3 просто для логирования

PXIDb::PXIDb(const QPointer<Logger> &logger, const QPointer<Settings> settings, QObject *parent) :
    QObject(parent),
    m_logger(logger),
    m_settings(settings),
    insertQuery(QStringLiteral(
            "INSERT INTO rack_log "
            "(timeStamp, sourceDevice, operation %1) "
            "VALUES ('%2', %3, %4 %5)")),
    createQuery(QStringLiteral(
          "CREATE TABLE IF NOT EXISTS rack_log\n"
          "(\n\t"
            "id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL,\n\t"
            "timeStamp TEXT NOT NULL,\n\t"
            "sourceDevice INTEGER NOT NULL,\n\t"
            "operation INTEGER NOT NULL,\n\t"
            "psVoltage REAL,\n\t"
            "psVPlus   REAL,\n\t"
            "psVMinus  REAL,\n\t"
            "psCurrent REAL,\n\t"
            "psCPlus   REAL,\n\t"
            "psCMinus  REAL,\n\t"
            "psV       REAL,\n\t"
            "psC       REAL,\n\t"
            "psAmplitude REAL,\n\t"
            "generatorOffset  REAL,\n\t"
            "generatorFrequency REAL,\n\t"
            "generatorCycle REAL,\n\t"
            "thermocoupleMin REAL,\n\t"
            "thermocoupleMax REAL,\n\t"
            "thermocoupleTemperature REAL,\n\t"
            "plcAction TEXT,\n\t"
            "plcPreferedtemperature REAL,\n\t"
            "plcTemperatureTolerance REAL,\n\t"
            "plcTemperatureChangeRate REAL,\n\t"
            "plcTemperature REAL,\n\t"
            "smuV REAL,\n\t"
            "smuC REAL,\n\t"
            "comment TEXT\n"
          ");"))
{
    connectToSqlite();
}

PXIDb::~PXIDb()
{
    if (dbSqlite.isOpen())
        dbSqlite.close();
}

bool PXIDb::connectToSqlite()
{
    /* Будем исходить из той идеи, что соединение с sqlite не может быть разорвано ни при каких обстоятельствах */

    QSqlDatabase::removeDatabase(SQLITE);
    dbSqlite = QSqlDatabase::addDatabase("QSQLITE", SQLITE); // эту ф-ю нужно взывать только один раз (ну видимо или использовать .removeDatabase())
    dbSqlite.setDatabaseName(m_settings->selfPath() + "/rack.sqlite");
    if (!dbSqlite.open()) {
        m_logger->log("SQLite database can't open");
        return false;
    }

    // create запрос с проверкой существования, поэтому проверку можно не делать
    QSqlQuery sqlCreate(dbSqlite);
    if (!sqlCreate.exec(createQuery)) {
        m_logger->log("SQLite database can't create table");
        return false;
    }

    return true;
}

void PXIDb::powerSet(const quint8 nPs, const double v, const double vP, const double vM, const double c, const double cP, const double cM)
{
    Devices psN;
    switch (nPs) {
    case 0: psN = PowerSupply0Device; break;
    case 1: psN = PowerSupply1Device; break;
    case 2: psN = PowerSupply2Device; break;
    case 3: psN = PowerSupply3Device; break;
    default: { m_logger->log("Used wrong power supply number"); return; }
    }
    const QString otherFileds(QStringLiteral(", psVoltage, psVPlus, psVMinus, psCurrent, psCPlus, psCMinus"));
    const QString otherValue(QStringLiteral(", %1, %2, %3, %4, %5, %6").arg(v).arg(vP).arg(vM).arg(c).arg(cP).arg(cM));
    const QString valuesString(insertQuery
                         .arg(otherFileds)
                         .arg(QDateTime::currentDateTime().toString(DB_DATETIME_FORMAT_MS))
                         .arg(psN)
                         .arg(SetOperation)
                         .arg(otherValue));

    doInsert(valuesString);
}

void PXIDb::powerGet(const quint8 nPs, const double v, const double c)
{
    Devices psN;
    switch (nPs) {
    case 0: psN = PowerSupply0Device; break;
    case 1: psN = PowerSupply1Device; break;
    case 2: psN = PowerSupply2Device; break;
    case 3: psN = PowerSupply3Device; break;
    default: { m_logger->log("Used wrong power supply number"); return; }
    }
    const QString otherFileds(QStringLiteral(", psV, psC"));
    const QString otherValue(QStringLiteral(", %1, %2").arg(v).arg(c));
    const QString valuesString(insertQuery
                         .arg(otherFileds)
                         .arg(QDateTime::currentDateTime().toString(DB_DATETIME_FORMAT_MS))
                         .arg(psN)
                         .arg(GetOperation)
                         .arg(otherValue));

    doInsert(valuesString);
}

void PXIDb::powerOff(const quint8 nPs)
{
    Devices psN;
    switch (nPs) {
    case 0: psN = PowerSupply0Device; break;
    case 1: psN = PowerSupply1Device; break;
    case 2: psN = PowerSupply2Device; break;
    case 3: psN = PowerSupply3Device; break;
    default: { m_logger->log("Used wrong power supply number"); return; }
    }
    const QString otherFileds;
    const QString otherValue;
    const QString valuesString(insertQuery
                         .arg(otherFileds)
                         .arg(QDateTime::currentDateTime().toString(DB_DATETIME_FORMAT_MS))
                         .arg(psN)
                         .arg(OffOperation)
                         .arg(otherValue));

    doInsert(valuesString);
}

void PXIDb::powerLog(const quint8 nPs, const QString & comment)
{
    Devices psN;
    switch (nPs) {
    case 0: psN = PowerSupply0Device; break;
    case 1: psN = PowerSupply1Device; break;
    case 2: psN = PowerSupply2Device; break;
    case 3: psN = PowerSupply3Device; break;
    default: { m_logger->log("Used wrong power supply number"); return; }
    }
    const QString otherFileds(QStringLiteral(", comment"));
    const QString otherValue(QStringLiteral(", '%1'").arg(comment));
    const QString valuesString(insertQuery
                         .arg(otherFileds)
                         .arg(QDateTime::currentDateTime().toString(DB_DATETIME_FORMAT_MS))
                         .arg(psN)
                         .arg(LogOperation)
                         .arg(otherValue));

    doInsert(valuesString);
}

void PXIDb::smuSet(const double v, const double c)
{
    Devices psN = SMUDevice;
    const QString otherFileds(QStringLiteral(", psVoltage, psCurrent"));
    const QString otherValue(QStringLiteral(", %1, %2").arg(v).arg(c));
    const QString valuesString(insertQuery
                         .arg(otherFileds)
                         .arg(QDateTime::currentDateTime().toString(DB_DATETIME_FORMAT_MS))
                         .arg(psN)
                         .arg(SetOperation)
                         .arg(otherValue));

    doInsert(valuesString);
}

void PXIDb::smuGet(const double v, const double c)
{
    Devices psN = SMUDevice;
    const QString otherFileds(QStringLiteral(", smuV, smuC"));
    const QString otherValue(QStringLiteral(", %1, %2").arg(v).arg(c));
    const QString valuesString(insertQuery
                         .arg(otherFileds)
                         .arg(QDateTime::currentDateTime().toString(DB_DATETIME_FORMAT_MS))
                         .arg(psN)
                         .arg(GetOperation)
                         .arg(otherValue));

    doInsert(valuesString);
}

void PXIDb::smuOff()
{
    Devices psN = SMUDevice;
    const QString otherFileds;
    const QString otherValue;
    const QString valuesString(insertQuery
                         .arg(otherFileds)
                         .arg(QDateTime::currentDateTime().toString(DB_DATETIME_FORMAT_MS))
                         .arg(psN)
                         .arg(OffOperation)
                         .arg(otherValue));

    doInsert(valuesString);
}

void PXIDb::smuLog(const QString & comment)
{
    Devices psN = SMUDevice;
    const QString otherFileds(QStringLiteral(", comment"));
    const QString otherValue(QStringLiteral(", '%1'").arg(comment));
    const QString valuesString(insertQuery
                         .arg(otherFileds)
                         .arg(QDateTime::currentDateTime().toString(DB_DATETIME_FORMAT_MS))
                         .arg(psN)
                         .arg(LogOperation)
                         .arg(otherValue));

    doInsert(valuesString);
}

void PXIDb::powerLog(const QString & comment)
{
    const QString otherFileds(QStringLiteral(", comment"));
    const QString otherValue(QStringLiteral(", %1").arg(comment));
    const QString valuesString(insertQuery
                         .arg(otherFileds)
                         .arg(QDateTime::currentDateTime().toString(DB_DATETIME_FORMAT_MS))
                         .arg(Log)
                         .arg(LogOperation)
                         .arg(otherValue));

    doInsert(valuesString);
}

void PXIDb::logBugs(const Bugs &bugs)
{
    QString concatString;
    for (auto bug : bugs) {
        concatString.append( bug.message + " type = " + QString::number(bug.type) + " slot = " + QString::number(bug.slot) + "; ");
    }
    powerLog(concatString);
}

void PXIDb::doInsert(const QString &insertString)
{
    QSqlQuery sqlInsert(dbSqlite);
    if (!sqlInsert.exec(insertString)) {
        m_logger->log("SQLite database can't insert rows");
        return;
    }
}

