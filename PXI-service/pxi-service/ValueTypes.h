#ifndef VALUETYPES_H
#define VALUETYPES_H

#include <QtGlobal>

//! TODO: Сделать здесь шаблонный класс
struct BoolValue
{
    BoolValue():
        isValid(false),
        value(false)
    {   }

    void setValue(const double v)
    {
        value = v;
        isValid = true;
    }

    bool isValid;           // валидно ли значение или оно здесь просто по умолчанию (ну пока еще не получено от железа)
    bool value;             // значение датчика
};

struct ShortValue
{
    ShortValue():
        isValid(false),
        value(0)
    {   }

    ShortValue(const qint16 v):
        isValid(true),
        value(v)
    {   }

    void setValue(const qint16 v)
    {
        value = v;
        isValid = true;
    }

    bool isValid;          // валидно ли значение или оно здесь просто по умолчанию (ну пока еще не получено от железа)
    qint16 value;          // значение
};

struct DoubleValue
{
    DoubleValue():
        isValid(false),
        value(0.0)
    {   }

    void setValue(const double v)
    {
        isValid = true;
        value = v;
    }

    bool isValid;           // валидно ли значение или оно здесь просто по умолчанию (ну пока еще не получено от железа)
    double value;           // значение датчика
};

struct FloatValue
{
    FloatValue():
        isValid(false),
        value(0.0)
    {   }

    FloatValue(const float v):
        isValid(true),
        value(v)
    {   }

    void setValue(const float v)
    {
        isValid = true;
        value = v;
    }

    bool isValid;           // валидно ли значение или оно здесь просто по умолчанию (ну пока еще не получено от железа)
    float value;            // значение датчика
};

#endif // VALUETYPES_H
