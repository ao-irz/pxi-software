#ifndef PXISERVICE_H
#define PXISERVICE_H
#include <QDateTime>
#include <QCoreApplication>
#include <QTimer>
#include <QPointer>
#include <QProcess>
#include "QLocalServer"
#include "qtservice.h"
#include <PXISlot.h>
#include "Logger.h"
#include "Settings.h"
#include "ValueTypes.h"
#include "config.h"
#include "schedule-components/PXIScheduleBase.h"
#include "plc-connection/PXIPlcConnection.h"
#include "switch-connection/PXISwitchConnection.h"
#include "database/PXIDb.h"
#include "../../PXI-common/common.h"
#include "../../PXI-common/led-color.h"

#ifdef PXI_EMULATOR_ENABLE
#include "../pxi-emulator/PxiEmulator.h"
#else
#include "../warm-pxi/pxi/pxi.h"
#endif

class PXIService : public QtService<QCoreApplication>
{
public:
    PXIService(int argc, char **argv, const QString & m_selfPath);
    virtual ~PXIService();

protected:
    void start() override;
    void stop() override;
    void pause() override;
    void resume() override;
    void processCommand(int code) override;

private:
    enum SocketCommandType
    {
        NoCommand = NO_COMMAND,
        StateRequest = STATE_REQUEST,
        RunCyclogramCommand = RUN_CYCLOGRAM_COMMAND,
        BreakCyclogramCommand =	BREAK_CYCLOGRAM_COMMAND,
        SetRackTemperatureCommand =	SET_RACK_TEMPERATURE_COMMAND,
        AlarmResetCommand = ALARM_RESET_COMMAND,
        SetLedColorCommand = SET_LED_COLOR_COMMAND,
        PauseCommand = PAUSE_COMMAND,
        ResumeCommand = RESUME_COMMAND
    };

    QString m_selfPath;
    QPointer<Settings> m_settings{nullptr};
    QPointer<Logger> m_logger{nullptr};
    QPointer<PXIDb> m_db{nullptr};
    QPointer<QLocalServer> m_localServer{nullptr};
    QPointer<PXIPlcConnection> m_plcConnection{nullptr};
    QPointer<PXISwitchConnection> m_switchConnection{nullptr};
    QPointer<QTimer> m_scheduleTimer{nullptr};              // основной таймер планировщика
    QPointer<QTimer> m_tryResumeTimer{nullptr};             // таймер включается только для контроля завершения паузы
    QCoreApplication * m_serviceApp{nullptr};
    bool m_isPauseOn{false};
    QDateTime m_beginPauseTime;                             // время начала паузы

#ifdef PXI_EMULATOR_ENABLE
    QSharedPointer<PxiEmulator> m_hardware;
#else
    QSharedPointer<Pxi> m_hardware;
#endif

    bool m_socketFlag{false};
    quint8 m_socketCommand{0};
    quint32 m_socketCommandLength{0};

    QHash<quint32, QSharedPointer<PXISlot> > m_slots;                                       // хранит работающие слоты
    QHash<quint32, QMultiMap<QDateTime, QSharedPointer<PXIScheduleBase> > > m_pxiSchedules; // основной объект расписания

    LedColor m_currentLedColor{NoColor};

    void socketHandler(QLocalSocket *socket);
    void cleanSocketCommand();

    void stateRequestHandler(QLocalSocket * socket);
    void runCyclogramCommandHandler(const QByteArray &meat);
    void breakCyclogramCommandHandler(const QByteArray &meat);
    void setRackTemperatureCommandHandler(const QByteArray &meat);
    void alarmResetCommandHandler();
    void setLedColorCommandHandler(const QByteArray &meat);
    void pauseCommandHandler();
    void pauseByTemperatureReason();
    void resumeCommandHandler(const QByteArray &meat);
    void tryResumeTests();

    void emergencyBreakPxiModules(quint32 nSlot);
    void emergencyBreakThermoprofile();

    void disableTests();

    void interruptTest(quint32 nSlot, PXISlot::SlotState state);

    void changeLedColor();

    void parseCyclogram(quint32 nSlot, const QJsonObject &cyclogramObject);
    //! void parseDio(const QDateTime &currentTime, const quint32 nSlot, const QSharedPointer<PXISlot> &currentPXISlot, const QJsonObject &cyclogramObject);
    //! void parseSmu(const QDateTime &currentTime, const quint32 nSlot, const QSharedPointer<PXISlot> &currentPXISlot, const QJsonObject &cyclogramObject);
    void parseCyclogramThermo(const QJsonObject & cyclogramObject);
    bool postParsing();

    void sendResponseState(QLocalSocket * socket);

    void logMessageString(const QString &head);
    void logBugs(const Bugs & bugs);

    void schedulerHandler();

    void swapScheduler(qint64 timeLength);
};

#endif // PXISERVICE_H
