#ifndef PXISWITCHCONNECTION_H
#define PXISWITCHCONNECTION_H

#include <QObject>
#include <QSerialPort>
#include <QPointer>

class Logger;

class PXISwitchConnection : public QObject
{
    Q_OBJECT
public:
    explicit PXISwitchConnection(const QPointer<Logger> &logger, QObject *parent = nullptr);
    ~PXISwitchConnection();

public:
    void switchOn(quint32 n);
    void switchOff(quint32 n);
    void switchOff();


private slots:
    void portHandler();

private:
    QPointer<Logger> m_logger;

    QByteArray m_buffer;
    QPointer<QSerialPort> m_serialPort;
    bool m_endFlag;

    void responseHandler();

};

#endif // PXISWITCHCONNECTION_H
