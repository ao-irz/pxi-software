#ifndef PXISLOT_H
#define PXISLOT_H

#include <QtGlobal>
#include <QDateTime>
#include <QJsonObject>
#include <QPointer>
#include "Logger.h"
#include "../../PXI-common/common.h"
#include "ValueTypes.h"
#include "config.h"

class PXISlot
{
public:
    enum SlotState {
        NoSlotState = NO_SLOT_STATE,                        // вероятно не будет использоваться, потому что пустые слоты будут из массива удаляться
        WaitingForStartState = WAITING_START_SLOT_STATE,
        HardwareErrorSlotState = HARDWARE_ERROR_SLOT_STATE, // особое состояние, для варианта, когда произошла ошибка pxi
        NormalSlotState = NORMAL_SLOT_STATE,
        PauseSlotState = PAUSE_SLOT_STATE,                  // специальное состояние, когда крышку термокамеры открыли
        //WarningSlotState = WARNING_SLOT_STATE,              // возможно не понадобится
        AlarmSlotState = ALARM_SLOT_STATE,                  // состояние выставляется, если есть выходы из допуска по V,I,chipT,(G)
        TestCompletedState = TEST_COMPLETED_SLOT_STATE      // конечное состояние ЖЦ слота
    };

    PXISlot(QPointer<Logger> & logger, const qint32 nSlot, const QString & chipsName, const QString & chipsBatch) :
        m_logger(logger),
        m_slotNumber(nSlot),
        m_chipsName(chipsName),
        m_chipsBatch(chipsBatch),
        m_state(WaitingForStartState)
    {    }

    quint32 number() const
    { return m_slotNumber; }

    QJsonObject stateToJson() const;

    void setTestTimeFrame(const QDateTime & start, const QDateTime & stop);
    void extendOutputTime(qint64 extendTimeMs);

    void setChipVoltage(quint32 nPowerSupply, double v, double min, double max);
    void setChipCurrent(quint32 nPowerSupply, double c, double min, double max);
    void disableVoltageCurrent(quint32 nPowerSupply);
    void setChipTemperature(double t, double max);
    void disableTemperature();
    void setGeneratorOk(bool f);
    void setGeneratorInfo(double amplitude, double frequency, double dutycycle, double impedance);
    void disableGenerator();
    void setHsdioState(bool ok, double logic, quint32 hex);
    void disableHsdio();
    void setSmuCurrent(double c, double max);
    void setSmuVoltage(double v, bool useVoltage, double min, double max);
    void disableSmu();

    void completeTest();
    void harwareErrorTest();

    SlotState state() const
    { return m_state; }

    void pauseTest();
    void resumeTest();

private:
    QPointer<Logger> m_logger;

    quint32 m_slotNumber;
    QString m_chipsName;
    QString m_chipsBatch;
    QDateTime m_chipsDateInput;
    QDateTime m_chipsDateOutput;
    QDateTime m_lastDateOutput;

    QDateTime m_emergencyStopTime;                          // время остановки теста. актуально, когда слот влетел в Alarm или HWError

    SlotState m_state;

    DoubleValue m_chipsVoltage[POWER_SUPPLY_NUMBER];        // текущая напряжение устройства
    double m_voltageMin[POWER_SUPPLY_NUMBER];               // максимально допустимое напряжение для устройства
    double m_voltageMax[POWER_SUPPLY_NUMBER];               // минимально допустимое напряжение для устройства

    DoubleValue m_chipsCurrent[POWER_SUPPLY_NUMBER];        // текущий ток устройства
    double m_currentMin[POWER_SUPPLY_NUMBER];               // максимально допустимый ток для тестируемого устройства
    double m_currentMax[POWER_SUPPLY_NUMBER];               // минимально допустимый ток для тестируемого устройства

    DoubleValue m_chipsTemperature;                         // температура самого чипа, снятая термопарой
    double m_chipsTemperatureMax;                            // минимально допустимая
    ShortValue m_chipsTemperatureSensorOk;                  // флаг исправности термопары

    BoolValue m_generatorOk;
    DoubleValue m_generatorAmplitude;
    DoubleValue m_generatorFrequency;
    DoubleValue m_generatorDutycycle;
    DoubleValue m_generatorImpedance;

    BoolValue m_hsdioOk;
    double m_hsdioVoltageLogic;
    quint32 m_hsdioHex;

    DoubleValue m_smuCurrent;
    double m_smuCurrentMax;

    DoubleValue m_smuVoltage;
    double m_smuVoltageMin;
    double m_smuVoltageMax;
    bool m_useVoltage{false};

    void syncState(); // должна вызываться из любой ф-ии, изменяющей поля данного класса
};



#endif // PXISLOT_H
