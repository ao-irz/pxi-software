#ifndef PXISCHEDULEPLC_H
#define PXISCHEDULEPLC_H

#include "config.h"
#include "../plc-connection/PXIPlcConnection.h"
#include "PXIScheduleBase.h"

class PXISchedulePlc : public PXIScheduleBase
{
public:
    enum ThermoAction {
        HeatingType,
        MaintainingType,
        CoolingType
    };

    PXISchedulePlc() = delete;
    PXISchedulePlc(const QPointer<PXIPlcConnection> &hardware,
            const QPointer<Logger> &logger,
            const QDateTime &start, const QDateTime &stop,
            double temperature, double chanegeRate, const QString &thermoAction, const bool isInfinite);

    PXISchedulePlc(const QPointer<PXIPlcConnection> &hardware,
            const QPointer<Logger> &logger,
            const QDateTime &start, const QDateTime &stop,
            PXIScheduleBase::ScheduleAction action, const bool isInfinite);


    void execSchedule() override;

private:
    QPointer<PXIPlcConnection> m_hardware;

    ThermoAction m_thermoAction{MaintainingType};
    const float m_preferedtemperature{0.0};
    const float m_temperatureTolerance{TEMPERATURE_TOLERANCE};
    const float m_temperatureChangeRate{0.0};

    void pollHardware() override;
    void latestPollHardware() override;
};

#endif // PXISCHEDULEPLC_H
