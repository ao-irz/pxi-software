#ifndef PXISCHEDULEPOWERSUPPLY_H
#define PXISCHEDULEPOWERSUPPLY_H

#include "PXIScheduleEquipment.h"

class PXISchedulePowerSupply : public PXIScheduleEquipment
{
public:
    PXISchedulePowerSupply() = delete;
    PXISchedulePowerSupply(const QSharedPointer<PXISlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
            const QSharedPointer<PxiEmulator> &hardware,
#else
            const QSharedPointer<Pxi> &hardware,
#endif
            const QPointer<Logger> &logger, const qint64 pollIntervall,
            const QDateTime & start, const QDateTime &stop,
            quint32 number,
            double v, double ptv, double mtv, double c, double ptc, double mtc);

    PXISchedulePowerSupply(const QSharedPointer<PXISlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
            const QSharedPointer<PxiEmulator> &hardware,
#else
            const QSharedPointer<Pxi> &hardware,
#endif
            const QPointer<Logger> &logger,
            const QDateTime & start, const QDateTime &stop,
            quint8 number,
            ScheduleAction action);

    void execSchedule() override;

    quint32 instanceNumber() const override;

private:
    quint32 m_number;

#ifdef PXI_EMULATOR_ENABLE
    QSharedPointer<PxiEmulator> m_hardware;
#else
    QSharedPointer<Pxi> m_hardware;
#endif

    const double m_voltage{0.0};                // иметь ввиду, что это просто номинал. а для контроля работы нужны m_voltageMin m_voltageMax
    const double m_plusToleranceVoltage{0.0};   // иметь ввиду, что это просто относительное смещение верхнее
    const double m_minusToleranceVoltage{0.0};  // иметь ввиду, что это просто относительное смещение нижнее
    const double m_voltageMin{0.0};
    const double m_voltageMax{0.0};

    const double m_current{0.0};                // оганичение по току. иметь ввиду, что это просто номинал. а для контроля работы нужны m_currentMin m_currentMax
    const double m_plusToleranceCurrent{0.0};   // иметь ввиду, что это просто относительное смещение верхнее
    const double m_minusToleranceCurrent{0.0};  // иметь ввиду, что это просто относительное смещение нижнее
    const double m_currentMin{0.0};
    const double m_currentMax{0.0};

    void pollHardware() override;    
};

#endif // PXISCHEDULEPOWERSUPPLY_H
