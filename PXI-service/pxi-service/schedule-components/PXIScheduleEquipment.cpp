#include "PXIScheduleEquipment.h"

PXIScheduleEquipment::PXIScheduleEquipment(const QSharedPointer<PXISlot> &pxiSlot,
        const QPointer<Logger> &logger,
        const qint64 pollIntervall,
        const QDateTime &start,
        const QDateTime &stop,
        const PXIScheduleBase::ScheduleAction action) :
    PXIScheduleBase(logger, pollIntervall, start, stop, action),
    m_slot(pxiSlot),
    m_slotNumber(pxiSlot->number())
{

}
