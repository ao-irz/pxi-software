#ifndef PXISCHEDULEDIO_H
#define PXISCHEDULEDIO_H

#include "PXIScheduleEquipment.h"

//! Особенностью данного наследника Base является то, что здесь сделана попытка отказаться от On/Off,
//! что дает возможность отказаться от второго конструктора
//! Но необходимо делать в latestPollHardware() отсылку в Slot отключения анализа dio
//! Еще особенность такая, что в pollHardware() (в отличие от других таких же наследников)
//! не передаются в Slot вместе с текущим значением еще и рамки допустимого интервала
//! а сразу передается готовое решение (true/false).
//! Ну еще передается m_allPins, но на самом деле пока в работу ее брать не гоже. Работа с ней сделана не очень красиво
//!
//! Видимо смысл работы данного моудля в том, что на ножки, сконфигурированные как write только подается какой-то сигнал
//! а контролируются только ножки, сконфигурированные как read
//!
//! Кстати, по включению стойки, релюхи скроссированы на hsdio, то есть если требуется smu, то нужно делать вызов toSMU()
//!

class PXIScheduleDIO : public PXIScheduleEquipment
{
public:
    struct PinState {
        qint32 number;
        Hsdio::PinState direction;
        bool value;
    };

    PXIScheduleDIO() = delete;
    PXIScheduleDIO(const QSharedPointer<PXISlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
            const QSharedPointer<PxiEmulator> &hardware,
#else
            const QSharedPointer<Pxi> &hardware,
#endif
            const QPointer<Logger> &logger, const quint32 pollIntervall,
            const QDateTime & start, const QDateTime &stop,
            Hsdio::LogicVoltage voltageLogic,
            const QList<PXIScheduleDIO::PinState> & pins);

    void execSchedule() override;

private:
#ifdef PXI_EMULATOR_ENABLE
    QSharedPointer<PxiEmulator> m_hardware;
#else
    QSharedPointer<Pxi> m_hardware;
#endif

    const Hsdio::LogicVoltage m_voltageLogic{Hsdio::v1_8};
    QList<PinState> m_pins{
        PinState{0,Hsdio::none,false}, PinState{1,Hsdio::none,false},
        PinState{2,Hsdio::none,false}, PinState{3,Hsdio::none,false},
        PinState{4,Hsdio::none,false}, PinState{5,Hsdio::none,false},
        PinState{6,Hsdio::none,false}, PinState{7,Hsdio::none,false}
    };
    quint32 m_allPins;
    double m_logic{0.0};

    void pollHardware() override;    
    void latestPollHardware() override;
};

#endif // PXISCHEDULEDIO_H
