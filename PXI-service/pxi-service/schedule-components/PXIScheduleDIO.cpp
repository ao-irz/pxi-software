#include "config.h"
#include "PXIScheduleDIO.h"

PXIScheduleDIO::PXIScheduleDIO(
        const QSharedPointer<PXISlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
        const QSharedPointer<PxiEmulator> &hardware,
#else
        const QSharedPointer<Pxi> &hardware,
#endif
        const QPointer<Logger> &logger,
        const quint32 pollIntervall,
        const QDateTime &start, const QDateTime &stop,
        Hsdio::LogicVoltage voltageLogic,
        const QList<PXIScheduleDIO::PinState> & pins) :
    PXIScheduleEquipment(pxiSlot, logger, pollIntervall, start, stop, PXIScheduleBase::ScheduleActionNoNeed),
    m_hardware(hardware),
    m_voltageLogic(voltageLogic)
{
    switch (m_voltageLogic){
    case Hsdio::v5_0: {m_logic = 5.0;} break;
    case Hsdio::v3_3: {m_logic = 3.3;} break;
    case Hsdio::v2_5: {m_logic = 2.5;} break;
    case Hsdio::v1_8: {m_logic = 1.8;} break;
//    default: m_logic = 0;
    }

    for (const auto & p : pins) {
        if ((p.number >= 0) && (p.number < HSDIO_PINS_NUMBER))
            m_pins.replace(p.number, p);
    }

    for (const auto & p : m_pins) {
        m_allPins = m_allPins + (((int)p.value) << p.number);
    }
}

void PXIScheduleDIO::execSchedule()
{
    if (m_stage != ScheduleWaitingStart)
        return;

    Bugs bugs;
    try {
        m_logger->log(QString("Set new hsdio pin's value: %1 %2 %3 %4 %5 %6 %7 %8").arg(m_pins.at(0).number).arg(m_pins.at(1).number).arg(m_pins.at(2).number).arg(m_pins.at(3).number).arg(m_pins.at(4).number).arg(m_pins.at(5).number).arg(m_pins.at(6).number).arg(m_pins.at(7).number));
        m_logger->log(QString("Set new hsdio pin's direction: %1 %2 %3 %4 %5 %6 %7 %8").arg(m_pins.at(0).direction).arg(m_pins.at(1).direction).arg(m_pins.at(2).direction).arg(m_pins.at(3).direction).arg(m_pins.at(4).direction).arg(m_pins.at(5).direction).arg(m_pins.at(6).direction).arg(m_pins.at(7).direction));
        m_logger->log(QString("Set new hsdio pin's value: %1 %2 %3 %4 %5 %6 %7 %8").arg(m_pins.at(0).value).arg(m_pins.at(1).value).arg(m_pins.at(2).value).arg(m_pins.at(3).value).arg(m_pins.at(4).value).arg(m_pins.at(5).value).arg(m_pins.at(6).value).arg(m_pins.at(7).value));
        auto hsdio = m_hardware->sample(m_slotNumber)->hsdio;

        for (const auto & p : m_pins)
            m_hardware->sample(m_slotNumber)->relay->toDIO(p.number);

        bugs += hsdio->init4Slots(m_voltageLogic);
        for (const auto & p : m_pins) {
            bugs += hsdio->initPin(p.number, p.direction);
            if (p.direction == Hsdio::write)
                bugs += hsdio->put(p.number, p.value);
        }

    } catch (PxiException & e) {
        m_stage = ScheduleError;
        m_slot->harwareErrorTest();
        m_logger->log(e.msg);
        return;
    }

    if (!bugs.isEmpty()) {
        m_stage = ScheduleError;
        m_slot->harwareErrorTest();
        logBugs(bugs);
        return;
    }

    if (m_startTime == m_stopTime) {
        m_logger->log("Schedule's task (hsdio) completed at once");
        m_stage = ScheduleCompleted;
        return;
    }

    m_stage = ScheduleRun;
    startAliveTimer();
    startPollTimer();
}

void PXIScheduleDIO::pollHardware()
{
    // Эта проверка под сомнением. Т.е. нужно ли продолжать запросы состояния в данном таске, если произошла hardware ошибка
    if ((m_stage != ScheduleRun) && (m_stage != SchedulePause))
        return;

    Bugs bugs;
    try {
        auto hsdio = m_hardware->sample(m_slotNumber)->hsdio;
        for (const auto & p : m_pins) {
            if (p.direction == Hsdio::read) {
                bool value;
                bugs += hsdio->get(p.number, &value);
                if (value != p.value) {
                    m_logger->log(QStringLiteral("Slot=") + QString::number(m_slotNumber+1) + QString("Level of %1 pins mismatch right value (%2)").arg(p.number).arg(p.value));
                    m_slot->setHsdioState(false, m_logic, value << p.number);
                    break;
                } else {
                    m_slot->setHsdioState(true, m_logic, m_allPins);
                }
            }
        }
    } catch (PxiException & e) {
        m_stage = ScheduleError;
        m_slot->harwareErrorTest();
        m_logger->log(e.msg);
        return;
    }

    if (!bugs.isEmpty()) {
        m_stage = ScheduleError;
        m_slot->harwareErrorTest();
        logBugs(bugs);
        return;
    }
}

void PXIScheduleDIO::latestPollHardware()
{
    m_slot->disableHsdio();
}
