#ifndef PXISCHEDULESMU_H
#define PXISCHEDULESMU_H

#include "PXIScheduleEquipment.h"
#include "../switch-connection/PXISwitchConnection.h"

/*
 * Скопировано с PowerSupply
 * Отличие в том, что СМУ работать в двух режимах: "с подачей напряжения" и "без подачи"
 * Контроль напряжения производится только для режима "с подачей напряжения"
 * Контроль тока производится для обоих режимов, (но по сути только на КЗ, что возможно никогда не случится)
 * Надо логгировать и отображать в UI
 * Правда правда syncState в Slot для SMU отключен
 *
 * I вводится пользователем через циклограмму, как и [+/-]V как и [+]I
 *
 * useVoltage - определяет режим работы
 *
 * Кстати, по включению стойки, релюхи скроссированы на hsdio, то есть если требуется smu, то нужно делать вызов toSMU()
*/

class PXIScheduleSMU : public PXIScheduleEquipment
{
public:
    PXIScheduleSMU() = delete;
    PXIScheduleSMU(const QSharedPointer<PXISlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
            const QSharedPointer<PxiEmulator> &hardware,
#else
            const QSharedPointer<Pxi> &hardware,
#endif
            const QPointer<PXISwitchConnection> &switchConnection,
            const QPointer<Logger> &logger, const quint32 pollIntervall,
            const QDateTime & start, const QDateTime &stop, quint8 pinNumber, bool useVoltage,
            double v, double ptv, double mtv, double c, double ptc);

    PXIScheduleSMU(const QSharedPointer<PXISlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
            const QSharedPointer<PxiEmulator> &hardware,
#else
            const QSharedPointer<Pxi> &hardware,
#endif
            const QPointer<PXISwitchConnection> &switchConnection,
            const QPointer<Logger> &logger,
            const QDateTime & start, const QDateTime &stop, quint8 pinNumber,
            ScheduleAction action);

    void execSchedule() override;
    void latestPollHardware() override;

private:

#ifdef PXI_EMULATOR_ENABLE
    QSharedPointer<PxiEmulator> m_hardware;
#else
    QSharedPointer<Pxi> m_hardware;
#endif

    QPointer<PXISwitchConnection> m_switchConnection;

    const bool m_useVoltage{false};             // useVoltage определяет в каком режиме мы работаем; true - с подачей напряжения; false - без подачи
    const quint8 m_pinNumber{0};
    const double m_voltage{0.0};                // иметь ввиду, что это просто номинал. а для контроля работы нужны m_voltageMin m_voltageMax
    const double m_plusToleranceVoltage{0.0};   // иметь ввиду, что это просто относительное смещение верхнее
    const double m_minusToleranceVoltage{0.0};  // иметь ввиду, что это просто относительное смещение нижнее
    const double m_voltageMin{0.0};
    const double m_voltageMax{0.0};

    const double m_current{0.0};                //! СМУ занимается тем, что очень точно измеряет ток. Этот параметр будет просто базой для отсчета max тока (КЗ) для визуальной совместимости с PS
    const double m_plusToleranceCurrent{0.0};
    const double m_currentMax{0.0};

    void pollHardware() override;

    void switchRelay(bool switchOn);
};

#endif // PXISCHEDULESMU_H
