#include "config.h"
#include "PXIScheduleThermoCouple.h"

PXIScheduleThermoCouple::PXIScheduleThermoCouple(const QSharedPointer<PXISlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
        const QSharedPointer<PxiEmulator> &hardware,
#else
        const QSharedPointer<Pxi> &hardware,
#endif
        const QPointer<Logger> &logger,
        const quint32 pollIntervall,
        const QDateTime &start, const QDateTime &stop, double chipTemperatureMin, double chipTemperatureMax) :
    PXIScheduleEquipment(pxiSlot, logger, pollIntervall, start, stop, PXIScheduleBase::ScheduleActionNoNeed),
    m_hardware(hardware),
    m_chipTemperatureMin(chipTemperatureMin),
    m_chipTemperatureMax(chipTemperatureMax)
{
}

PXIScheduleThermoCouple::PXIScheduleThermoCouple(
        const QSharedPointer<PXISlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
        const QSharedPointer<PxiEmulator> &hardware,
#else
        const QSharedPointer<Pxi> &hardware,
#endif
        const QPointer<Logger> &logger,
        const QDateTime &start, const QDateTime &stop,
        PXIScheduleBase::ScheduleAction action) :
    PXIScheduleEquipment(pxiSlot, logger, 0, start, stop, action),
    m_hardware(hardware)
{
}

void PXIScheduleThermoCouple::execSchedule()
{
    if (m_stage != ScheduleWaitingStart)
        return;

    if ((m_action == ScheduleActionOn) || (m_action == ScheduleActionNoNeed)) {
        m_stage = ScheduleRun;
        startAliveTimer();
        startPollTimer();
    } else {
        m_slot->disableTemperature(); // на время выключения термопар syncState не должен играть роли да и отображение в UI должно пропасть
        if (m_startTime == m_stopTime) {
            m_stage = ScheduleCompleted;
            m_logger->log("Schedule's task completed at once");
        } else {
            m_stage = ScheduleRun;
            startAliveTimer();
        }
    }
}

void PXIScheduleThermoCouple::pollHardware()
{
    // Эта проверка под сомнением. Т.е. нужно ли продолжать запросы состояния в данном таске, если произошла hardware ошибка
    if ((m_stage != ScheduleRun) && (m_stage != SchedulePause))
        return;

    Bugs bugs;
    try {
        auto thermoCouple = m_hardware->sample(m_slotNumber)->tcouple;
        bugs += thermoCouple->get(&m_temperature);
        m_logger->log(QStringLiteral("Termocouple T = ") + QString::number(m_temperature));

        m_slot->setChipTemperature(m_temperature, m_chipTemperatureMax);
    } catch (PxiException & e) {
        m_stage = ScheduleError;
        m_slot->harwareErrorTest();
        m_logger->log(e.msg);
        return;
    }

    if (!bugs.isEmpty()) {
        m_stage = ScheduleError;
        m_slot->harwareErrorTest();
        logBugs(bugs);
    }
}


void PXIScheduleThermoCouple::latestPollHardware()
{
    /// Это сделано на тот случай, если после блока работающей термопары уже не будет никаких других блоков
    /// и закрывающего блока тоже не окажется.... поэтому нужно заставить слот уже не анализировать температуру
    m_slot->disableTemperature(); // на время выключения термопар syncState не должен играть роли да и отображение в UI должно пропасть
}
