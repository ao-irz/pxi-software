#ifndef PXISCHEDULEGENERATOR_H
#define PXISCHEDULEGENERATOR_H

#include "PXIScheduleEquipment.h"

class PXIScheduleGenerator : public PXIScheduleEquipment
{
public:
    enum SignalForm {
        SinusForm,
        SquareForm,
        TriangleForm
    };

    PXIScheduleGenerator() = delete;
    PXIScheduleGenerator(const QSharedPointer<PXISlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
            const QSharedPointer<PxiEmulator> &hardware,
#else
            const QSharedPointer<Pxi> &hardware,
#endif
            const QPointer<Logger> &logger,
            const quint32 pollIntervall,
            const QDateTime & start, const QDateTime &stop,
            const QString & signalForm, double a, double o, double f, double d, double i);

    PXIScheduleGenerator(const QSharedPointer<PXISlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
            const QSharedPointer<PxiEmulator> &hardware,
#else
            const QSharedPointer<Pxi> &hardware,
#endif
            const QPointer<Logger> &logger,
            const QDateTime & start, const QDateTime &stop,
            ScheduleAction action = ScheduleActionOff);

    SignalForm toSiganlForm(const QString & string);
    GenForm::GenForm toSiganlForm(SignalForm signalForm);

    void execSchedule() override;

private:
#ifdef PXI_EMULATOR_ENABLE
    QSharedPointer<PxiEmulator> m_hardware;
#else
    QSharedPointer<Pxi> m_hardware;
#endif


    // устанавливаемые параметры generator
    SignalForm m_signalForm;
    const double m_amplitude{0.0};
    const double m_offset{0.0};
    const double m_frequency{0.0};
    const double m_dutycycle{0.0};
    const double m_impedance{DEFAULT_IMPEDANCE}; //1000000 - высокоомная нагрузка; -1 - (50 Ом) низкоомная нагрузка

    void pollHardware() override;
};

#endif // PXISCHEDULEGENERATOR_H
