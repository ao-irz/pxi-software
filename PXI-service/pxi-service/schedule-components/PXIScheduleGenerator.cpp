#include "PXIScheduleGenerator.h"


PXIScheduleGenerator::PXIScheduleGenerator(
        const QSharedPointer<PXISlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
        const QSharedPointer<PxiEmulator> &hardware,
#else
        const QSharedPointer<Pxi> &hardware,
#endif
        const QPointer<Logger> &logger,
        const quint32 pollIntervall,
        const QDateTime &start, const QDateTime &stop,
        const QString &signalForm, double a, double o, double f, double d, double i) :
    PXIScheduleEquipment(pxiSlot, logger, pollIntervall, start, stop, PXIScheduleBase::ScheduleActionOn),
    m_hardware(hardware),
    m_signalForm( toSiganlForm(signalForm) ),
    m_amplitude(a),
    m_offset(o),
    m_frequency(f),
    m_dutycycle(d),
    m_impedance((i == DEFAULT_IMPEDANCE) ? DEFAULT_IMPEDANCE : -1.0)
{   }

PXIScheduleGenerator::PXIScheduleGenerator(
        const QSharedPointer<PXISlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
        const QSharedPointer<PxiEmulator> &hardware,
#else
        const QSharedPointer<Pxi> &hardware,
#endif
        const QPointer<Logger> &logger,
        const QDateTime &start, const QDateTime &stop,
        PXIScheduleBase::ScheduleAction action) :
    PXIScheduleEquipment(pxiSlot, logger, 0, start, stop, action),
    m_hardware(hardware)
{   }

PXIScheduleGenerator::SignalForm PXIScheduleGenerator::toSiganlForm(const QString &string)
{
    if (string == "sinus")
        return SinusForm;
    if (string == "square")
        return SquareForm;
    if (string == "triangle")
        return TriangleForm;

    return SinusForm;
}

GenForm::GenForm PXIScheduleGenerator::toSiganlForm(PXIScheduleGenerator::SignalForm signalForm)
{
    switch (signalForm) {
    case SinusForm:
        return GenForm::sinus;
    case SquareForm:
        return GenForm::square;
    case TriangleForm:
        return GenForm::triangle;
    default:
        return GenForm::sinus;
    }
}

void PXIScheduleGenerator::execSchedule()
{
    if (m_stage != ScheduleWaitingStart)
        return;

    Bugs bugs;
    try {
#ifdef PXI_EMULATOR_ENABLE
        SampleEmulator * sample = m_hardware->sample(m_slotNumber);
#else
        Sample * sample = m_hardware->sample(m_slotNumber);
#endif
        if ((m_action == ScheduleActionOn) || (m_action == ScheduleActionNoNeed)) {
            m_logger->logGenerator(m_slotNumber+1, m_amplitude, m_offset, m_frequency, m_frequency, m_impedance);
            bugs += sample->fgen->putLoadImpedance(m_impedance);
            bugs += sample->fgen->put(toSiganlForm(m_signalForm), m_amplitude, m_offset, m_frequency, m_dutycycle);

            if (!sample->fgen->isOn()) {
                m_logger->logGeneratorSwitch(m_slotNumber+1, true);
                bugs += sample->fgen->on();
            }

        } else {
            m_logger->logGeneratorSwitch(m_slotNumber+1, false);
            bugs += sample->fgen->off();
        }
    } catch (PxiException & e) {
        m_stage = ScheduleError;
        m_slot->harwareErrorTest();
        m_logger->log(e.msg);
        return;
    }

    if (!bugs.isEmpty()) {
        m_stage = ScheduleError;
        m_slot->harwareErrorTest();
        logBugs(bugs);
        return;
    }

    if ((m_action == ScheduleActionOn) || (m_action == ScheduleActionNoNeed)) {
        m_stage = ScheduleRun;
        m_slot->setGeneratorInfo(m_amplitude, m_frequency, m_dutycycle, m_impedance);
        startAliveTimer();
        startPollTimer();
    } else {
        m_slot->disableGenerator(); // на время выключения PS syncState не должен играть роли да и отображение в UI должно пропасть
        if (m_startTime == m_stopTime) {
            m_stage = ScheduleCompleted;
            m_logger->log("Schedule's task completed at once");
        } else {
            m_stage = ScheduleRun;
            startAliveTimer();
        }
    }
}

void PXIScheduleGenerator::pollHardware()
{
    // Эта проверка под сомнением. Т.е. нужно ли продолжать запросы состояния в данном таске, если произошла hardware ошибка
    if ((m_stage != ScheduleRun) && (m_stage != SchedulePause))
        return;

    //! TODO: Пока совершенно не ясно как контроллировать генератор
    //!
    //! TODO: Соответственно, пока генератор не может повлиять на состояние слота НИКАК
    //!       Но вот ниже примерный образец, как это можно было бы реализовать

    m_slot->setGeneratorOk(true);
}
