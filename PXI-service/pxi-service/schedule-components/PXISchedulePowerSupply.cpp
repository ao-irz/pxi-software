#include "config.h"
#include "PXISchedulePowerSupply.h"
#include "database/PXIDb.h"

PXISchedulePowerSupply::PXISchedulePowerSupply(
        const QSharedPointer<PXISlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
        const QSharedPointer<PxiEmulator> &hardware,
#else
        const QSharedPointer<Pxi> &hardware,
#endif
        const QPointer<Logger> &logger,
        const qint64 pollIntervall,
        const QDateTime &start, const QDateTime &stop,
        quint32 number,
        double v, double ptv, double mtv, double c, double ptc, double mtc) :
    PXIScheduleEquipment(pxiSlot, logger, pollIntervall, start, stop, PXIScheduleBase::ScheduleActionOn),
    m_number(number),
    m_hardware(hardware),
    //!m_voltage(((m_number == NEGATIVE_POWER_SUPPLY_NUMBER) && (v > 0.0)) ? (-1 * v) : (v)), //! временно удалено. Работа с СМУ временно отключена. СМУ становится снова обычным источником
    m_voltage(v),
    m_plusToleranceVoltage(ptv),
    m_minusToleranceVoltage(mtv),
    m_voltageMin(m_voltage - m_minusToleranceVoltage),
    m_voltageMax(m_voltage + m_plusToleranceVoltage),
    m_current(c),
    m_plusToleranceCurrent(ptc),
    m_minusToleranceCurrent(mtc),
    m_currentMin(m_current - m_minusToleranceCurrent),
    m_currentMax(m_current + m_plusToleranceCurrent)
{
}

PXISchedulePowerSupply::PXISchedulePowerSupply(
        const QSharedPointer<PXISlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
        const QSharedPointer<PxiEmulator> &hardware,
#else
        const QSharedPointer<Pxi> &hardware,
#endif
        const QPointer<Logger> &logger,
        const QDateTime &start, const QDateTime &stop,
        quint8 number,
        PXIScheduleBase::ScheduleAction action) :
    PXIScheduleEquipment(pxiSlot, logger, 0, start, stop, action),
    m_number(number),
    m_hardware(hardware)
{
}

void PXISchedulePowerSupply::execSchedule()
{
    if (m_stage != ScheduleWaitingStart)
        return;

    Bugs bugs;
    try {
        auto powerSupply = m_hardware->sample(m_slotNumber)->power(m_number);
        if ((m_action == ScheduleActionOn) || (m_action == ScheduleActionNoNeed)) {

            m_logger->logPowerSupply(m_slotNumber+1, m_number, m_voltage, m_current);
            bugs += powerSupply->put(m_voltage, m_current);
            PXIDb::getInstance()->powerSet(m_number, m_voltage, m_plusToleranceVoltage, m_minusToleranceVoltage, m_current, m_plusToleranceCurrent, m_minusToleranceCurrent);

            if (!powerSupply->isOn()) {
                m_logger->logPowerSupplySwitch(m_slotNumber+1, m_number, true);
                bugs += powerSupply->on();
            }
        } else {
            m_logger->logPowerSupplySwitch(m_slotNumber+1, m_number, false);
            bugs += powerSupply->off();
            PXIDb::getInstance()->powerOff(m_number);
        }
    } catch (PxiException & e) {
        m_stage = ScheduleError;
        m_slot->harwareErrorTest();
        m_logger->log(e.msg);
        PXIDb::getInstance()->powerLog(m_number, e.msg);
        return;
    }

    if (!bugs.isEmpty()) {
        m_stage = ScheduleError;
        m_slot->harwareErrorTest();
        logBugs(bugs);
        PXIDb::getInstance()->logBugs(bugs);
        return;
    }

    if ((m_action == ScheduleActionOn) || (m_action == ScheduleActionNoNeed)) {
        m_stage = ScheduleRun;
        startAliveTimer();
        startPollTimer();
    } else {
        m_slot->disableVoltageCurrent(m_number); // на время выключения PS syncState не должен играть роли да и отображение в UI должно пропасть
        if (m_startTime == m_stopTime) {
            m_stage = ScheduleCompleted;
            m_logger->log("Schedule's task completed at once");
        } else {
            m_stage = ScheduleRun;
            startAliveTimer();
        }
    }
}

quint32 PXISchedulePowerSupply::instanceNumber() const
{
    return m_number;
}

void PXISchedulePowerSupply::pollHardware()
{
    // Эта проверка под сомнением. Т.е. нужно ли продолжать запросы состояния в данном таске, если произошла hardware ошибка
    if ((m_stage != ScheduleRun) && (m_stage != SchedulePause))
        return;

    Bugs bugs;
    try {
        auto powerSupply = m_hardware->sample(m_slotNumber)->power(m_number);
        double v;
        double c;
        bugs += powerSupply->voltage(&v);
        bugs += powerSupply->current(&c);
        m_logger->log(QStringLiteral("%1 Measure V = %2 I = %3").arg(m_number).arg(v).arg(c));
        PXIDb::getInstance()->powerGet(m_number, v, c);

        m_slot->setChipVoltage(m_number, v, m_voltageMin, m_voltageMax);
        m_slot->setChipCurrent(m_number, c, m_currentMin, m_currentMax);
    } catch (PxiException & e) {
        m_stage = ScheduleError;
        m_slot->harwareErrorTest();
        m_logger->log(e.msg);
        return;
    }

    if (!bugs.isEmpty()) {
        m_stage = ScheduleError;
        m_slot->harwareErrorTest();
        logBugs(bugs);
    }
}
