#ifndef PXISCHEDULETHERMOCOUPLE_H
#define PXISCHEDULETHERMOCOUPLE_H

#include "PXIScheduleEquipment.h"

class PXIScheduleThermoCouple : public PXIScheduleEquipment
{
public:
    PXIScheduleThermoCouple() = delete;
    PXIScheduleThermoCouple(const QSharedPointer<PXISlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
            const QSharedPointer<PxiEmulator> &hardware,
#else
            const QSharedPointer<Pxi> &hardware,
#endif
            const QPointer<Logger> &logger, const quint32 pollIntervall,
            const QDateTime & start, const QDateTime &stop,
            double chipTemperatureMin, double chipTemperatureMax);

    PXIScheduleThermoCouple(const QSharedPointer<PXISlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
            const QSharedPointer<PxiEmulator> &hardware,
#else
            const QSharedPointer<Pxi> &hardware,
#endif
            const QPointer<Logger> &logger,
            const QDateTime & start, const QDateTime &stop,
            ScheduleAction action);

    void execSchedule() override;

private:
#ifdef PXI_EMULATOR_ENABLE
    QSharedPointer<PxiEmulator> m_hardware;
#else
    QSharedPointer<Pxi> m_hardware;
#endif


    double m_temperature{0.0};
    const double m_chipTemperatureMin{0.0};
    const double m_chipTemperatureMax{0.0};

    void pollHardware() override;
    void latestPollHardware() override;

};

#endif // PXISCHEDULETHERMOCOUPLE_H
