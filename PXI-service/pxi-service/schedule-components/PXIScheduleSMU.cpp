#include "config.h"
#include "PXIScheduleSMU.h"
#include "database/PXIDb.h"

PXIScheduleSMU::PXIScheduleSMU(const QSharedPointer<PXISlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
        const QSharedPointer<PxiEmulator> &hardware,
#else
        const QSharedPointer<Pxi> &hardware,
#endif
        const QPointer<PXISwitchConnection> &switchConnection,
        const QPointer<Logger> &logger,
        const quint32 pollIntervall,
        const QDateTime &start, const QDateTime &stop,
        quint8 pinNumber,
        bool useVoltage,
        double v, double ptv, double mtv, double c, double ptc) :
    PXIScheduleEquipment(pxiSlot, logger, pollIntervall, start, stop, PXIScheduleBase::ScheduleActionOn),
    m_hardware(hardware),
    m_switchConnection(switchConnection),
    m_useVoltage(useVoltage),
    m_pinNumber(pinNumber),
    m_voltage((m_useVoltage) ? v : 0.0f),
    m_plusToleranceVoltage((m_useVoltage) ? ptv : 0.0f),
    m_minusToleranceVoltage((m_useVoltage) ? mtv : 0.0f),
    m_voltageMin(m_voltage - m_minusToleranceVoltage),
    m_voltageMax(m_voltage + m_plusToleranceVoltage),
    m_current(c),
    m_plusToleranceCurrent(ptc),
    m_currentMax(m_current + m_plusToleranceCurrent)
{
}

PXIScheduleSMU::PXIScheduleSMU(
        const QSharedPointer<PXISlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
        const QSharedPointer<PxiEmulator> &hardware,
#else
        const QSharedPointer<Pxi> &hardware,
#endif
        const QPointer<PXISwitchConnection> &switchConnection,
        const QPointer<Logger> &logger,
        const QDateTime &start, const QDateTime &stop,
        quint8 pinNumber,
        PXIScheduleBase::ScheduleAction action) :
    PXIScheduleEquipment(pxiSlot, logger, 0, start, stop, action),
    m_hardware(hardware),
    m_switchConnection(switchConnection),
    m_pinNumber(pinNumber)
{
}

void PXIScheduleSMU::execSchedule()
{
    if (m_stage != ScheduleWaitingStart)
        return;

    Bugs bugs;
    try {
        auto smu = m_hardware->sample(m_slotNumber)->smu;
        if ((m_action == ScheduleActionOn) || (m_action == ScheduleActionNoNeed)) {

            m_logger->logSmu(m_slotNumber+1, m_voltage);
            PXIDb::getInstance()->smuSet(m_voltage, m_current);

            switchRelay(true);

            // Для режима "с подачей напряжения"
            if (m_useVoltage)
                bugs += smu->put(m_voltage, m_current);

            if (!smu->isOn() && m_useVoltage) {
                m_logger->logSmuSwitch(m_slotNumber+1, true);
                bugs += smu->on();
            }
        } else {
            bugs += smu->off();

            switchRelay(false);
        }
    } catch (PxiException & e) {
        m_stage = ScheduleError;
        m_slot->harwareErrorTest();
        m_logger->log(e.msg);
        PXIDb::getInstance()->smuLog(e.msg);
        return;
    }

    if (!bugs.isEmpty()) {
        m_stage = ScheduleError;
        m_slot->harwareErrorTest();
        logBugs(bugs);
        PXIDb::getInstance()->logBugs(bugs);
        return;
    }

    if ((m_action == ScheduleActionOn) || (m_action == ScheduleActionNoNeed)) {
        m_stage = ScheduleRun;
        startAliveTimer();
        startPollTimer();
    } else {
        m_slot->disableSmu(); // на время выключения СМУ syncState не должен играть роли да и отображение в UI должно пропасть (хотя syncState() и так для СМУ отключен )
        if (m_startTime == m_stopTime) {
            m_stage = ScheduleCompleted;
            m_logger->log(QStringLiteral("Slot=") + QString::number(m_slotNumber+1) + " Schedule's task completed at once");
        } else {
            m_stage = ScheduleRun;
            startAliveTimer();
        }
    }
}

void PXIScheduleSMU::latestPollHardware()
{
    // даже не знаю, нужно ли реализовывать эту функцию, ибо disable для СМУ предусмотрены, но на всякий случай, наверное надо

    if ((m_action == ScheduleActionOn) || (m_action == ScheduleActionNoNeed)) {

        m_logger->logSmuSwitch(m_slotNumber+1, false);
        PXIDb::getInstance()->smuOff();

        auto smu = m_hardware->sample(m_slotNumber)->smu;
        smu->off();
        switchRelay(false);
    }
}

void PXIScheduleSMU::pollHardware()
{
    if ((m_stage != ScheduleRun) && (m_stage != SchedulePause))
        return;

    Bugs bugs;
    try {
        auto smu = m_hardware->sample(m_slotNumber)->smu;
        double v = m_voltage;
        double c;
        bugs += smu->current(&c);
        if (m_useVoltage) // для режима "с подачей напряжения" производим измерение тока
            bugs += smu->voltage(&v);

        m_logger->log(QStringLiteral("Slot=") + QString::number(m_slotNumber+1) + QStringLiteral(" (SMU V = %1) Measure I = %2").arg(v).arg(c));
        PXIDb::getInstance()->smuGet(v, c);

        m_slot->setSmuCurrent(c, m_currentMax);
        m_slot->setSmuVoltage(v, m_useVoltage, m_voltageMin, m_voltageMax);
    } catch (PxiException & e) {
        m_stage = ScheduleError;
        m_slot->harwareErrorTest();
        m_logger->log(e.msg);
        return;
    }

    if (!bugs.isEmpty()) {
        m_stage = ScheduleError;
        m_slot->harwareErrorTest();
        logBugs(bugs);
    }
}

void PXIScheduleSMU::switchRelay(bool switchOn)
{
    if (switchOn) {
        m_hardware->sample(m_slotNumber)->relay->toSMU(m_pinNumber);
        m_switchConnection->switchOn(m_slotNumber*HSDIO_PINS_NUMBER + m_pinNumber + 1);
    } else {
        m_hardware->sample(m_slotNumber)->relay->toDIO(m_pinNumber);
        m_switchConnection->switchOff(m_slotNumber*HSDIO_PINS_NUMBER + m_pinNumber + 1);
    }
}
