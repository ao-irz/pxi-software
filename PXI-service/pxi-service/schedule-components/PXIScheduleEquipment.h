#ifndef PXISCHEDULEEQUIPMENT_H
#define PXISCHEDULEEQUIPMENT_H

/*! Класс предок для всего оборудования PXI !*/

#include "PXIScheduleBase.h"

class PXIScheduleEquipment : public PXIScheduleBase
{
public:
    PXIScheduleEquipment() = delete;
    PXIScheduleEquipment(
        const QSharedPointer<PXISlot> &pxiSlot,
        const QPointer<Logger> & logger,
        const qint64 pollIntervall,
        const QDateTime &start,
        const QDateTime &stop,
        const ScheduleAction action);

protected:
    QSharedPointer<PXISlot> m_slot;
    quint32 m_slotNumber;
};

#endif // PXISCHEDULEEQUIPMENT_H
