#ifndef CONFIG_H
#define CONFIG_H

//#define PXI_EMULATOR_ENABLE

#define SQLITE                      "sqlite"

#define CONFIG_NAME                 "PXIService.ini"
#define POLL_PROCESSES_PERIOD       1000
#define MS_INTO_MINUTE              60000

#define DEFAULT_IMPEDANCE           (1000000.0)

#define SCHEDULER_PERIOD            500                 // мс
#define CHECK_END_PAUSE_PERIOD      1000                // мс

#define DB_DATETIME_FORMAT_MS       "yyyy-MM-dd hh:mm:ss.zzz"

#define TEMPERATURE_TOLERANCE       2.0                 // допустимое отклонение температуры камеры от заданной (в рабочем режиме)

#define POWER_SUPPLY_NUMBER             4               // количество источников питания для одного слота //!TODO: прописать валидацию в parse циклограммы
#define NEGATIVE_POWER_SUPPLY_NUMBER    2               // номер источника питания для которого будет отрицательное напряжение

#define HSDIO_PINS_NUMBER               8               // количество пинов в DIO

#define PLC_POLL_INTERVAL               500
//#define PLC_WATCHDOG_INTERVAL           (PLC_POLL_INTERVAL+3000)            // интервал срабатывания watchdog, после которого plc будет считаться недоступным (и все его параметры тоже)
#define PLC_STANDBY_MODE                1               // термокамера в режиме STANDBY
#define PLC_CHAMBER_MANUAL_MODE         2               // термокамера будет работать в этом режиме (эта цифра из ее протокола)
#define PLC_CHAMBER_ALARM_MODE          6               // аварийная остановка

#define PLC_ALARM_NORESET               0               // 0 - сбросить аварии
#define PLC_ALARM_RESET                 1               // 1 - сбросить аварии

#define PLC_STATE_FIELDS_NUMBER         173             // реальное количество полей в пакете состояния
#define PLC_STATE_RESPONSE_LENGHT       400
#define PLC_STATE_REQUEST_LENGTH        20
#define PLC_CONTROL_REQUEST_LENGTH      80
#define PLC_PARAMETERS_REQUEST_LENGTH   500
#define PLC_STATE_REQUEST_CMD           0x01            // код команды в внутреннем протоколе

#ifdef PXI_EMULATOR_ENABLE
#define PLC_DESTINATION_ADDRESS         QHostAddress::LocalHost
#define PLC_SOURCE_PORT                 2001
#define PLC_DESTINATION_PORT            2002
#else
#define PLC_DESTINATION_ADDRESS         "192.168.0.50"
#define PLC_SOURCE_PORT                 2000
#define PLC_DESTINATION_PORT            2000
#endif

#define PLC_PERIPHERAL_SENSOR_STATUS_UNKNOWN        0x00
#define PLC_PERIPHERAL_SENSOR_STATUS_OK             0x01
#define PLC_PERIPHERAL_SENSOR_STATUS_FAILURE        0x02

#define PLC_FAN_STATUS_OK               0x01
#define PLC_FAN_STATUS_FAILURE          0x02

#define PLC_THERMAL_CHAMBER_ERROR_CODE_OK              0x00
#define PLC_THERMAL_CHAMBER_ERROR_CODE_OVERHEATING_HW  0x01
#define PLC_THERMAL_CHAMBER_ERROR_CODE_OVERFREEZING_HW 0x02
#define PLC_THERMAL_CHAMBER_ERROR_CODE_OVERHEATING_SW  0x03
#define PLC_THERMAL_CHAMBER_ERROR_CODE_OVERFREEZING_SW 0x04
#define PLC_THERMAL_CHAMBER_ERROR_CODE_LOW_VOLTAGE     0x05
#define PLC_THERMAL_CHAMBER_ERROR_CODE_HIGH_VOLTAGE    0x06

#define PLC_PARAMETER_TEMP_HH                   160.0
#define PLC_PARAMETER_TEMP_LL                   (-70.0)
#define PLC_PARAMETER_COOL_AUTOSTOP_ENABLE      0x01 // true
#define PLC_PARAMETER_TEMP_COOL_AUTOSTOP        40.0
#define PLC_PARAMETER_RACK_SP                   25.1
#define PLC_PARAMETER_RACK_HH                   50.4
#define PLC_PARAMETER_RACK_H                    40.3
#define PLC_PARAMETER_RACK_L                    10.6
#define PLC_PARAMETER_RACK_LL                   5.4

#endif // CONFIG_H
