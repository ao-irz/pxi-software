set n=warm-backup-full.zip

7z u %n% .git -bd -mx=9
7z u %n% -bd -mx=9 -xr!*.zip -xr!*.7z -xr!binaries\*.dll -xr!binaries\*.exe -xr!binaries\*\logs\*.txt -xr!*.o -xr!*.a -xr!*\.moc\* -xr!.hg\*
7z t %n%
