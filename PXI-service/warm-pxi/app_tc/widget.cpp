#include "widget.h"
#include "ui_widget.h"
#include <QtDebug>
#include <QStyle>

Widget::Widget(Cfg & cfg, QWidget * parent) : QWidget(parent), ui(new Ui::Widget) {
    setWindowIcon(style()->standardIcon(QStyle::SP_MessageBoxQuestion));
    ui->setupUi(this);

    //ui->errors->setVisible(false);
    ui->json->setText(cfg.remote);

    t = new QTimer(this);
    connect(t, SIGNAL(timeout()), this, SLOT(onTime()));
    connect(ui->new_values, SIGNAL(clicked()), this, SLOT(onNewValues()));
    t->start(1000);
    ui->value_b->setText(QString("%1В %2Гц %3%\n").arg(0.0).arg(0.0).arg(0.0));
}

Widget::~Widget() {
    delete ui;
}

void Widget::onTime() {
    //qDebug() << "onTime";
    Bugs bugs;
    try {
        if (pxi == nullptr) {
            pxi = new Pxi();
            qDebug() << "PXI object maked";
            bugs += pxi->init();
            go(bugs, "after pxi init", true);
            ui->pxi->setText("thermocouple");
        } else {
            QStringList l;
            et.start();
            double t;
            for (int s = 0; s < samples_slot_count; s ++) {
                auto sample = pxi->sample(s);                
                bugs += sample->tcouple->get(&t);
                if (s) l << " ";
                l << "thermocouple" << QString::number(s) << " "
                  << QString::number(t, 'f', 2) << "°C;";
            }
            l << "\n";
            l << QString("-> измерено за %1 миллисекунд").arg(et.elapsed());
            ui->value_a->setText(l.join(""));
            go(bugs, "", false);
        }
    } catch (PxiException & e) {
        auto l = new QLabel(e.msg);
        ui->errors->addWidget(l);
        ui->errors->addStretch(1);
        t->stop();
        qDebug() << "timer stopped";
    }
}

void Widget::go(const Bugs & bugs, QString msg, bool raise) {
    for (auto bug : bugs) {
        auto m = bug.message.replace("\n\n", " ").replace("\n", " ");
        auto l = new QLabel(QString("%1:%2:%3:%4").arg(bug.device).arg(bug.slot).arg(bug.type).arg(m));
        ui->errors->addWidget(l);
        qDebug() << "Widget::go" << bug.message;
    }
    if (raise && bugs.count()) throw PxiException(msg);
}

void Widget::closeEvent(QCloseEvent * e) {
    t->stop();
    pxi->close();
    e->accept();
}

void Widget::onNewValues() {
}
