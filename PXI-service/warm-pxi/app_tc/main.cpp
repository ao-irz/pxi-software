#include "widget.h"
#include <QApplication>
#include <QtDebug>

#include "cfg/cfg.h"
#include "log/init.h"

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    log_init();
    auto cfg = Cfg();
    qDebug() << cfg.valid << cfg.remote;
    Widget w{cfg};
    w.showMaximized();
    return a.exec();
}
