#ifndef WIDGET_H
#define WIDGET_H

#include <QTimer>
#include <QWidget>
#include <QCloseEvent>
#include <QElapsedTimer>

#include "cfg/cfg.h"
#include "pxi.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    //explicit Widget(QWidget * parent = nullptr, Cfg * cfg = nullptr);
    explicit Widget(Cfg & cfg, QWidget * parent = nullptr);
    ~Widget();
    QTimer * t;
    Pxi * pxi = nullptr;
    void go(const Bugs & bugs, QString msg, bool raise);
    void closeEvent(QCloseEvent * e);

public slots:
    void onNewValues();
    void onTime();

private:
    Ui::Widget *ui;
    int values_state = 0;
    QElapsedTimer et;
};

#endif // WIDGET_H
