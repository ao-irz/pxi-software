#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <QException>
#include "pxi_global.h"

class PXISHARED_EXPORT PxiException : public QException {
public:
    PxiException(QString error);
    void raise() const override;
    PxiException * clone() const override;
    QString msg = nullptr;
    virtual const char * what() const _GLIBCXX_TXN_SAFE_DYN _GLIBCXX_USE_NOEXCEPT override;
};

class PXISHARED_EXPORT TestException : public QException {
public:
    TestException(QString error);
    void raise() const override;
    TestException * clone() const override;
    QString msg = nullptr;
    virtual const char * what() const _GLIBCXX_TXN_SAFE_DYN _GLIBCXX_USE_NOEXCEPT override;
};

#endif // EXCEPTIONS_H
