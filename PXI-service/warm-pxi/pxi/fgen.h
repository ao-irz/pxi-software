#ifndef FGEN_H
#define FGEN_H

#include <QString>
#include "pxi_global.h"
#include "bugs.h"

#if defined(PXI_LIBRARY)
#include "visa.h"
#endif

namespace GenForm {
enum GenForm {
        //none,
        sinus,
        square,
        triangle
    };
}

class PXISHARED_EXPORT FGen { //генератор сигналов
public:
    FGen(int slot, QString device, QString channel); //создаёт С++ объект
    Bugs init(); //инициализирует оборудование, возвращает список ошибок
    Bugs put(GenForm::GenForm form, double ampl, double dcoffset, double freq, double dutycycle); /*
        задаёт:
        форму сигнала,
        амплитуду напряжения,
        смещение напряжения,
        частоту,
        рабочий ход (duty cycle) для прямоугольного счигнала (NI: Specify this attribute as a percentage
        of the time the square wave is high in a cycle)
        */
    /*
    enum Impedance {
        _50,
        _75,
        };
    Bugs putOutputImpedance(Impedance imp);
    */
    Bugs putLoadImpedance(double imp);  // настраивает канал на импеданс нагрузки "imp"
                                        // типичное значение: 1 000 000
                                        // вступает в силу при начале следующей генерации
                                        // т.е. после вызова put(...)
    Bugs on(); //подключает выход канала
    bool isOn() const; //возвращает предполагаемое состояние выхода канала
    Bugs off(); //отключает выход канала
    Bugs test(); //проверка на КЗ, и, возможно, на другие исключительные ситуации
    void close(); //освобождает оборудование

    static const GenForm::GenForm sinus = GenForm::sinus; //константа для метода put
    static const GenForm::GenForm square = GenForm::square; //константа для метода put
    static const GenForm::GenForm triangle = GenForm::triangle; //константа для метода put
#if defined(PXI_LIBRARY)
private:
    int slot = -1;
    QString device = nullptr;
    QString channel = nullptr;
    QByteArray tmp = nullptr;
    ViConstString ch = nullptr;
    ViSession id = VI_NULL;
    bool enabled = false;
    void go(Bugs & e, ViStatus status, QString notes);
    void use(ViStatus status, QString notes);
#endif
};

#endif // FGEN_H
