#ifndef PXI_H
#define PXI_H

#include <QObject>
#include <QString>
#include "pxi_global.h"
#include "exceptions.h"
#include "fgen.h"
#include "power.h"
#include "hsdio.h"
#include "thermocouple.h"
#include "relay.h"
#include "local_pxi_class_value.h"

#if defined(PXI_LIBRARY)
#include "visa.h"
#endif

#define WITHSMU 0

class Pxi;

class PXISHARED_EXPORT Sample { // оборудование для тестирования определённого слота
    friend Pxi;
public:
    Sample(int slot, HsdioCommon * hc, ThermocoupleDevice * tcd, RelayDevice * rd); // создание объекта по индексу слота
    ~Sample();
    Bugs init(); // инициализация оборудования
    void close(); // освобождение ресурсов оборудования
    QString title = "uninitialized Sample object"; // вспомогательный заголовок
    int slot = -1; // индекс слота
    Power * power(int ch); // объект "питание" по индексу канала питания
    QList<Power *> powers(); // список всех объектов "питание" этого слота
    Bugs test(); /*
            проверка этого слота на экстраординарные неисправности (КЗ и т.п.),
            метод возможно ненужен, т.к. вероятно невозможно определить неисправность
            вне контекста циклограммы
                  */
#if WITHSMU
    SMU * smu = nullptr;
#endif
    FGen * fgen = nullptr; // объект "генератор" этого слота
    Hsdio * hsdio = nullptr;
    Thermocouple * tcouple = nullptr;
    Relay * relay = nullptr;
    Bugs neutral();
private:
    QList<Power *> power_chanels;
};

/*
 * приведение слота Х в неактивное, нейтральное состояние в контексте программы
 * выглядит следующим образом:
 *
 * bugs += pxi->sample(X)->neutral();
 * // <замена тестируемого образца или иное ручное вмешательство>
 * // вызов специальных дополнительных методов для отмены неактивного состояния не требуется
 * // требуется стандартная настройка слота Х
 * bugs += pxi->sample(X)->relay->...
 * bugs += pxi->sample(X)->hsdio->...
 * bugs += pxi->sample(X)->power(Y)->on()
 * // и т.д.
 *
 */

class PXISHARED_EXPORT Pxi { // все поддерживаемые устройства стойки для всех слотов
public:
    Pxi(); // создание объекта
    Bugs init(); // инициализация оборудования
    QString title; // вспомогательный заголовок
    Sample * sample(int slot); // объект Sample по индексу слота
    Bugs test(); /*
            проверка всех слотов на экстраординарные неисправности (КЗ и т.п.),
            метод возможно ненужен, т.к. вероятно невозможно определить неисправность
            вне контекста циклограммы
                  */
    void close(); // освобождение ресурсов оборудования
private:
    QList<HsdioCommon *> hsdio;
    QList<Sample *> samples;
    ThermocoupleDevice * tcd = nullptr;
    RelayDevice * rd = nullptr;
};

#endif // PXI_H

/*

Класс Pxi применяет следующие идентификаторы оборудования:

ni2571d0
ni4110d0 ni4110d1 ni4110d2 ni4110d3 ni4110d4 ni4110d5 ni4110d6 ni4110d7
ni4130d0 ni4130d1 ni4130d2 ni4130d3 ni4130d4 ni4130d5 ni4130d6 ni4130d7
ni4353d0
ni5413d0 ni5413d1 ni5413d2 ni5413d3
ni6541d0 ni6541d1

*/
