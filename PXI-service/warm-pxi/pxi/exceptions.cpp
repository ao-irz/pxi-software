#include <QtDebug>
#include "exceptions.h"

PxiException::PxiException(QString error) : QException() {
    msg = "PxiException " + error;
    qDebug() << msg;
}

void PxiException::raise() const {
    throw *this;
}

PxiException * PxiException::clone() const {
    auto tmp = new PxiException(*this);
    tmp->msg = QString(this->msg);
    return tmp;
}

const char * PxiException::what() const noexcept {
    if (msg == nullptr) {
        return "PxiException";
    }
    return msg.toLocal8Bit();
}

TestException::TestException(QString error) : QException() {
    msg = "PxiException " + error;
    qDebug() << msg;
}

void TestException::raise() const {
    throw *this;
}

TestException * TestException::clone() const {
    auto tmp = new TestException(*this);
    tmp->msg = QString(this->msg);
    return tmp;
}

const char * TestException::what() const noexcept {
    if (msg == nullptr) {
        return "PxiException";
    }
    return msg.toLocal8Bit();
}
