#include "bugs.h"
#include <QtDebug>

Bug::Bug(QString _device, int _slot, int _type, QString _message) {
    device = _device;
    slot = _slot;
    type = _type;
    message = _message;
    qDebug() << "Bug::Bug" << device << slot << type << message;
}

Bug::Bug(const Bug & o) {
    device = o.device;
    slot = o.slot;
    type = o.type;
    message = o.message;
}

Bug & Bug::operator=(const Bug & o) {
    if (this == &o) return *this;
    device = o.device;
    slot = o.slot;
    type = o.type;
    message = o.message;
    return *this;
}
