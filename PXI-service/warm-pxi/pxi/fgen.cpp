#include <QtDebug>
#include "fgen.h"
#include "niFgen.h"

#include "exceptions.h"

FGen::FGen(int _slot, QString _device, QString _channel) {
    qDebug() << "FGen" << _device << _channel;
    slot = _slot;
    device = _device;
    channel = _channel;
    tmp = channel.toUtf8();
    ch = tmp.data();
}

Bugs FGen::init() {
    Bugs rv;
    try {
        use(niFgen_InitializeWithChannels(device.toUtf8().data(), ch, VI_TRUE, "", &id), "InitializeWithChannels");
        use(niFgen_ConfigureOutputEnabled(id, ch, VI_FALSE), "ConfigureOutputEnabled");
        use(niFgen_AbortGeneration(id), "AbortGeneration");
        //use(niFgen_ConfigureOutputMode(id, NIFGEN_VAL_OUTPUT_FUNC), "ConfigureOutputMode");
        //use(niFgen_ConfigureStandardWaveform(id, ch, NIFGEN_VAL_WFM_SINE, 0.006, 0, 50, 0), "ConfigureStandardWaveform");
        //use(niFgen_InitiateGeneration(id), "InitiateGeneration");
    } catch (PxiException & e) {
        rv.append(Bug(device, slot, Bug::ni, e.msg));
    }
    return rv;
}

Bugs FGen::put(GenForm::GenForm gm, double ampl, double dcoffset, double freq, double dutycycle) {
    Bugs rv;
    ViInt32 wf;
    if (gm == sinus) {
        wf = NIFGEN_VAL_WFM_SINE;
    } else if (gm == square) {
        wf = NIFGEN_VAL_WFM_SQUARE;
    } else if (gm == triangle) {
        wf = NIFGEN_VAL_WFM_TRIANGLE;
    } else {
        rv.append(Bug(device, slot, Bug::ni, "bad waveform " + QString::number(gm)));
        return rv;
    }
    try {
        use(niFgen_AbortGeneration(id), "AbortGeneration");
        use(niFgen_WaitUntilDone(id, 11), "WaitUntilDone");
        use(niFgen_ConfigureOutputMode(id, NIFGEN_VAL_OUTPUT_FUNC), "ConfigureOutputMode");
        use(niFgen_ConfigureStandardWaveform(id, ch, wf, ampl, dcoffset, freq, 0), "ConfigureStandardWaveform");
        if (wf == NIFGEN_VAL_WFM_SQUARE) {
            use(niFgen_SetAttributeViReal64(id, ch, NIFGEN_ATTR_FUNC_DUTY_CYCLE_HIGH, dutycycle), "set duty cycle");
        }
        use(niFgen_InitiateGeneration(id), "InitiateGeneration");
    } catch (PxiException & e) {
        rv.append(Bug(device, slot, Bug::ni, e.msg));
    }
    return rv;
}

Bugs FGen::putLoadImpedance(double imp) {
    try {
        use(niFgen_SetAttributeViReal64(id, ch, NIFGEN_ATTR_LOAD_IMPEDANCE, imp), "put load impedance");
    } catch (PxiException & e) {
        return {Bug(device, slot, Bug::ni, e.msg)};
    }
    return {};
}

/*
Bugs FGen::putOutputImpedance(FGen::Impedance imp) {
    ViReal64 out = (imp == Impedance::_50) ? NIFGEN_VAL_50_OHMS : NIFGEN_VAL_75_OHMS;
    Bugs rv;
    go(rv, niFgen_ConfigureOutputImpedance(id, ch, out), "ConfigureOutputImpedance");
    return rv;
}
*/

Bugs FGen::on() {
    Bugs rv;
    go(rv, niFgen_ConfigureOutputEnabled(id, ch, VI_TRUE), "ConfigureOutputEnabled");
    if (rv.empty()) enabled = true;
    return rv;
}

bool FGen::isOn() const {
    return enabled;
}

Bugs FGen::off() {
    Bugs rv;
    go(rv, niFgen_ConfigureOutputEnabled(id, ch, VI_FALSE), "ConfigureOutputEnabled");
    if (rv.empty()) enabled = false;
    return rv;
}

Bugs FGen::test() {
    Bugs rv;
    //test method maybe not exists for this hardware
    //niFgen_GetHardwareState
    return rv;
}

void FGen::close() {
    niFgen_ConfigureOutputEnabled(id, ch, VI_FALSE);
    niFgen_AbortGeneration(id);
    niFgen_close(id);
}

void FGen::go(Bugs & rv, ViStatus status, QString notes) {
    if (status == VI_SUCCESS) return;
    QString error = device + ":" + channel + " " + notes;
    ViStatus code;
    ViChar buff[BUFSIZ] = {0};
    if (niFgen_GetError(id, &code, BUFSIZ, buff) == VI_SUCCESS) {
        error += " `" + QString::number(code) + "` " + buff;
    }
    rv.append(Bug(device, slot, Bug::ni, error));
}

void FGen::use(ViStatus status, QString notes) {
    Bugs rv;
    go(rv, status, notes);
    if (rv.count()) {
        //qDebug() << "try_" << 1 << rv[0].message;
        throw PxiException(rv[0].message);
    }
}
