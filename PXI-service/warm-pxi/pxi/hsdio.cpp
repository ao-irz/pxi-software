#include "hsdio.h"
#include "exceptions.h"
#include <QtDebug>
#include <QHash>
#include "niHSDIO.h"

static const bool ts = false;

HsdioCommon::HsdioCommon(int device_idx) {
    idx = device_idx;
    device = QString("ni6541d%1").arg(device_idx);
}

Bugs HsdioCommon::init() {
    readed.clear();
    writed.clear();
    auto name = device.toUtf8();
    auto id = name.data();
    ViConstString pins = "0-31";
    Bugs rv;
    try {
        use(niHSDIO_InitAcquisitionSession(id, VI_FALSE, VI_FALSE, "", &r), "InitAcquisitionSession");
        use(niHSDIO_InitGenerationSession(id, VI_FALSE, VI_FALSE, "", &w), "InitGenerationSession");
        use(niHSDIO_AssignStaticChannels(r, "none"), "AssignStaticChannelsR");
        use(niHSDIO_AssignStaticChannels(w, "none"), "AssignStaticChannelsW");
        if (ts) {
            use(niHSDIO_TristateChannels(r, pins, VI_TRUE), "TristateChannelsR");
            use(niHSDIO_TristateChannels(w, pins, VI_TRUE), "TristateChannelsW");
        }
    } catch (PxiException & e) {
        int first = idx * 4;
        for (int slot = first; slot < (first + 4); ++slot) {
            rv.append(Bug(device, slot, Bug::ni, e.msg));
        }
    }
    return rv;
}

Bugs HsdioCommon::initPin(int slot, int pin_idx, bool none, bool read) {
    qDebug() << "HsdioCommon::initPin" << slot << pin_idx << none << read;
    Bugs rv;
    auto pin = QString("%1").arg(pin_idx);
    bool toread = false, towrite = false;
    try {
        if (none) {
            toread = readed.remove(pin);
            towrite = writed.remove(pin);
        } else if (read) {
            readed << pin;
            toread = true;
        } else {
            writed << pin;
            towrite = true;
        }
        if (toread) {
            auto b = QStringList(readed.toList()).join(",").toUtf8();
            use(niHSDIO_AssignStaticChannels(r, b.data()), "AssignStaticChannelsR");
        }
        if (towrite) {
            auto b = QStringList(writed.toList()).join(",").toUtf8();
            use(niHSDIO_AssignStaticChannels(w, b.data()), "AssignStaticChannelsW");
        }
    } catch (PxiException & e) {
        rv.append(Bug(device, slot, Bug::ni, e.msg));
    }
    return rv;
}

Bugs HsdioCommon::write(int slot, quint32 value, quint32 mask) {
    Bugs rv;
    try {
        use(niHSDIO_WriteStaticU32(w, value, mask), "WriteStaticU32");
    } catch (PxiException & e) {
        rv.append(Bug(device, slot, Bug::ni, e.msg));
    }
    return rv;
}

Bugs HsdioCommon::read(int slot, quint32 * value) {
    Bugs rv;
    ViUInt32 v;
    try {
        use(niHSDIO_ReadStaticU32(r, &v), "ReadStaticU32");
    } catch (PxiException & e) {
        rv.append(Bug(device, slot, Bug::ni, e.msg));
    }
    static_assert(sizeof(quint32) == sizeof(ViUInt32), "sizeof(ViUInt32)");
    *value = v;
    return rv;
}

Bugs HsdioCommon::init4Slot(int slot, int level) {
    Bugs rv;
    try {
        use(niHSDIO_ConfigureDataVoltageLogicFamily(r, "0-31", level), "ConfigureDataVoltageLogicFamilyR");
        use(niHSDIO_ConfigureDataVoltageLogicFamily(w, "0-31", level), "ConfigureDataVoltageLogicFamilyW");
    } catch (PxiException & e) {
        rv.append(Bug(device, slot, Bug::ni, e.msg));
    }
    return rv;
}

void HsdioCommon::close() {
    niHSDIO_close(r);
    r = VI_NULL;
    niHSDIO_close(w);
    w = VI_NULL;
}

void HsdioCommon::use(ViStatus status, QString notes) {
    if (status == VI_SUCCESS) return;
    QString error = device + " " + notes;
    ViStatus code;
    ViChar buff[BUFSIZ] = {0};
    if (niHSDIO_GetError(r, &code, BUFSIZ, buff) == VI_SUCCESS) {
        error += " `" + QString::number(code) + "` " + buff;
    }
    throw PxiException(error);
}

static QHash<Hsdio::LogicVoltage, int> lv_to_ni;

Hsdio::Hsdio(HsdioCommon * _hc, int _slot) {
    hc =_hc;
    slot = _slot;
    if (!lv_to_ni.count()) {
        lv_to_ni[v5_0] = NIHSDIO_VAL_5_0V_LOGIC;
        lv_to_ni[v3_3] = NIHSDIO_VAL_3_3V_LOGIC;
        lv_to_ni[v2_5] = NIHSDIO_VAL_2_5V_LOGIC;
        lv_to_ni[v1_8] = NIHSDIO_VAL_1_8V_LOGIC;
    }
}

Bugs Hsdio::init4Slots(Hsdio::LogicVoltage lv) {
    if (!lv_to_ni.contains(lv)) {
        throw PxiException(QString("%1:%2:bad logic level %3").arg(hc->device).arg(slot).arg(lv));
    }
    return hc->init4Slot(slot, lv_to_ni[lv]);
}

Bugs Hsdio::initPin(int pin_idx, Hsdio::PinState ps) {
    if ((pin_idx < 0) || (pin_idx >= Hsdio::pin_count)) {
        throw PxiException(QString("%1:%2:bad pin index %3").arg(hc->device).arg(slot).arg(pin_idx));
    }
    pin_idx = 8 * slot + pin_idx - (hc->idx * 32);
    return hc->initPin(slot, pin_idx, ps == Hsdio::none, ps == Hsdio::read);
}

quint32 Hsdio::mask(int pin_idx) {
    if ((pin_idx < 0) || (pin_idx >= Hsdio::pin_count)) {
        throw PxiException(QString("%1:%2:bad pin index %3").arg(hc->device).arg(slot).arg(pin_idx));
    }
    quint32 mask = 1;
    mask <<= (8 * slot + pin_idx - (hc->idx * 32));
    return mask;
}

Bugs Hsdio::put(int pin_idx, bool up) {
    auto m = mask(pin_idx);
    return hc->write(slot, up ? m : 0, m);
}

Bugs Hsdio::get(int pin_idx, bool * up) {
    auto m = mask(pin_idx);
    quint32 data;
    auto bugs = hc->read(slot, &data);
    *up = data & m;
    return bugs;
}
