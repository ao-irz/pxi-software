#ifndef BUGS_H
#define BUGS_H

#include "pxi_global.h"
#include <QString>
#include <QList>

class PXISHARED_EXPORT Bug {
public:
    Bug(QString _device, int _slot, int _type, QString _message);
    Bug(const Bug & o);
    QString device;
    int slot;
    int type;
    QString message;
    Bug & operator=(const Bug & o);
    // list of values for 'type' property:
    static const int ni = 0; //NI libs
    static const int circuit = 1; //КЗ
};

typedef QList<Bug> Bugs;

#endif // BUGS_H
