#include <QtDebug>
#include "relay.h"
#include "exceptions.h"
#include "niswitch.h"
#include "local_pxi_class_value.h"

RelayDevice::RelayDevice() {
    device = "ni2571d0";
}

Bugs RelayDevice::init() {
    Bugs rv;
    auto dev = device.toUtf8();
    id = VI_NULL;
    try {
        use(niSwitch_InitWithTopology(
           dev.data(),
           NISWITCH_TOPOLOGY_2571_66_SPDT,
           VI_FALSE,
           VI_TRUE,
           &id), "InitWithTopology");
        use(niSwitch_DisconnectAll(id), "DisconnectAll");
        use(niSwitch_WaitForDebounce(id, 3000), "WaitForDebounce");
    } catch (PxiException & e) {
        for (int slot = 0; slot < samples_slot_count; ++slot) {
            rv.append(Bug(device, slot, Bug::ni, e.msg));
        }
    }
    return rv;
}

Bugs RelayDevice::put(int global_idx, bool on) {
    Bugs rv;
    auto rn = QString("k%1").arg(global_idx).toUtf8();
    ViInt32 action = on ? NISWITCH_VAL_OPEN_RELAY : NISWITCH_VAL_CLOSE_RELAY;
    use(niSwitch_RelayControl(
       id,
       rn.data(),
       action), "RelayControl");
    use(niSwitch_WaitForDebounce(id, 5000), "WaitForDebounce");
    return rv;
}

void RelayDevice::close() {
    if (id != VI_NULL) {
       niSwitch_close(id);
       id = VI_NULL;
    }
}

void RelayDevice::use(ViStatus status, QString notes) {
    if (status == VI_SUCCESS) return;
    auto error = QString("%1 %2 `%3`").arg(device).arg(notes).arg(status);
    char err_buff[2048] = {'\0'};
    if (niSwitch_GetError(id, &status, 2048, err_buff) == VI_SUCCESS) {
        error = QString("%1 %2").arg(error).arg(err_buff);
    }
    throw PxiException(error);
}

Relay::Relay(RelayDevice * _rd, int _slot) {
    rd = _rd;
    slot = _slot;
    smu = -1;
}

Bugs Relay::toDIO(int local_idx) {
    auto b = put(local_idx, true);
    if ((smu == local_idx) && (!b.count())) smu = -1;
    return b;
}

Bugs Relay::toSMU(int local_idx) {
    Bugs rv;
    if (smu != -1) rv += put(smu, true);
    if (!rv.count()) {
        smu = -1;
        rv += put(local_idx, false);
        if (!rv.count()) smu = local_idx;
    }
    return rv;
}

Bugs Relay::put(int local_idx, bool on) {
    Bugs rv;
    if ((local_idx < 0) || (local_idx >= this->count)) {
        throw PxiException(QString("%1:%2:bad relay index %3").arg(rd->device).arg(slot).arg(local_idx));
    }
    try {
        rd->put(slot * this->count + local_idx, on);
    } catch (PxiException & e) {
        rv.append(Bug(rd->device, slot, Bug::ni, e.msg));
    }
    return rv;
}
