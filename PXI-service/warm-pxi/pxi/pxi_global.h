#ifndef PXI_GLOBAL_H
#define PXI_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(PXI_LIBRARY)
#  define PXISHARED_EXPORT Q_DECL_EXPORT
#else
#  define PXISHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // PXI_GLOBAL_H
