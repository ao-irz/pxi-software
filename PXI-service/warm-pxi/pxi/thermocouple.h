#ifndef THERMOCOUPLE_H
#define THERMOCOUPLE_H

#include "bugs.h"

#if defined(PXI_LIBRARY)
#include <NIDAQmx.h>
#endif

class ThermocoupleDevice {
public:
    ThermocoupleDevice();
    Bugs init();
    void close();
    QString device;
    void use(qint32 status, QString notes);
    bool inited = false;
    Bugs bugs;
    static const int CHANNELS = 8;
    double buff[CHANNELS];
#if defined(PXI_LIBRARY)
private:
    TaskHandle id = nullptr;
#endif
};

class PXISHARED_EXPORT Thermocouple {
public:
    Thermocouple(int _slot, ThermocoupleDevice * _tcd);
    Bugs get(double * result);
private:
    int slot;
    ThermocoupleDevice * tcd;
};

#endif // THERMOCOUPLE_H

/*
Термопары Thermocouple.

Класс позволяет прочитать усреднённое значение температуры датчика термопары за последнюю секунду.

Каждому слоту поставлен в соответствие один датчик термопары.

Чтение датчика:
    double t;
    bugs += pxi->sample(0)->tcouple->get(&t);
    // или заполняет переменную t прочитанным значением температуры для слота 0
    // или возвращает не пустой список ошибок

Этот метод срабатывает за время меньшее одной миллисекунды, вычисление среднего и
работа с оборудованием происходят автоматически в фоновом режиме.
*/
