#-------------------------------------------------
#
# Project created by QtCreator 2020-06-15T09:22:15
#
#-------------------------------------------------

QT       -= gui

TARGET = pxi
TEMPLATE = lib

DEFINES += PXI_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11
CONFIG += c++14
CONFIG += c++17

SOURCES += \
        bugs.cpp \
        exceptions.cpp \
        fgen.cpp \
        hsdio.cpp \
        power.cpp \
        pxi.cpp \
        relay.cpp \
        thermocouple.cpp

HEADERS += \
        bugs.h \
        exceptions.h \
        fgen.h \
        hsdio.h \
        local_pxi_class_value.h \
        power.h \
        pxi.h \
        pxi_global.h  \
        relay.h \
        thermocouple.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DESTDIR = $$clean_path($$PWD/../../warm-pxi-tmp)
message(DESTDIR: $$DESTDIR)

VISA_INCLUDE_DIR = $$quote($$(VXIPNPPATH)/WinNT/Include)
INCLUDEPATH += $$VISA_INCLUDE_DIR

NIDAQMX_INCLUDE_DIR = $$quote($$(NIEXTCCOMPILERSUPP)/include)
INCLUDEPATH += $$NIDAQMX_INCLUDE_DIR
NIDAQMX_LIB_DIR = $$quote($$(NIEXTCCOMPILERSUPP)/lib32/msvc)
LIBS +=-L$${NIDAQMX_LIB_DIR} -lNIDAQmx

IVI_INCLUDE_DIR = $$quote($$(IVIROOTDIR32)/Include)
IVI_LIB_DIR = $$quote($$(IVIROOTDIR32)/lib/msc)
INCLUDEPATH += $$IVI_INCLUDE_DIR
LIBS +=-L$${IVI_LIB_DIR} -lnidcpower
LIBS +=-L$${IVI_LIB_DIR} -lnifgen
LIBS +=-L$${IVI_LIB_DIR} -lnihsdio
LIBS +=-L$${IVI_LIB_DIR} -lniswitch
