#ifndef POWER_H
#define POWER_H

#include <QString>
#include "pxi_global.h"
#include "bugs.h"

#if defined(PXI_LIBRARY)
#include "visa.h"
#endif

class PXISHARED_EXPORT Power { //питание (один канал)
public:
    Power(int slot, QString device, QString channel); //создаёт С++ объект
    Bugs init(); //инициализирует оборудование, возвращает список ошибок
    Bugs on(); //подключает выход канала
    bool isOn() const; //возвращает предполагаемое состояние выхода канала
    Bugs off(); //отключает выход канала
    Bugs put(double u, double i); //задаёт выходное напряжение и оганичение по току
    Bugs voltage(double * result); //возвращает список ошибок, напряжение по ссылке
    Bugs current(double * result); //возвращает список ошибок, ток по ссылке
    Bugs measure(double * voltage, double * current); //возвращает список ошибок; напряжение, ток по ссылке
    Bugs test(); //проверка на КЗ, и, возможно, на другие исключительные ситуации
    bool hasRemote(); //есть ли режим выносного измерения (SMU, Source Measure Unit)
    Bugs getRemote(bool * value); //возвращает по ссылке режим выносного измерения
    Bugs putRemote(bool value); //задаёт режим выносного измерения
    void close(); //освобождает оборудование

#if defined(PXI_LIBRARY)
protected:
    int slot = -1;
    QString device = nullptr;
    QString channel = nullptr;
    QByteArray tmp = nullptr;
    ViConstString ch = nullptr;
    ViSession id = VI_NULL;
    double u;
    double i;
    bool enabled = false;
    bool has_remote = false;
    void go(Bugs & e, ViStatus status, QString notes);
    void use(ViStatus status, QString notes);
#endif
};

class PXISHARED_EXPORT SMU : public Power { //канал питания, используемый как измеритель тока
public:
    using Power::Power;
    Bugs init(); //инициализирует оборудование, возвращает список ошибок
};

/*
Для получения измеренной силы тока SMU слота s следует делать:

double a;
bugs += pxi->sample(s)->smu->current(&a);

Никакие другие методы и свойства объекта smu использовать не следует.
*/
#endif // POWER_H
