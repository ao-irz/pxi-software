#include <QThread>
#include <QtDebug>
#include <NIDAQmx.h>
#include "thermocouple.h"
#include "exceptions.h"
#include "local_pxi_class_value.h"

#define AVG_COUNT 10
#define RATE 10

int32 CVICALLBACK EveryNCallback(TaskHandle id, int32 eType, uInt32 nSamples, void * cbdata) {
    (void)eType; /* This parameter contains the value you passed in the
        everyNsamplesEventType parameter of DAQmxRegisterEveryNSamplesEvent function */
    (void)nSamples; /* This parameter contains the value you passed in the
        nSamples parameter of DAQmxRegisterEveryNSamplesEvent function */
    int32 read = 0;
    float64 data[ThermocoupleDevice::CHANNELS * AVG_COUNT];
    auto rc = DAQmxReadAnalogF64(
        id,
        -1, // The number of samples, per channel, to read
        10.0, // Seconds, timeout
        DAQmx_Val_GroupByScanNumber, // or DAQmx_Val_GroupByChannel
        data, // array for data
        ThermocoupleDevice::CHANNELS * AVG_COUNT, // arraySizeInSamps
        &read, // The actual number of samples read from each channel
        nullptr // Reserved
        );
    auto tcd = reinterpret_cast<ThermocoupleDevice *>(cbdata);
    /*
    qDebug() << "EveryNCallback" << "everyNsamplesEventType" << everyNsamplesEventType
             << "nSamples" << nSamples
             << "rc" << rc
             << "read" << read
             << "device" << tcd->device;
    */
    try {
        tcd->use(rc, "ReadAnalogF64");
    } catch (PxiException & e) {
        tcd->bugs.clear();
        for (int slot = 0; slot < samples_slot_count; ++slot) {
            tcd->bugs.append(Bug(tcd->device, slot, Bug::ni, e.msg));
        }
        return 0;
    }
    if ((read < 0) || (read > AVG_COUNT)) {
        tcd->bugs.clear();
        for (int slot = 0; slot < samples_slot_count; ++slot) {
            tcd->bugs.append(Bug(tcd->device, slot, Bug::ni,
                QString("ReadAnalogF64 fill `read` to `%1`").arg(read)));
        }
        return 0;
    }
    float64 div = 1.0 / read, tmp;
    for (int ch = 0; ch < tcd->CHANNELS; ++ch) {
        tmp = 0;
        for (int scan = 0; scan < read; ++scan) {
            tmp += data[scan * tcd->CHANNELS + ch] * div;
        }
        tcd->buff[ch] = tmp;
    }
    tcd->bugs.clear();
    return 0;
}

int32 CVICALLBACK DoneCallback(TaskHandle id, int32 status, void * ptr) {
    qDebug() << "DoneCallback" << "status" << status;
    (void)id;
    (void)ptr;
    return 0;
}

ThermocoupleDevice::ThermocoupleDevice() {
    device = "ni4353d0";
    for (int i = 0; i < CHANNELS; ++i) {
        buff[i] = -333;
    }
}

Bugs ThermocoupleDevice::init() {
    Bugs rv;
    auto ch = QString("%1/ai0:%2").arg(device).arg(CHANNELS - 1).toUtf8();
    if (inited) {
        for (int slot = 0; slot < samples_slot_count; ++slot) {
            rv.append(Bug(device, slot, Bug::ni, "Already initialized"));
        }
        return rv;
    }
    try {
        use(DAQmxCreateTask("", &id), "CreateTask");
        use(DAQmxCreateAIThrmcplChan(
                id,
                ch.data(),
                "",
                15.0,
                135.0,
                DAQmx_Val_DegC,
                DAQmx_Val_K_Type_TC,
                DAQmx_Val_BuiltIn,
                25.0,
                ""
                ), "CreateAIThrmcplChan");
        use(DAQmxCfgSampClkTiming(
                id,
                "",
                RATE, // rate
                DAQmx_Val_Rising,
                DAQmx_Val_ContSamps,
                2 * CHANNELS * AVG_COUNT // buffer size
                ), "CfgSampClkTiming");
        use(DAQmxRegisterEveryNSamplesEvent(
                id,
                DAQmx_Val_Acquired_Into_Buffer,
                AVG_COUNT, // every AVG samples per channel
                0, // 0: called in NI thread
                EveryNCallback,
                this
                ), "RegisterEveryNSamplesEvent");
        use(DAQmxRegisterDoneEvent(id, 0, DoneCallback, this), "RegisterDoneEvent");
        bugs.clear();
        use(DAQmxStartTask(id), "StartTask");
        QThread::currentThread()->msleep(AVG_COUNT * 1100 / RATE);
        inited = true;
    } catch (PxiException & e) {
        for (int slot = 0; slot < samples_slot_count; ++slot) {
            rv.append(Bug(device, slot, Bug::ni, e.msg));
        }
    }
    return rv;
}

void ThermocoupleDevice::close() {
    if (id != nullptr) {
        DAQmxStopTask(id);
        DAQmxClearTask(id);
    }
    inited = false;
}

void ThermocoupleDevice::use(qint32 status, QString notes) {
    if (status == 0) return;
    auto error = QString("%1 %2 `%3`").arg(device).arg(notes).arg(status);
    char err_buff[2048] = {'\0'};
    if (DAQmxGetExtendedErrorInfo(err_buff, 2048) == 0) {
        error = QString("%1 %2").arg(error).arg(err_buff);
    }
    throw PxiException(error);
}

Thermocouple::Thermocouple(int _slot, ThermocoupleDevice * _tcd) {
    slot = _slot;
    tcd = _tcd;
}

Bugs Thermocouple::get(double * result) {
    if (!tcd->inited) {
        return {Bug(tcd->device, slot, Bug::ni, "Uninitialized")};
    }
    auto rv = Bugs(tcd->bugs);
    if (!rv.length()) {
        *result = tcd->buff[slot];
    }
    return rv;
}
