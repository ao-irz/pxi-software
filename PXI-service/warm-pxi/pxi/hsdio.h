#ifndef HSDIO_H
#define HSDIO_H

#include <QSet>

#include "bugs.h"

#if defined(PXI_LIBRARY)
#include "visa.h"
#endif

class HsdioCommon {
public:
    HsdioCommon(int device_idx);
    Bugs init();
    Bugs initPin(int slot, int pin_idx, bool none, bool read);
    Bugs init4Slot(int slot, int level);
    Bugs write(int slot, quint32 value, quint32 mask);
    Bugs read(int slot, quint32 * value);
    void close();
    QString device;
    int idx;
#if defined(PXI_LIBRARY)
private:
    QSet<QString> readed;
    QSet<QString> writed;
    ViSession w = VI_NULL;
    ViSession r = VI_NULL;
    void use(ViStatus status, QString notes);
#endif
};

class PXISHARED_EXPORT Hsdio {
public:
    enum PinState {
        read,
        write,
        none
        };
    enum LogicVoltage {
        v5_0,
        v3_3,
        v2_5,
        v1_8
        };
    static const int pin_count = 8;
    Hsdio(HsdioCommon * _hc, int _slot);
    Bugs init4Slots(LogicVoltage lv);
    Bugs initPin(int pin_idx, PinState ps);
    Bugs put(int pin_idx, bool up);
    Bugs get(int pin_idx, bool * up);
private:
    HsdioCommon * hc;
    int slot;
    quint32 mask(int pin_idx);
};

#endif // HSDIO_H

/*
Цифровой генератор-анализатор Hsdio.
В данный момент реализовано статическое использование hsdio:
на выходе каждого пина можно записать высокий или низкий уровень напряжения,
которое будет сохранятся до тех пор, пока не будет изменено
явным образом. Читаться будет мгновенное значение напряжения
на пине. Сразу после инициализации pxi состояние пина и его режим неопределено и неизвестно.

Все слоты разделены на две группы 0..3 и 4..7, это ограничение аппаратуры.
Каждой группе можно назначить своё напряжени логики. Назначая напряжение логики одному слоту,
тем самым назначается напряжение логики для всех слотов из группы.
    bugs += pxi->sample(0)->hsdio->init4Slots(Hsdio::v1_8);
    или
    bugs += pxi->sample(3)->hsdio->init4Slots(Hsdio::v1_8);
    // будет назначено напряжение логики 1.8 В для первых 4-х слотов

На каждый слот отведено 8 пинов.
Каждый пин можно настроить на чтение, запись или бездействие.
    bugs += pxi->sample(0)->hsdio->initPin(0, Hsdio::read);
    bugs += pxi->sample(0)->hsdio->initPin(1, Hsdio::write);
    bugs += pxi->sample(0)->hsdio->initPin(2, Hsdio::none);

Чтение пина, настроеного на чтение:
    bool value;
    bugs += pxi->sample(0)->hsdio->get(0, &value);
    // возвращает или прочитаное значение на пине с индексом 0
    // или не пустой список ошибок

Запись пина, настроеного на зарись:
    bugs += pxi->sample(0)->hsdio->put(1, true);
    // или устанавливает высокий уровень (true) на выходе пина с индексом 1
    // или возвращает не пустой список ошибок
*/
