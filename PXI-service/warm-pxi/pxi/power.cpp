#include "power.h"
#include "nidcpower.h"

#include "exceptions.h"
#include <QtDebug>

Power::Power(int _slot, QString _device, QString _channel) {
    slot = _slot;
    device = _device;
    channel = _channel;
    tmp = channel.toUtf8();
    ch = tmp.data();
}

Bugs Power::init() {
    Bugs rv;
    try {
        use(niDCPower_InitializeWithChannels(device.toUtf8().data(), ch, VI_TRUE, "", &id), "InitializeWithChannels");
        use(niDCPower_ConfigureOutputEnabled(id, ch, VI_FALSE), "ConfigureOutputEnabled F");
        use(niDCPower_ConfigureSourceMode(id, NIDCPOWER_VAL_SINGLE_POINT), "ConfigureSourceMode");
        use(niDCPower_ConfigureOutputFunction(id, ch, NIDCPOWER_VAL_DC_VOLTAGE), "ConfigureOutputFunction");
        has_remote = niDCPower_ConfigureSense(id, ch, NIDCPOWER_VAL_REMOTE) == VI_SUCCESS;
        use(niDCPower_ConfigureSense(id, ch, NIDCPOWER_VAL_LOCAL), "ConfigureSense");
        use(niDCPower_SetAttributeViInt32(id, ch, NIDCPOWER_ATTR_VOLTAGE_LEVEL_AUTORANGE, NIDCPOWER_VAL_ON), "VOLTAGE_LEVEL_AUTORANGE");
        use(niDCPower_SetAttributeViInt32(id, ch, NIDCPOWER_ATTR_CURRENT_LIMIT_AUTORANGE, NIDCPOWER_VAL_ON), "CURRENT_LIMIT_AUTORANGE");
        use(niDCPower_SetAttributeViBoolean(id, "", NIDCPOWER_ATTR_OVERRANGING_ENABLED, VI_FALSE), "OVERRANGING_ENABLED");
        use(niDCPower_ConfigureVoltageLevel(id, ch, 0), "ConfigureVoltageLevel");
        use(niDCPower_ConfigureCurrentLimit(id, ch, NIDCPOWER_VAL_CURRENT_REGULATE, 0.1), "ConfigureCurrentLimit");
        use(niDCPower_Initiate(id), "Initiate");
        use(niDCPower_WaitForEvent(id, NIDCPOWER_VAL_SOURCE_COMPLETE_EVENT, 5.0), "WaitForEvent");
    } catch (PxiException & e) {
        rv.append(Bug(device, slot, Bug::ni, e.msg));
    }
    return rv;
}

Bugs Power::on() {
    Bugs rv;
    go(rv, niDCPower_ConfigureOutputEnabled(id, ch, VI_TRUE), "ConfigureOutputEnabled T");
    if (rv.empty()) enabled = true;
    return rv;
}

bool Power::isOn() const {
    // Возможно, более правильно использовать NIDCPOWER_ATTR_OUTPUT_ENABLED
    return enabled;
}

Bugs Power::off() {
    Bugs rv;
    go(rv, niDCPower_ConfigureOutputEnabled(id, ch, VI_FALSE), "ConfigureOutputEnabled F");
    if (rv.empty()) enabled = false;
    return rv;
}

Bugs Power::put(double _u, double _i) {
    u = _u;
    i = _i;
    Bugs rv;
    go(rv, niDCPower_ConfigureVoltageLevel(id, ch, _u), "put.ConfigureVoltageLevel");
    go(rv, niDCPower_ConfigureCurrentLimit(id, ch, NIDCPOWER_VAL_CURRENT_REGULATE, _i), "put.ConfigureCurrentLimit");
    go(rv, niDCPower_WaitForEvent(id, NIDCPOWER_VAL_SOURCE_COMPLETE_EVENT, 5.0), "put.WaitForEvent");
    return rv;
}

Bugs Power::voltage(double * result) {
    Bugs rv;
    try {
        use(niDCPower_Measure(id, ch, NIDCPOWER_VAL_MEASURE_VOLTAGE , result), "Measure U");
        //use(niDCPower_WaitForEvent(id, NIDCPOWER_VAL_MEASURE_COMPLETE_EVENT, 5.0), "WaitForEvent U");
    } catch (PxiException & e) {
        rv.append(Bug(device, slot, Bug::ni, e.msg));
    }
    return rv;
}

Bugs Power::current(double * result) {
    Bugs rv;
    try {
        use(niDCPower_Measure(id, ch, NIDCPOWER_VAL_MEASURE_CURRENT , result), "Measure I");
        //use(niDCPower_WaitForEvent(id, NIDCPOWER_VAL_MEASURE_COMPLETE_EVENT, 5.0), "WaitForEvent I");
    } catch (PxiException & e) {
        rv.append(Bug(device, slot, Bug::ni, e.msg));
    }
    return rv;
}

Bugs Power::measure(double * voltage, double * current) {
    Bugs rv;
    go(rv, niDCPower_MeasureMultiple(id, ch, voltage, current), "Measure M");
    return rv;
}

Bugs Power::test() {
    Bugs rv;
    if (!enabled) return rv;
    double v = 0, c = 0;
    rv += measure(&v, &c);
    if (!rv.length()) {
        QString error = device + ":" + channel + ", " + QString::number(v) + "V, " + QString::number(c) + "A";
        if (((abs(u) > 0.001) && (abs(v) < 0.001)) || (abs(c) < 0.001)) {
            rv.append(Bug(device, slot, Bug::circuit, error));
        }
    }
    return rv;
}

bool Power::hasRemote() {
    return has_remote;
}

Bugs Power::getRemote(bool * value) {
    Bugs bugs;
    ViInt32 rv = 0;
    go(bugs, niDCPower_GetAttributeViInt32(id, ch, NIDCPOWER_ATTR_SENSE, &rv), "getRemote");
    if (bugs.count()) return bugs;
    if (rv == NIDCPOWER_VAL_LOCAL) {
        *value = false;
    } else if (rv == NIDCPOWER_VAL_REMOTE) {
        *value = true;
    } else {
        bugs += Bug(device, slot, Bug::ni, QString("Unknown ATTR_SENSE %1").arg(rv));
    }
    return bugs;
}

Bugs Power::putRemote(bool value) {
    Bugs bugs;
    ViInt32 attr = value ? NIDCPOWER_VAL_REMOTE : NIDCPOWER_VAL_LOCAL;
    try {
        use(niDCPower_Abort(id), "putRemote.Abort");
        use(niDCPower_SetAttributeViInt32(id, ch, NIDCPOWER_ATTR_SENSE, attr), "putRemote.ATTR_SENSE");
        use(niDCPower_Initiate(id), "putRemote.Initiate");
        use(niDCPower_WaitForEvent(id, NIDCPOWER_VAL_SOURCE_COMPLETE_EVENT, 5.0), "putRemote.WaitForEvent");
    } catch (PxiException & e) {
        bugs.append(Bug(device, slot, Bug::ni, e.msg));
    }
    return bugs;
}

void Power::close() {
    enabled = false;
    niDCPower_ConfigureOutputEnabled(id, ch, VI_FALSE);
    niDCPower_close(id);
}

void Power::go(Bugs & rv, ViStatus status, QString notes) {
    if (status == VI_SUCCESS) return;
    QString error = device + ":" + channel + " " + notes;
    ViStatus code;
    ViChar buff[BUFSIZ] = {0};
    if (niDCPower_GetError(id, &code, BUFSIZ, buff) == VI_SUCCESS) {
        error += " `" + QString::number(code) + "` " + buff;
    }
    rv.append(Bug(device, slot, Bug::ni, error));
}

void Power::use(ViStatus status, QString notes) {
    Bugs rv;
    go(rv, status, notes);
    if (rv.count()) {
        //qDebug() << "try_" << 1 << rv[0].message;
        throw PxiException(rv[0].message);
    }
}

Bugs SMU::init() {
    Bugs bugs = Power::init();
    if (bugs.count()) return bugs;
    if (hasRemote()) {
        bugs += putRemote(true);
    } else {
        bugs.append(Bug(device, slot, Bug::ni, "Not a SMU"));
    }
    return bugs;
}
