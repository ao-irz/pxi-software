#ifndef RELAY_H
#define RELAY_H

#include "bugs.h"

#if defined(PXI_LIBRARY)
#include "visa.h"
#endif

class RelayDevice {
public:
    RelayDevice();
    Bugs init();
    Bugs put(int global_idx, bool on);
    void close();
    QString device;
private:
#if defined(PXI_LIBRARY)
    ViSession id = VI_NULL;
    void use(ViStatus status, QString notes);
#endif
};

class PXISHARED_EXPORT Relay {
public:
    static const int count = 8;
    Relay(RelayDevice * rd, int _slot);
    Bugs toDIO(int local_idx);
    Bugs toSMU(int local_idx);
private:
    RelayDevice * rd;
    int slot;
    int smu;
    Bugs put(int local_idx, bool on);
};

#endif // RELAY_H

/*
Реле Relay.

У каждого слота есть 8 линий.
После инициализации стойки эти 8 линий (0..7)
подключённы через реле к цифровому вводу/выводу (DIO) -
генератору-анализатору HSDIO.
Класс позволяет перелючать одну из этих 8 линий к
SMU этого же слота, или обратно к HSDIO.

Подключение линии к SMU:
    bugs += pxi->sample(0)->relay->toSMU(5);
    // или подключает линию с индексом 5 слота 0 к SMU
    // или возвращает не пустой список ошибок

Подключение линии к HSDIO:
    bugs += pxi->sample(0)->relay->toDIO(5);
    // или подключает линию с индексом 5 слота 0 к DIO
    // или возвращает не пустой список ошибок
*/
