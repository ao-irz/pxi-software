#include <QtDebug>
#include "pxi.h"

Pxi::Pxi() {
    title = "PXI box for " + QString::number(samples_slot_count) + " samples";
}

Bugs Pxi::init() {
    if (rd != nullptr) throw PxiException("Pxi::init() already executed without Pxi::close()");
    Bugs rv;
    rd = new RelayDevice();
    rv += rd->init();
    tcd = new ThermocoupleDevice();
    rv += tcd->init();
    hsdio.append(new HsdioCommon(0));
    hsdio.append(new HsdioCommon(1));
    rv += hsdio[0]->init();
    rv += hsdio[1]->init();
    for (int i = 0; i < samples_slot_count; i++) {
        auto s = new Sample(i, hsdio[i / 4], tcd, rd);
        rv += s->init();
        samples.append(s);
    }
    return rv;
}

Sample * Pxi::sample(int slot) {
    if ((slot < 0) || (slot >= samples_slot_count)) {
        throw PxiException("bad sample slot");
    }
    return samples[slot];
}

Bugs Pxi::test() {
    Bugs b;
    for (auto s : samples) {
        b += s->test();
    }
    return b;
}

void Pxi::close() {
    for (auto s : samples) {
        if (s == nullptr) continue;
        s->close();
    }
    while (!samples.isEmpty()) {
        auto s = samples.takeLast();
        delete s;
    }
    for (auto h : hsdio) {
        if (h == nullptr) continue;
        h->close();
    }
    while (!hsdio.isEmpty()) {
        auto h = hsdio.takeLast();
        delete h;
    }
    tcd->close();
    tcd = nullptr;
    rd->close();
    rd = nullptr;
}

Sample::Sample(int slot, HsdioCommon * hc, ThermocoupleDevice * tcd, RelayDevice * rd) {
    this->slot = slot;
    auto s = QString::number(slot);
    title = "Test sample slot [" + s + "]";
    QString sample = "d" + s;
    power_chanels.append(new Power(slot, "ni4110" + sample, "0")); // 0 V to +6 V
    power_chanels.append(new Power(slot, "ni4110" + sample, "1")); // 0 V to +20 V
#if WITHSMU
    power_chanels.append(new Power(slot, "ni4110" + sample, "2")); // 0 V to -20 V
#else
    power_chanels.append(new Power(slot, "ni4130" + sample, "1"));
#endif
    power_chanels.append(new Power(slot, "ni4130" + sample, "0")); // 0 V to 6 V
#if WITHSMU
    smu = new SMU(slot, "ni4130" + sample, "1");
#endif
    fgen = new FGen(slot, "ni5413d" + QString::number(slot / 2), QString::number(slot % 2));
    hsdio = new Hsdio(hc, slot);
    tcouple = new Thermocouple(slot, tcd);
    relay = new Relay(rd, slot);
}

Sample::~Sample() {
    delete relay;
    delete hsdio;
    delete fgen;
#if WITHSMU
    delete smu;
#endif
    while (!power_chanels.isEmpty()) {
        auto p = power_chanels.takeLast();
        delete p;
    }
}

Bugs Sample::init() {
    Bugs rv;
    for(auto p : powers()) {
        rv += p->init();
    }
#if WITHSMU
    rv += smu->init();
#endif
    rv += fgen->init();
    return rv;
}

Power * Sample::power(int ch) {
    if ((ch < 0) || (ch > 4)) {
        throw PxiException("Sample::power: bad channel index");
    }
    return power_chanels[ch];
}

QList<Power *> Sample::powers() {
    return QList<Power *>(power_chanels);
}

Bugs Sample::test() {
    Bugs b;
    for (auto p : powers()) {
        b += p->test();
    }
    b += fgen->test();
    return b;
}

Bugs Sample::neutral() {
    Bugs rv;
    for(auto p : powers()) {
        rv += p->off();
    }
#if WITHSMU
    rv += smu->off();
#endif
    rv += fgen->off();
    for (int idx = 0; idx < hsdio->pin_count; ++idx) {
        rv += hsdio->initPin(idx, hsdio->none);
    }
    for (int idx = 0; idx < relay->count; ++idx) {
        rv += relay->toDIO(idx);
    }
    return rv;
}

void Sample::close() {
    if (fgen != nullptr) {
        fgen->close();
    }
    for (auto p : powers()) {
        p->close();
    }
}
