#-------------------------------------------------
#
# Project created by QtCreator 2020-04-27T09:20:07
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = warm-pxi-gui
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11
CONFIG += c++14
CONFIG += c++17

SOURCES += \
        cfg/cfg.cpp \
        log/init.cpp \
        main.cpp \
        widget.cpp

HEADERS += \
        cfg/cfg.h \
        log/init.h \
        widget.h

FORMS += \
        widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
message(INSTALLS: $$INSTALLS)

RC_ICONS = images/temperature-icon-48.ico

RESOURCES +=

cutelogger = $$PWD/../cutelogger
INCLUDEPATH += $$cutelogger/include
win32:CONFIG(release, debug|release): LIBS += -L$$cutelogger/lib/release/ -lCuteLogger
else:win32:CONFIG(debug, debug|release): LIBS += -L$$cutelogger/lib/debug/ -lCuteLogger
message(LIBS: $$LIBS)

DESTDIR = $$clean_path($$PWD/../../warm-pxi-tmp)
message(DESTDIR: $$DESTDIR)

INCLUDEPATH += $$PWD/../pxi
LIBS += -L$$DESTDIR -lpxi
