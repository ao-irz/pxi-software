#include "cfg.h"
#include <QDir>
#include <QJsonDocument>
#include <QJsonObject>
#include <QCoreApplication>
#include <QtDebug>

Cfg::Cfg() {
    QString json = "cfg.json", cfg;
    auto adp = QDir(QCoreApplication::applicationDirPath());
    auto dev = QDir(adp.filePath("../warm"));
    QList<QDir> dirs = {dev, adp};
    for (auto d : dirs) {
        d.makeAbsolute();
        auto check = d.filePath(json);
        qDebug() << "???:" << check;
        if (QFile::exists(check)) cfg = check;
    }
    qDebug() << "cfg:" << cfg;
    QFile f(cfg);
    if (!f.open(QIODevice::ReadOnly)) {
        qWarning() << "Couldn't open" << cfg;
        return;
    }
    auto doc = QJsonDocument::fromJson(f.readAll());
    f.close();
    if (doc.isNull()) return;

    auto o = doc.object();
    if (o.contains("remote") && o["remote"].isString()) {
        remote = o["remote"].toString();
        if (remote.isNull()) return;
        if (remote.length()) valid = true;
    }
}
