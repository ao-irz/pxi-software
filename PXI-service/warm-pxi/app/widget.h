#ifndef WIDGET_H
#define WIDGET_H

#include <QTimer>
#include <QWidget>
#include <QCloseEvent>
#include <QElapsedTimer>

#include "cfg/cfg.h"
#include "pxi.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(Cfg & cfg, QWidget * parent = nullptr);
    ~Widget();
    QTimer * t;
    Pxi * pxi = nullptr;
    QSet<int> bad_slots;
    QList<int> work;
    void slot_state_update(Bugs);
    void put_state(double base);
    void go(const Bugs & bugs, QString msg, bool raise);
    void closeEvent(QCloseEvent * e);

public slots:
    void onNewValues();
    void onTime();

private:
    Ui::Widget * ui;
    int values_state = 0;
    QElapsedTimer et;
    int relay_pos = 0;
};

#endif // WIDGET_H
