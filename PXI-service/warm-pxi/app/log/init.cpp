#include "init.h"
#include <QDir>
#include <QDateTime>
#include <QCoreApplication>
#include "Logger.h"
#include "FileAppender.h"

log_init::log_init() {
    const QString logs = "logs";
    QDir pdir(".");
    pdir.makeAbsolute();
    QDir ldir(pdir.filePath(logs));
    if (!ldir.exists()) pdir.mkdir(logs);
    auto prefix = ldir.filePath(QDateTime::currentDateTime().toString("yyyy_MM_dd__hh_mm_ss_zzz"));

    /*
    SignalAppender * signalAppender = new SignalAppender;
    signalAppender->setFormat("[%{type:-7}] <%{Function}> %{message}\n");
    cuteLogger->registerAppender(signalAppender);
    */

    FileAppender * fa = new FileAppender(prefix + ".txt");
    fa->setFormat("[%{type:-7}] <%{Function}> %{message}\n");
    cuteLogger->registerAppender(fa);
    qDebug() << "warm project" << QCoreApplication::applicationFilePath();
    qDebug() << "AUTOREV";
    //qDebug() << AUTOREV;
    //setWindowTitle(AUTOREV + " - " + windowTitle());
}
