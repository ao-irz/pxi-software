#include "widget.h"
#include "ui_widget.h"
#include <QtDebug>

Widget::Widget(Cfg & cfg, QWidget * parent) : QWidget(parent), ui(new Ui::Widget) {
    ui->setupUi(this);

    //ui->errors->setVisible(false);
    ui->json->setText(cfg.remote);

    t = new QTimer(this);
    connect(t, SIGNAL(timeout()), this, SLOT(onTime()));
    connect(ui->new_values, SIGNAL(clicked()), this, SLOT(onNewValues()));
    t->start(1000);
    ui->value_b->setText(QString("%1В %2Гц %3%\n").arg(0.0).arg(0.0).arg(0.0));
}

Widget::~Widget() {
    delete ui;
}

void Widget::slot_state_update(Bugs bugs) {
    for (auto bug : bugs) {
        bad_slots.insert(bug.slot);
    }
    work.clear();
    for (int s = 0; s < samples_slot_count; s ++) {
        if (!bad_slots.contains(s)) {
            work.append(s);
        }
    }
}

void Widget::put_state(double base) {
    Bugs bugs;
    auto u = base;
    int freq_base = 100;
    int df_base = 0; // duty factor for duty cycle
    if (base > 1) {
        freq_base = 150;
        df_base = samples_slot_count;
    }
    ui->sample->setText(QString("Samples with base %1").arg(u));
    QStringList l;
    for (int s : work) {
        auto sample = pxi->sample(s);
        ui->sample->setText(ui->sample->text() + QString(" #%1").arg(s));
        for (int c = 0; c < sample->powers().count(); c ++) {
            auto p = sample->power(c);
            sample->power(c)->close();
            bugs += sample->power(c)->init();
            bugs += p->put((c == 2) ? -u : u, 0.1);
            //bugs += p->off();
            if (bugs.empty() && (!p->isOn())) bugs += p->on();
            u += 0.1;
            if (p->hasRemote()) {
                bool r;
                bugs += p->getRemote(&r);
                bugs += p->putRemote(!r);
            }
#if WITHSMU
            bugs += sample->smu->put(5.0, 0.2);
            if (base < 1) {
                bugs += sample->smu->off();
            } else {
                bugs += sample->smu->on();
            }
#endif
            go(bugs, "after power put", true);
        }
        double freq = freq_base * (s + 1);
        double dutycycle = 100 / (df_base + s + 2);
        bugs += sample->fgen->putLoadImpedance(1E6);
        bugs += sample->fgen->put(FGen::square, u, 0, freq, dutycycle);
        if (bugs.empty() && (!sample->fgen->isOn())) bugs += sample->fgen->on();
        l << QString("%1-slot %2В %3Гц %4%\n").arg(s).arg(u).arg(freq).arg(dutycycle);
        go(bugs, "after generator put", true);
        bugs += sample->hsdio->init4Slots(Hsdio::v1_8);
        int p = (s + 1) % sample->hsdio->pin_count;
        bugs += sample->hsdio->initPin(s, Hsdio::write);
        bugs += sample->hsdio->initPin(p, Hsdio::write);
        bugs += sample->hsdio->initPin(p, Hsdio::read);
        bugs += sample->hsdio->put(s, base < 1);
        bugs += sample->hsdio->put(p, base < 1);
        go(bugs, "after hsdio put", true);
    }
    et.start();
    for (int s : work) {
        auto sample = pxi->sample(s);
        int pos = (relay_pos + s) % Relay::count;
        bugs += sample->relay->toSMU(pos);
        l << QString("slot %1 relay: ").arg(s);
        auto r = QStringList({"0", "0", "0", "0", "0", "0", "0", "0", "\n"});
        r[sample->relay->count - pos - 1] = "1";
        l += r;
        go(bugs, "after relay operation", true);
    }
    l << QString("-> переключились за %1 миллисекунд").arg(et.elapsed());
    ui->value_b->setText(l.join(""));
    relay_pos = (relay_pos + 1) % Relay::count;
}

void Widget::onTime() {
    //qDebug() << "onTime";
    Bugs bugs;
    try {
        if (pxi == nullptr) {
            pxi = new Pxi();
            qDebug() << "PXI object maked";
            bugs += pxi->init();
            go(bugs, "after pxi init", false);
            slot_state_update(bugs);
            bugs.clear();
            qDebug() << "work" << work;

            ui->pxi->setText(pxi->title);
            for (int s : work) {
                bugs += pxi->sample(s)->neutral();
            }
            go(bugs, "after pxi neutral", false);
            slot_state_update(bugs);
            bugs.clear();

            for (int s : work) {
                auto sample = pxi->sample(s);
                for (int p = 0; p < Hsdio::pin_count; ++p) {
                    bugs += sample->hsdio->initPin(p, Hsdio::read);
                }
            }
            go(bugs, "after hsdio init", false);
            slot_state_update(bugs);
            bugs.clear();

            put_state(0.5);
            values_state = 2;
        } else {
            double v = 0, a = 0;
            QStringList l;
            et.start();
            for (int s : work) {
                auto sample = pxi->sample(s);
                for (int c = 0; c < sample->powers().count(); c ++) {
                    auto p = sample->power(c);
                    bugs += p->measure(&v, &a);
                    l << "Slot" << QString::number(s) << " DC" << QString::number(c) << " "
                      << QString::number(v, 'f') << "V " << QString::number(a, 'f') << "A\n";
                }
            }
            l << QString("-> измерено за %1 миллисекунд\n").arg(et.elapsed());

#if WITHSMU
            et.start();
            for (int s : work) {
                bugs += pxi->sample(s)->smu->current(&a);
                l << QString("Slot %1 SMU:%2 %3 A\n")
                     .arg(s).arg(pxi->sample(s)->smu->isOn() ? "on" : "off").arg(a);
            }
            l << QString("-> измерено за %1 миллисекунд\n").arg(et.elapsed());
#endif

            bool hsdio;
            l << "hsdio: ";
            et.start();
            for (int s = (work.count() - 1); s >= 0; --s) {
                s = work[s];
                auto sample = pxi->sample(s);
                for (int pin_idx = (Hsdio::pin_count - 1); pin_idx >= 0; --pin_idx) {
                    bugs += sample->hsdio->get(pin_idx, &hsdio);
                    l << QString("%1").arg(hsdio);
                    if (pin_idx == 4) l << "_";
                }
                l << " ";
            }
            l << QString("\n-> измерено за %1 миллисекунд\n").arg(et.elapsed());

            double t;
            l << "thermocouple: ";
            et.start();
            for (int s : work) {
                auto sample = pxi->sample(s);
                bugs += sample->tcouple->get(&t);
                if (s) l << ", ";
                l << QString::number(s) << ": " << QString::number(t, 'f', 2) << "°C";
            }
            l << QString("\n-> измерено за %1 миллисекунд").arg(et.elapsed());

            ui->value_a->setText(l.join(""));
            go(bugs, "", false);
        }
    } catch (PxiException & e) {
        auto l = new QLabel(e.msg);
        ui->errors->addWidget(l);
        ui->errors->addStretch(1);
        t->stop();
        qDebug() << "timer stopped";
    }
}

void Widget::go(const Bugs & bugs, QString msg, bool raise) {
    for (auto bug : bugs) {
        auto m = bug.message.replace("\n\n", " ").replace("\n", " ");
        auto l = new QLabel(QString("%1:%2:%3:%4").arg(bug.device).arg(bug.slot).arg(bug.type).arg(m));
        ui->errors->addWidget(l);
        qDebug() << "Widget::go" << bug.message;
    }
    if (raise && bugs.count()) throw PxiException(msg);
}

void Widget::closeEvent(QCloseEvent * e) {
    t->stop();
    pxi->close();
    e->accept();
}

void Widget::onNewValues() {
    try {
        if (values_state == 0) {
            return;
        } else if (values_state == 1) {
            put_state(0.5);
            values_state = 2;
        } else {
            put_state(1.5);
            values_state = 1;
        }
    } catch (PxiException & e) {
        auto l = new QLabel(e.msg);
        ui->errors->addWidget(l);
        ui->errors->addStretch(1);
        t->stop();
        qDebug() << "timer stopped";
    }
}
