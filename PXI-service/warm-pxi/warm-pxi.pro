TEMPLATE = subdirs

CONFIG += c++11
CONFIG += c++14
CONFIG += c++17
CONFIG += ordered

SUBDIRS += \
    pxi \
    app \
    app_tc

#DEFINES += "USE_MY_STUFF = u789"
#$$(q) = qwe
