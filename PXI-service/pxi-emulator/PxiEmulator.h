#ifndef PXIEMULATOR_H
#define PXIEMULATOR_H

#include <QSettings>
#include <QPointer>
#include "../warm-pxi/pxi/pxi.h"

class PowerEmulator
{
public:
    PowerEmulator(int nSlot, int n, const QPointer<QSettings> sett) :
        m_n(n),
        m_settings(sett),
        m_nSlot(nSlot)
    {   }
    Bugs on() //подключает выход канала
    { m_onOffState = true; return Bugs(); }
    bool isOn() const //возвращает предполагаемое состояние выхода канала
    { return  m_onOffState; }
    Bugs off() //отключает выход канала
    { m_onOffState = false; return Bugs();  }
    Bugs put(double u, double i);
    Bugs voltage(double * result); //возвращает список ошибок, напряжение по ссылке
    Bugs current(double * result); //возвращает список ошибок, ток по ссылке

    int m_n;
    double m_u;
    double m_i;
    bool m_onOffState{false};

    QPointer<QSettings> m_settings;
    int m_nSlot;
};

class SMUEmulator : public PowerEmulator
{
    public:
    using PowerEmulator::PowerEmulator;
    Bugs init();
};

class FGenEmulator
{
public:
    FGenEmulator(int nSlot, const QPointer<QSettings> sett):
        m_settings(sett),
        m_nSlot(nSlot)
    {   }
    Bugs on() //подключает выход канала
    { onOffState = true; return Bugs(); }
    bool isOn() const //возвращает предполагаемое состояние выхода канала
    { return  onOffState; }
    Bugs off() //отключает выход канала
    { onOffState = false; return Bugs();  }
    Bugs put(GenForm::GenForm form, double ampl, double dcoffset, double freq, double dutycycle) //задаёт выходное напряжение и оганичение по току
    { Q_UNUSED(form) Q_UNUSED(ampl) Q_UNUSED(dcoffset) Q_UNUSED(freq) Q_UNUSED(dutycycle) return Bugs(); }

    Bugs putLoadImpedance(double i)
    { Q_UNUSED(i) return Bugs(); }

    bool onOffState{false};

    QPointer<QSettings> m_settings;
    int m_nSlot;

};

class HsdioEmulator
{
public:
    HsdioEmulator(int nSlot, const QPointer<QSettings> sett):
        m_settings(sett),
        m_nSlot(nSlot)
    {   }

    static const int pin_count = 8;

    Bugs initPin(int pin_idx, Hsdio::PinState ps);
    Bugs init4Slots(Hsdio::LogicVoltage lv);
    Bugs put(int pin_idx, bool up);
    Bugs get(int pin_idx, bool * up);

    QPointer<QSettings> m_settings;
    int m_nSlot;
};

class ThermocoupleEmulator
{
public:
    ThermocoupleEmulator(int nSlot, const QPointer<QSettings> sett):
        m_settings(sett),
        m_nSlot(nSlot)
    {   }

    Bugs get(double * result);

    QPointer<QSettings> m_settings;
    int m_nSlot;
};

class RelayEmulator
{
public:
    RelayEmulator(int nSlot, const QPointer<QSettings> sett):
        m_settings(sett),
        m_nSlot(nSlot)
    {   }

    static const int count = 8;

    Bugs toDIO(int local_idx);
    Bugs toSMU(int local_idx);

    QPointer<QSettings> m_settings;
    int m_nSlot;
};

class SampleEmulator
{
public:
    SampleEmulator(int nSlot, const QPointer<QSettings> sett);

    QList<PowerEmulator *> power_chanels;
    SMUEmulator * smu = nullptr;
    FGenEmulator * fgen = nullptr;
    HsdioEmulator * hsdio = nullptr;
    ThermocoupleEmulator * tcouple = nullptr;
    RelayEmulator * relay = nullptr;
    Bugs neutral();

    PowerEmulator * power(int ch);

    QPointer<QSettings> m_settings;
    int m_nSlot;
};

class PxiEmulator : public QObject
{
    Q_OBJECT

public:
    PxiEmulator(QObject * parent, const QString & selfPath);
    Bugs init();
    void close();
    SampleEmulator * sample(int slot);

    QList<SampleEmulator *> samples;

    const QString m_selfPath;
    QPointer<QSettings> m_settings;
};

#endif // PXIEMULATOR_H
