#include <QSslError>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>
#include <QUrl>
#include "NetworkInterface.h"
#include "model/SlotsListModel.h"

struct ChamberMainParametersData;

NetworkInterface::NetworkInterface(QObject *parent) : QObject(parent)
{
    m_socket.reset(new QLocalSocket());
    connect(m_socket.data(), &QLocalSocket::connected, [this]() {
        qDebug() << "connect to service";
        connect(m_socket.data(), &QLocalSocket::readyRead, [this]() {
            //qDebug() << "socket readyRead";
            //qDebug() << m_socket->readAll();
            socketHandler();
        });

        connect(m_socket.data(), &QLocalSocket::disconnected, []() {
            qDebug() << "socket disconnected";
        });
    });

    m_disconnectTimer.reset(new QTimer());
    connect(m_disconnectTimer.data(), &QTimer::timeout, [this]() {
        if (m_socket->state() == QLocalSocket::ConnectedState)
            return;

        m_socket->connectToServer(PIPE_NAME);
    });
    m_disconnectTimer->start(1000);
}

bool NetworkInterface::sendStateRequest()
{
    if (m_socket->state() != QLocalSocket::ConnectedState)
        return false;

    QByteArray request;
    request.append(static_cast<char>(STATE_REQUEST));
    request.append("\0\0\0\0", LENGTH_OF_PAYLOAD);
    m_socket->write(request);
    m_socket->waitForBytesWritten();
    return true;
}

bool NetworkInterface::sendRunCyclogramCommand(const QUrl & fileUrl, const quint32 nSlot)
{
    if (m_socket->state() != QLocalSocket::ConnectedState)
        return false;

    QFile file( fileUrl.toLocalFile() );
    if (!file.open(QIODevice::ReadOnly))
        return false;

    QByteArray command;
    QByteArray jsonCyclogram(file.readAll());
    quint32 length = jsonCyclogram.size() + NSLOT_LENGTH;
    file.close();

    command.append(static_cast<char>(RUN_CYCLOGRAM_COMMAND));
    command.append(reinterpret_cast<char *>(&length), LENGTH_OF_PAYLOAD);
    command.append(reinterpret_cast<const char *>(&nSlot), NSLOT_LENGTH);
    command.append(jsonCyclogram);

    m_socket->write(command);
    m_socket->waitForBytesWritten();
    return true;
}

bool NetworkInterface::sendBreakCyclogramCommand(const quint32 nSlot)
{
    if (m_socket->state() != QLocalSocket::ConnectedState)
        return false;

    QByteArray command;
    quint32 length = NSLOT_LENGTH;
    command.append(static_cast<char>(BREAK_CYCLOGRAM_COMMAND));
    command.append(reinterpret_cast<char *>(&length), LENGTH_OF_PAYLOAD);
    command.append(reinterpret_cast<const char *>(&nSlot), NSLOT_LENGTH);

    m_socket->write(command);
    m_socket->waitForBytesWritten();
    return true;
}

bool NetworkInterface::sendRackTemperatureCommand()
{
    if (m_socket->state() != QLocalSocket::ConnectedState)
        return false;

    auto model = SlotsListModel::getInstance();
    if (model->get_chamberMainParameters().size() < 9)
        return false;

    float chamberParamTempHH = model->get_chamberMainParameters().at(0).value<ChamberMainParametersData>().value().toFloat();
    float chamberParamTempLL = model->get_chamberMainParameters().at(1).value<ChamberMainParametersData>().value().toFloat();
    quint32 chamberParamAutostop = model->get_chamberMainParameters().at(2).value<ChamberMainParametersData>().value().toBool();
    float chamberParamTempAutostop = model->get_chamberMainParameters().at(3).value<ChamberMainParametersData>().value().toFloat();
    float chamberParamRackSP = model->get_chamberMainParameters().at(4).value<ChamberMainParametersData>().value().toFloat();
    float chamberParamRackHH = model->get_chamberMainParameters().at(5).value<ChamberMainParametersData>().value().toFloat();
    float chamberParamRackH = model->get_chamberMainParameters().at(6).value<ChamberMainParametersData>().value().toFloat();
    float chamberParamRackL = model->get_chamberMainParameters().at(7).value<ChamberMainParametersData>().value().toFloat();
    float chamberParamRackLL = model->get_chamberMainParameters().at(8).value<ChamberMainParametersData>().value().toFloat();

    QByteArray command;
    quint32 length = 9 * REAL_PARAM_LENGTH;
    command.append(static_cast<char>(SET_RACK_TEMPERATURE_COMMAND));
    command.append(reinterpret_cast<char *>(&length), LENGTH_OF_PAYLOAD);
    command.append(reinterpret_cast<const char *>(&chamberParamTempHH), REAL_PARAM_LENGTH);
    command.append(reinterpret_cast<const char *>(&chamberParamTempLL), REAL_PARAM_LENGTH);
    command.append(reinterpret_cast<const char *>(&chamberParamAutostop), REAL_PARAM_LENGTH);
    command.append(reinterpret_cast<const char *>(&chamberParamTempAutostop), REAL_PARAM_LENGTH);
    command.append(reinterpret_cast<const char *>(&chamberParamRackSP), REAL_PARAM_LENGTH);
    command.append(reinterpret_cast<const char *>(&chamberParamRackHH), REAL_PARAM_LENGTH);
    command.append(reinterpret_cast<const char *>(&chamberParamRackH), REAL_PARAM_LENGTH);
    command.append(reinterpret_cast<const char *>(&chamberParamRackL), REAL_PARAM_LENGTH);
    command.append(reinterpret_cast<const char *>(&chamberParamRackLL), REAL_PARAM_LENGTH);

    m_socket->write(command);
    m_socket->waitForBytesWritten();
    return true;
}

bool NetworkInterface::sendAlarmResetCommand()
{
    if (m_socket->state() != QLocalSocket::ConnectedState)
        return false;

    QByteArray request;
    request.append(static_cast<char>(ALARM_RESET_COMMAND));
    request.append("\0\0\0\0", LENGTH_OF_PAYLOAD);
    m_socket->write(request);
    m_socket->waitForBytesWritten();
    return true;
}

bool NetworkInterface::sendLedColorCommand(LedColor color)
{
    if (m_socket->state() != QLocalSocket::ConnectedState)
        return false;

    QByteArray command;
    quint32 length = LEDCOLOR_LENGTH;
    command.append(static_cast<char>(SET_LED_COLOR_COMMAND));
    command.append(reinterpret_cast<char *>(&length), LENGTH_OF_PAYLOAD);
    command.append(reinterpret_cast<const char *>(&color), LEDCOLOR_LENGTH);

    m_socket->write(command);
    m_socket->waitForBytesWritten();
    return true;
}

bool NetworkInterface::sendPauseCommand()
{
    if (m_socket->state() != QLocalSocket::ConnectedState)
        return false;

    QByteArray request;
    request.append(static_cast<char>(PAUSE_COMMAND));
    request.append("\0\0\0\0", LENGTH_OF_PAYLOAD);
    m_socket->write(request);
    m_socket->waitForBytesWritten();
    return true;
}

bool NetworkInterface::sendResumeCommand()
{
    if (m_socket->state() != QLocalSocket::ConnectedState)
        return false;

    quint32 parameter = 0;

    QByteArray command;
    quint32 length = NSLOT_LENGTH;
    command.append(static_cast<char>(RESUME_COMMAND));
    command.append(reinterpret_cast<char *>(&length), LENGTH_OF_PAYLOAD);
    command.append(reinterpret_cast<const char *>(&parameter), PARAMETER_LENGTH);

    m_socket->write(command);
    m_socket->waitForBytesWritten();
    return true;
}

void NetworkInterface::socketHandler()
{
    //! Format specification: 1byte(command) + 4byte(length of payload) + Nbyte(payload)
    qint64 availabeBytes = m_socket->bytesAvailable();
    while (availabeBytes >= HEADER_LENGTH) {
        if (!m_socketFlag) {
            m_socket->getChar(reinterpret_cast<char *>(&m_socketCommand));
            QByteArray lenBA = m_socket->read(LENGTH_OF_PAYLOAD);
            m_socketCommandLength = *(reinterpret_cast<quint32 *>(lenBA.data()));
            m_socketFlag = true;
            availabeBytes -= HEADER_LENGTH;
        }

        if (availabeBytes < m_socketCommandLength)
            return;

        QByteArray meat;
        switch(m_socketCommand)
        {
        case StateResponse:
            meat = m_socket->read(m_socketCommandLength);
            stateResponseHandler(meat);
            cleanSocketCommand();
            break;
        default:
            meat = m_socket->read(m_socketCommandLength);
            cleanSocketCommand();
        }

        availabeBytes = m_socket->bytesAvailable();
    }
}

void NetworkInterface::stateResponseHandler(const QByteArray &meat)
{
    QJsonParseError error;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(meat, &error);
    if (error.error != QJsonParseError::NoError)
        qDebug() << "config error: " << error.errorString() << " at " << error.offset;

    QJsonObject rootObj = jsonDoc.object();

    SlotsListModel::getInstance()->updateModel(rootObj);

    emit serviceAlive();
}

void NetworkInterface::cleanSocketCommand()
{
    m_socketFlag = false;
    m_socketCommandLength = 0;
    m_socketCommand = 0;
}
