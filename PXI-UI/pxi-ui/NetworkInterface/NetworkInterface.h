#ifndef NETWORKINTERFACE_H
#define NETWORKINTERFACE_H

#include <QObject>
#include <QLocalSocket>
#include <QTimer>
#include "../../PXI-common/common.h"
#include "../qttricks/qqmlhelpers.h"
#include "../../PXI-common/common-enumerations.h"

class NetworkInterface : public QObject
{
    Q_OBJECT
    SINGLETON(NetworkInterface)

    enum SocketCommandType
    {
        NoCommand = NO_COMMAND,
        StateResponse = STATE_RESPONSE
    };

public:
    explicit NetworkInterface(QObject *parent = nullptr);

    enum LedColor
    {
        NoLed = LED_NO_COLOR,
        WhiteLed = LED_WHITE_COLOR,
        BlueLed = LED_BLUE_COLOR,
        GreenLed = LED_GREED_COLOR,
        YellowLed = LED_YELLOW_COLOR,
        RedLed = LED_RED_COLOR,
        BlinkRedLed = LED_BLINK_RED_COLOR
    };
    Q_ENUM(LedColor)

signals:
    void serviceAlive();

public slots:
    bool sendStateRequest();
    bool sendRunCyclogramCommand(const QUrl &fileUrl, const quint32 nSlot);
    bool sendBreakCyclogramCommand(const quint32 nSlot);
    bool sendRackTemperatureCommand();    
    bool sendAlarmResetCommand();
    bool sendLedColorCommand(LedColor color);
    bool sendPauseCommand();
    bool sendResumeCommand();

private:
    QScopedPointer<QLocalSocket> m_socket;
    QScopedPointer<QTimer> m_disconnectTimer;

    bool m_socketFlag{false};
    quint8 m_socketCommand{0};
    quint32 m_socketCommandLength{0};

    void socketHandler();
    void stateResponseHandler(const QByteArray &meat);
    void cleanSocketCommand();

};

#endif // NETWORKINTERFACE_H
