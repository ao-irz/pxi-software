import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtCharts 2.3
import PXIUIApplication 1.0
import "SimpleControls"
import "ScreenComponents"

Component {    

    Item {
        width: detailedScreensItem.width
        height: detailedScreensItem.height

        property bool isTestStopped: (model.stateRole === SlotsListModel.AlarmSlotState) || (model.stateRole === SlotsListModel.HardwareErrorSlotState)
        property var mapStates: (new Map([
            [ SlotsListModel.AlarmSlotState,         qsTr("ОСТАНОВЛЕН")],
            [ SlotsListModel.HardwareErrorSlotState, qsTr("ОСТАНОВЛЕН")],
            [ SlotsListModel.TestCompletedSlotState, qsTr("ЗАВЕРШЕН")],
            [ SlotsListModel.PauseSlotState,         qsTr("НА ПАУЗЕ")],
            [ SlotsListModel.NormalSlotState,        qsTr("ВЫПОЛНЯЕТСЯ")],
        ]))
        function getStateString(stateId) {
            if (mapStates.has(stateId))
                return mapStates.get(stateId)
            return ""
        }

        Rectangle {
            visible: !model.isActive
            anchors.fill: parent
            anchors.margins: 2
            color: "transparent"
            Text {
                anchors.centerIn: parent
                text: qsTr("пусто")
                font.pixelSize: 160
                color: "white"
                rotation: -35
            }
        }

        Page {
            visible: model.isActive
            anchors.fill: parent
            background:  Rectangle {
                anchors.fill: parent
                anchors.margins: 2
                border.color: "white"
                border.width: 2
                color: "transparent"
            }

            Column {
                anchors.fill: parent
                anchors.margins: 2
                spacing: 2

                Rectangle { height: 10; width: parent.width; color: "transparent" }

                UIProgressBar {
                    visible: (rootItem.state === "detailed")
                    anchors.margins: 10
                    anchors.left: parent.left
                    anchors.right: parent.right
                }

                Row {
                    anchors.left: parent.left
                    anchors.margins: 10
                    width: parent.width
                    spacing: 10

                    Grid {                        
                        width: parent.width / 2
                        columns: 3
                        columnSpacing: 20

                        UINameValueGray{ name: qsTr("Время загрузки: "); value: model.dateInput; pixelSize: 15 }
                        Rectangle { width: 20; height: 20; color: "transparent" }
                        Rectangle { width: 20; height: 20; color: "transparent" }

                        UINameValueGray{ name: qsTr("Время выгрузки: "); value: model.dateOutput; pixelSize: 15 }
                        Rectangle { width: 20; height: 20; color: "transparent" }
                        Rectangle { width: 20; height: 20; color: "transparent" }

                        UINameValueGray{ name: qsTr("Время остановки: "); value: model.emergencyStopTime; pixelSize: 15; valueColor: "red"; visible: isTestStopped }
                        Rectangle { width: 20; height: 20; color: "transparent"; visible: isTestStopped }
                        Rectangle { width: 20; height: 20; color: "transparent"; visible: isTestStopped }

                        UINameValueGray{ name: qsTr("Температура объекта: "); value: model.chipTemperature; units: "°"; pixelSize: 15}
                        UINameValueGray{ name: qsTr("Датчик: "); value: model.chipTemperatureSensorOk; pixelSize: 15; valueColor: (model.chipTemperatureSensorOk) ? ("green") : ("red") }
                        UINameValueGray{ name: qsTr("T(max): "); value: model.chipTemperatureMax; units: "°"; pixelSize: 15}
                    }
                }

                Row {
                    anchors.left: parent.left
                    anchors.margins: 10
                    width: parent.width
                    spacing: 10

                    Column { // эта column только для самого repeater
                        width: parent.width / 2
                        Repeater {
                            anchors.left: parent.left
                            anchors.leftMargin: 10
                            width: parent.width
                            model: powerSupplyCount

                            Column {
                                height: 70
                                width: parent.width

                                UIChamberParagraph { title: qsTr("Источник питания №") + powerSupplyNumber[model.index]; }
                                Item { width: parent.width; height: 10; }
                                Row {
                                    height: 20
                                    spacing: 30
                                    UINameValueGray{ name: qsTr("V:"); value: voltageArray[model.index]; units: qsTr("В"); pixelSize: 15 }
                                    UINameValueGray{ name: qsTr("V(min): "); value: voltageMinArray[model.index]; units: qsTr("В"); pixelSize: 15 }
                                    UINameValueGray{ name: qsTr("V(max): "); value: voltageMaxArray[model.index]; units: qsTr("В"); pixelSize: 15 }
                                }
                                Row {
                                    height: 20
                                    spacing: 30
                                    UINameValueGray{ name: qsTr("I :"); value: currentArray[model.index]; units: qsTr("А"); pixelSize: 15 }
                                    UINameValueGray{ name: qsTr("I(min): "); value: currentMinArray[model.index]; units: qsTr("А"); pixelSize: 15 }
                                    UINameValueGray{ name: qsTr("I(max): "); value: currentMaxArray[model.index]; units: qsTr("А"); pixelSize: 15 }
                                }
                            }
                        }
                    }

                    Column {
                        width: parent.width / 2 - 10

                        Column {
                            width: parent.width
                            height: 110

                            UIChamberParagraph { title: qsTr("Уставки генератора") }
                            Item { width: parent.width; height: 10; }
                            UINameValueGray{ name: qsTr("Амплитуда:"); value: model.generatorAmplitude; pixelSize: 15; height: 20 }
                            UINameValueGray{ name: qsTr("Частота:"); value: model.generatorFrequency; units: "Гц"; pixelSize: 15; height: 20 }
                            UINameValueGray{ name: qsTr("Рабочий цикл:"); value: model.generatorDutycycle; pixelSize: 15; height: 20 }
                            UINameValueGray{ name: qsTr("Импеданс:"); value: model.generatorImpedance; pixelSize: 15; height: 20 }
                        }

                        Column {
                            width: parent.width
                            height: 70

                            UIChamberParagraph { title: qsTr("DIO") }
                            Item { width: parent.width; height: 10; }
                            UINameValueGray{ name: qsTr("Ok:"); value: model.dioOk; pixelSize: 15; height: 20 }
                            UINameValueGray{ name: qsTr("V(логики):"); value: model.dioVoltageLogic; units: "Гц"; pixelSize: 15; height: 20 }
                        }

                        Column {
                            width: parent.width
                            height: 70

                            UIChamberParagraph { title: qsTr("СМУ") }
                            Item { width: parent.width; height: 10; }
                            UINameValueGray{ name: qsTr("V:"); value: model.smuCurrent; units: qsTr("В"); pixelSize: 15; height: 20}
                            UINameValueGray{ name: qsTr("I:"); value: model.smuVoltage; units: qsTr("А");  pixelSize: 15; height: 20 }
                        }
                    }
                }

                //! TODO: Видимо придется убрать в Popup
                /*Item { width: 500; height: 10; }
                UIDetailedCharts {
                    anchors.left: parent.left
                    anchors.margins: 10
                    width: parent.width / 2 - 10
                    height: 110
                }*/
            }

            header: Column {
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.leftMargin: 12
                anchors.topMargin: 12


                Row {
                    width: detailedScreensItem.width-20
                    Text {
                        width: parent.width/2
                        font.pixelSize: 20
                        font.bold: true
                        color: (isTestStopped) ? ("red") : (Material.accent)
                        text: qsTr("Слот № %1").arg(model.channelNumber+1)
                    }

                    Text {
                        width: parent.width/2
                        horizontalAlignment: Text.AlignRight
                        font.pixelSize: 20
                        font.bold: true
                        color: model.billetColor
                        text: getStateString(model.stateRole)
                    }
                }

                UINameValueGray{ name: qsTr("Наименование: "); value: model.name; pixelSize: 20 }
                UINameValueGray{ name: qsTr("Партия: "); value: model.batch; pixelSize: 20 }
                Rectangle {
                    border.color: "white"
                    border.width: 1
                    height: 2
                    width: parent.width - 24
                }
            }

            footer: Item {
                width: parent.width
                height: 50

                Button {
                    anchors.right: parent.right
                    anchors.rightMargin: 10
                    text: "Закончить"
                    onClicked: {
                        networkInterface.sendBreakCyclogramCommand(superIndex)
                        rootItem.state = "standard"
                    }
                }
            }
        }
    }
}
