import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Dialogs 1.2
import QtGraphicalEffects 1.0
import Qt.labs.settings 1.1
import PXIUIApplication 1.0
import "SimpleControls"
import "ScreenComponents"

ApplicationWindow {
    id: mainWindow

    visible: true
    //width: 1280
    //height: 768
    title: qsTr("PXI User Interface")
    //visibility: "FullScreen"
    visibility: "Maximized"
    flags: Qt.Dialog | Qt.FramelessWindowHint

    property Item detailedSlot
    property int superIndex: 0
    property date currentDateTime: new Date()

    onClosing: {
        close.accepted = false
        quitConfirmationWindow.open()
    }

    Component.onCompleted: {
        authWindow.open()
    }

    ListModel {
        id: loginList
        ListElement {text: qsTr("Пользователь")}
        ListElement {text: qsTr("Администратор")}
    }

    Settings {
        id: settings
        fileName: currentApplicationPath + "/pxi-ui-settings.ini"

        property string currentLogin: loginList.get(0).text
        property string adminPassword: Qt.md5("admin")
        property string userPassword: Qt.md5("user")
    }

    Timer {
        interval: 1000
        repeat: true
        running: true
        onTriggered: currentDateTime = new Date()
    }

    Timer {
        interval: 1000
        repeat: true
        running: true
        onTriggered: networkInterface.sendStateRequest()
    }

    Timer {
        id: noServiceWatchdog
        interval: 4000
        repeat: true
        running: true
        onTriggered: {
            if (!noServiceWindow.opened)
                noServiceWindow.open()
        }
    }

    Connections {
        target: networkInterface
        onServiceAlive: {
            noServiceWatchdog.restart()
            if (noServiceWindow.opened)
                noServiceWindow.close()
        }
    }

//    GaussianBlur {
//        visible: false
//        anchors.fill: parent
//        source: parent
//        deviation: 4
//        radius: 8
//        samples: 2
//    }

    UIImageButton {
        id: menuButton
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: 10
        toolTip: qsTr("Настройки")
        imageSource: "qrc:/images/menu-ui-pngrepo-com.png"
        buttonText: "menu"
        onClicked: settingsWindow.open()
    }

    /*UIImageButton {
        id: openCyclogramEditorButton
        imageSource: "qrc:/images/icon-2-algorithm-svg.png"
        anchors.right: menuButton.left
        anchors.top: parent.top
        anchors.margins: 10
        toolTip: qsTr("Открыть редактор циклограмм")
        onClicked: { console.log("Открывается редактор циклограмм") }
    }*/
    UIImageButton {
        id: openCyclogramEditorButton
        anchors.right: menuButton.left
        anchors.top: parent.top
        anchors.margins: 10
        toolTip: qsTr("Разрегистрироваться")
        imageSource: "qrc:/images/logout.png"
        onClicked: authWindow.open()
    }

    UIImageButton {
        imageSource: "qrc:/images/unnamed.png"
        anchors.right: openCyclogramEditorButton.left
        anchors.top: parent.top
        anchors.margins: 10
        toolTip: qsTr("Подробности")
        onClicked: { chamberDetailsWindow.open() }
    }

    UIImageButton {
        anchors.rightMargin: 10
        anchors.right: parent.right
        anchors.top: menuButton.bottom
        toolTip: qsTr("Технологические операции")
        imageSource: "qrc:/images/Renewal_Tracking.png"
        onClicked: operationWindow.open()
    }

    UISettingsWindow {
        id: settingsWindow
    }

    UIChamberDetailsWindow {
        id: chamberDetailsWindow
    }

    UINoServiceWindow {
        id: noServiceWindow
    }

    UIOperationsWindow {
        id: operationWindow
    }

    UIQuitConfirmationWindow {
        id: quitConfirmationWindow
    }

    UIAuthWindow {
        id: authWindow
    }

    Column {
        id: rootItem
        anchors.fill: parent
        focus: true
        state: "standard"

        Item {
            id: upperItem
            width: parent.width;
            height: parent.height/3

            Row {
                anchors.fill: parent
                anchors.leftMargin: 5

                Column {
                    width: parent.width/2;
                    anchors.verticalCenter: parent.verticalCenter

                    UINameValueGray { name: qsTr("Температура (t°):"); value: slotsListModel.mainTemperature; units: "°"; visible: slotsListModel.plcLink; pixelSize: 40; height: 50 }
                    UINameValueGray { name: qsTr("Состояние камеры:"); value: (slotsListModel.isThermalChamberOk) ? (qsTr("исправна")) : (qsTr("не исправна")); valueColor: (slotsListModel.isThermalChamberOk) ? ("green") : ("red"); visible: slotsListModel.plcLink }
                    UINameValueGray { name: qsTr("Текущее время:"); value: Qt.formatDateTime(currentDateTime, "yyyy-MM-dd hh:mm:ss") }
                    UINameValueGray { name: qsTr("Текущий пользователь:"); value: authWindow.currentLogin }
                    UINameValueGray { name: ""; value: qsTr("Нет связи с ПЛК камеры"); valueColor: "red"; visible: !slotsListModel.plcLink }
                }

                Grid {
                    visible: slotsListModel.plcLink
                    width: parent.width
                    anchors.verticalCenter: parent.verticalCenter
                    columns: 3
                    columnSpacing: 10

                    UINameValueGray{
                        name: qsTr("Режим камеры: ");
                        value: (slotsListModel.thermalChamberMode === SlotsListModel.ChamberManualMode) ?
                                   (qsTr("Включена")) : ((slotsListModel.thermalChamberMode === SlotsListModel.ChamberStandByMode) ? (qsTr("В ожидании")) : (qsTr("Аварийная остановка")));
                        valueColor: (slotsListModel.thermalChamberMode === SlotsListModel.ChamberManualMode) ?
                                        ("green") : ((slotsListModel.thermalChamberMode === SlotsListModel.ChamberStandByMode) ? ("lightblue") : ("red"));
                    }
                    Rectangle { width: 20; height: 20; color: "transparent" }
                    Rectangle { width: 20; height: 20; color: "transparent" }

                    UINameValueGray{ name: qsTr("Целевая температура: "); value: slotsListModel.thermalChamberTemperatureSet; units: "°" }
                    Rectangle { width: 20; height: 20; color: "transparent" }
                    Rectangle { width: 20; height: 20; color: "transparent" }

                    UINameValueGray{ name: qsTr("Ограничение: "); value: slotsListModel.thermalChamberTemperatureSpeed; units: "°/мин" }
                    Rectangle { width: 20; height: 20; color: "transparent" }
                    Rectangle { width: 20; height: 20; color: "transparent" }

                    UINameValueGray{ name: qsTr("Темп. стойки (сред.): "); value: slotsListModel.calculatedRackTemerature; }
                    Rectangle { width: 20; height: 20; color: "transparent" }
                    Rectangle { width: 20; height: 20; color: "transparent" }

                    UINameValueGray{ name: qsTr("Обороты вент.: "); value: slotsListModel.fanFrequency; units: "об/мин" }
                    UINameValueGray{ name: qsTr("Состояние: "); value: (/*slotsListModel.isFanOk*/true) ? (qsTr("исправен")) : (qsTr("не исправен")); valueColor: (/*slotsListModel.isFanOk*/true) ? ("green") : ("red") }
                    Rectangle { width: 20; height: 20; color: "transparent" }

                    UINameValueGray{ name: qsTr("Питание стойки:"); value: slotsListModel.ups220 ? ("220 В") : ("ИБП"); valueColor: (slotsListModel.ups220) ? ("green") : ("red") }
                    Rectangle { width: 20; height: 20; color: "transparent" }
                    Rectangle { width: 20; height: 20; color: "transparent" }
                }
            }
        }

        Row {
            width: parent.width
            height: parent.height/3*2

            Item {
                id: detailedScreensItem
                width: parent.width - slotsGrid.width
                height: slotsGrid.height

                ListView {
                    anchors.fill: parent
                    currentIndex: slotsGrid.currentIndex

                    interactive: true
                    clip: true
                    focus: true
                    snapMode: ListView.SnapOneItem
                    highlightRangeMode: ListView.StrictlyEnforceRange
                    boundsBehavior: Flickable.StopAtBounds
                    highlightMoveDuration: 200

                    model: slotsListModel
                    delegate:
                        UIDetailedScreen { }
                    onCurrentIndexChanged: {
                        slotsGrid.currentIndex = currentIndex
                    }
                }
            }

            GridView {
                id: slotsGrid
                width: parent.width
                height: parent.height
                boundsBehavior: Flickable.StopAtBounds
                model: slotsListModel
                delegate:
                    UISlot { }
            }
        }

        Keys.onPressed: {
            if (event.key === Qt.Key_Escape) {
                rootItem.state = "standard"
                event.accepted = true;
            }
        }

        states: [
            State {
                name: "detailed"
                PropertyChanges { target: upperItem; height: rootItem.height/4 }
                PropertyChanges { target: slotsGrid; height: rootItem.height/4*3; width: rootItem.width/(slotsListModel.amountOfSlots/2); cellHeight: height/slotsListModel.amountOfSlots; cellWidth: rootItem.width/(slotsListModel.amountOfSlots/2) }
            },
            State {
                name: "standard"
                PropertyChanges { target: upperItem; height: rootItem.height/3 }
                PropertyChanges { target: slotsGrid; height: rootItem.height/3*2; width: rootItem.width; cellHeight: height/2; cellWidth: width/(slotsListModel.amountOfSlots/2) }
            }
        ]

//        transitions: Transition {
//            ParallelAnimation {
//                PropertyAnimation {
//                    properties: "height,width"
//                    easing.type: Easing.Linear
//                    duration: 200;
//                }

////                PropertyAnimation {
////                    properties: "scale"
////                    easing.type: Easing.Linear
////                    duration: 200;
////                }
//            }
//        }
    }
}
