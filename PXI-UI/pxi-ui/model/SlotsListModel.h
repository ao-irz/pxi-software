#ifndef SLOTSLISTMODEL_H
#define SLOTSLISTMODEL_H

#include <QDebug>
#include <QAbstractListModel>
#include <QDateTime>
#include <QSharedPointer>
#include <QQmlListProperty>
#include <QVariantList>
#include "qttricks/qqmlhelpers.h"
#include "../../PXI-common/common.h"
#include "../../PXI-service/pxi-service/config.h"

// Фактически, это параметры, которые передаются в камеру отдельноый специальной командой "parameters"
struct ChamberMainParametersData
{
    Q_GADGET
    Q_PROPERTY(QString name READ name)
    Q_PROPERTY(QVariant value READ value)

public:
    ChamberMainParametersData() = default;
    ChamberMainParametersData(const QString & name, const QVariant & value) :
        m_name(name),
        m_value(value)
    {   }

    const QString & name() const
    { return m_name; }
    QVariant value() const
    { return m_value; }

    QString m_name;
    QVariant m_value;

};
Q_DECLARE_METATYPE(ChamberMainParametersData)

struct AlarmData
{
    Q_GADGET
    Q_PROPERTY(int moduleId READ moduleId)
    Q_PROPERTY(int instanceId READ instanceId)
    Q_PROPERTY(int errorCode READ errorCode)
    Q_PROPERTY(float parameter READ parameter)

public:
    AlarmData() = default;

    AlarmData(qint16 id, qint16 instanceNumber, qint16 errorCode, float param) :
        m_moduleId(id), m_instanceId(instanceNumber), m_errorCode(errorCode), m_parameter(param)
    {   }

    int moduleId() const
    { return m_moduleId; }
    int instanceId() const
    { return m_instanceId; }
    int errorCode() const
    { return m_errorCode; }
    float parameter() const
    { return m_parameter; }

    int m_moduleId;
    int m_instanceId;
    int m_errorCode;
    float m_parameter;
};
Q_DECLARE_METATYPE(AlarmData)

struct PeripheralTemperatureData
{
    Q_GADGET
    Q_PROPERTY(int number READ number)
    Q_PROPERTY(float temperature READ temperature)
    Q_PROPERTY(int errorCode READ errorCode)
    Q_PROPERTY(bool isOk READ isOk)
public:
    PeripheralTemperatureData() = default;

    PeripheralTemperatureData(qint16 number, float temp, int errorCode) :
        m_number(number), m_temperature(temp), m_errorCode(errorCode), m_isOk(errorCode == PLC_PERIPHERAL_SENSOR_STATUS_OK)
    {   }

    int number() const
    { return m_number; }
    float temperature() const
    { return m_temperature; }
    int errorCode() const
    { return m_errorCode; }
    bool isOk() const
    { return m_isOk; }

    int m_number;
    float m_temperature;
    int m_errorCode;
    bool m_isOk;
};
Q_DECLARE_METATYPE(PeripheralTemperatureData)

class SlotsListModel : public QAbstractListModel
{
    Q_OBJECT
    SINGLETON(SlotsListModel)

    QML_CONSTANT_PROPERTY(int, amountOfSlots)
    QML_READONLY_PROPERTY(int, pauseOn)                     // состояние паузы присуще каждому слоту и термокамере, а это как бы общий флаг

    QML_READONLY_PROPERTY(bool, plcLink)
    QML_READONLY_PROPERTY(int, thermoprofileState)

                                                            //! TODO: Вот из всех таких (типа systemInfoId) можно сделать вместо property Q_INVOKABLE
                                                            //! потому что нет никакого смысла их обновлять
                                                            //! TODO: все источники для производных property сделать не property-сами, а обычными полями
    QML_READONLY_PROPERTY(int, systemInfoId)
    QML_READONLY_PROPERTY(int, systemInfoVersion)
    QML_READONLY_PROPERTY(int, systemInfoErrorCode)
    QML_READONLY_PROPERTY(int, systemInfoDeviceVersion)
    QML_READONLY_PROPERTY(int, systemInfoHWVersion)
    QML_READONLY_PROPERTY(int, systemInfoSWVersion)
    QML_READONLY_PROPERTY(int, systemInfoSerialNumber)

    QML_READONLY_PROPERTY(int, thermalChamberId)
    QML_READONLY_PROPERTY(int, thermalChamberErrorCode)
    QML_READONLY_PROPERTY(bool, isThermalChamberOk)                 // производый от thermalChamberErrorCode
    QML_READONLY_PROPERTY(double, mainTemperature)          //! TEMPERATURE
    QML_READONLY_PROPERTY(double, thermalChamberSlidedTemperatureSet) // (по видимому речь идет об установленном параметре)
    QML_READONLY_PROPERTY(int, thermalChamberMode)
    QML_READONLY_PROPERTY(bool, isThermalChamberOn)                 // производый от thermalChamberMode
    QML_READONLY_PROPERTY(double, thermalChamberTemperatureSet)     // цеелвая температура
    QML_READONLY_PROPERTY(double, thermalChamberTemperatureSpeed)   // (по видимому речь идет об установленном ограничительном параметре)

    QML_READONLY_PROPERTY(QVariantList, chamberMainParameters)

    QML_READONLY_PROPERTY(int, accessoriesChamberId)
    QML_READONLY_PROPERTY(int, optionsChamberId)
    QML_READONLY_PROPERTY(int, alarmChamberId)

    QML_READONLY_PROPERTY(QVariantList, alarmDataList)
    //QML_READONLY_PROPERTY(QList<AlarmData *>, alarmDataList) // так и не смог запустить :(

    QML_READONLY_PROPERTY(int, extDI)
    QML_READONLY_PROPERTY(int, extDO)

    QML_READONLY_PROPERTY(QVariantList, peripheralTemperature)

    QML_READONLY_PROPERTY(double, calculatedRackTemerature) //! TEMPERATURE
    QML_READONLY_PROPERTY(double, fanFrequency)
    QML_READONLY_PROPERTY(bool, isFanOk)                            // не производный тип, просто сразу интерпретированный в bool

    QML_READONLY_PROPERTY(bool, ups220)                             // питание от 220В/питание от батарей UPS

    QML_READONLY_PROPERTY(bool, upsLink)
    QML_READONLY_PROPERTY(bool, cameraLink)

public:
    SlotsListModel(QObject *parent = nullptr);
    virtual ~SlotsListModel();

    // Обязаны быть переопределены
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;
    //int columnCount(const QModelIndex &parent) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    void updateModel(const QJsonObject &rootObject);

    void setPeripheralTemperature(const QJsonArray &peripheralArray);
    void setChamberMainParameters(const QJsonObject &rootObject);
    void setThermalChamberParameters(qint16 cameraOk, double mainTemp, double slideTempSet, qint16 mode, double tempSet, double tempSpeed);
    void setAlarms(const QJsonArray &alarmArray);
    void setExt(int extDO, int extDI);
    void setCalculatedRackTemerature(double temp);
    void setFan(qint16 fanOk, double freq);

    Q_INVOKABLE void setChamberParameterValue(int index, QVariant value);

    enum ChamberMode {
        ChamberStandByMode = PLC_STANDBY_MODE,
        ChamberManualMode = PLC_CHAMBER_MANUAL_MODE,
        ChamberAlarmMode = PLC_CHAMBER_ALARM_MODE
    };
    Q_ENUM(ChamberMode)

    enum ThermoprofileState { // нет смысла вводить новые define
        ThermoprofileNoState = NO_SLOT_STATE,
        ThermoprofileNormalState = NORMAL_SLOT_STATE,
        ThermoprofilePauseState = PAUSE_SLOT_STATE,
        ThermoprofileProblemState = ALARM_SLOT_STATE
    };
    Q_ENUM(ThermoprofileState)

    enum SlotState {
        NoSlotState = NO_SLOT_STATE,
        WaitingStartSlotState = WAITING_START_SLOT_STATE,
        HardwareErrorSlotState = HARDWARE_ERROR_SLOT_STATE,
        NormalSlotState = NORMAL_SLOT_STATE,
        PauseSlotState = PAUSE_SLOT_STATE,
        //WarningSlotState = WARNING_SLOT_STATE,
        AlarmSlotState = ALARM_SLOT_STATE,
        TestCompletedSlotState = TEST_COMPLETED_SLOT_STATE
    };
    Q_ENUM(SlotState)

    enum Column {
        SlotNumberRole = Qt::UserRole + 1,
        NameRole,
        BatchRole,
        DateInputRole,
        DateOutputRole,
        EmergencyStopTimeRole,

        PowerSupplyCount, // простое количество активных источников
        PowerSupplyNumber,  // массив номеров активных источниклв

        VoltageArrayRole,
        VoltageMinArrayRole,
        VoltageMaxArrayRole,
        CurrentArrayRole,
        CurrentMinArrayRole,
        CurrentMaxArrayRole,

        GeneratorAmplitudeRole,
        GeneratorFrequencyRole,
        GeneratorDutycycleRole,
        GeneratorImpedanceRole,

        DIOOkRole,
        DIOVoltageLogicRole,

        SMUCurrentRole,
        SMUVoltageRole,

        ChipTemperatureRole,                // приходит с pxi-thermocouple
        ChipTemperatureMaxRole,
        ChipTemperatureSensorOkRole,        //! TODO: пока не известно, будет это существовать или нет

        ColorRole,
        StateRole,

        IsActiveRole,
        EndOfColumnRole
    };
    Q_ENUM(Column)

    struct SlotData
    {
        SlotData(const quint32 slotNumber,
                 const QString &name,
                 const QString &batch,
                 const QDateTime &dateInput,
                 const QDateTime &dateOutput,
                 SlotState state) :
            slotNumber(slotNumber),
            name(name),
            batch(batch),
            dateInput(dateInput),
            dateOutput(dateOutput),
            emergencyStopTime(dateOutput), // ну как-то проиниц-ть надо
            state(state)
        {   }

        void setChipTemperature(double v, bool ok, double max)
        { isChipTemperatureValid = true; chipTemperature = v; chipTemperatureSensorOk = ok; chipTemperatureMax = max;}

        void setGeneratorInfo(double a, double f, double d, double i)
        { isGeneratorValid = true; generatorAmplitude = a; generatorFrequency = f; generatorDutycycle = d; generatorImpedance = i; }

        quint32 slotNumber;
        QString name;
        QString batch;
        QDateTime dateInput;
        QDateTime dateOutput;
        QDateTime emergencyStopTime;
        SlotState state;

        QList<int> powerSupplyNumberArray;

        QList<double> voltageArray;
        QList<double> voltageMinArray;
        QList<double> voltageMaxArray;
        QList<double> currentArray;
        QList<double> currentMinArray;
        QList<double> currentMaxArray;

        bool isGeneratorValid{false};
        double generatorAmplitude;
        double generatorFrequency;
        double generatorDutycycle;
        double generatorImpedance;

        bool isDioValid{false};
        bool dioOk;
        double dioVoltageLogic;

        bool isSmuValid{false};
        double smuCurrent;
        double smuVoltage;

        bool isChipTemperatureValid{false};
        double chipTemperature{0.0};
        double chipTemperatureMax{0.0};
        bool chipTemperatureSensorOk{true}; // не факт, что будет
    };


private:
    //QList<QSharedPointer<SlotData> > m_rows;
    QHash<quint32, QSharedPointer<SlotData> > m_rows;

    const QHash<SlotState, QString> m_colorTable;
};

#endif // SLOTSLISTMODEL_H
