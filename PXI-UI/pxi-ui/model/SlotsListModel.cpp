#include <math.h>
#include <QJsonArray>
#include <QJsonObject>
#include <QModelIndex>
#include <QRandomGenerator>
#include "SlotsListModel.h"
#include "../../PXI-service/pxi-service/config.h"

// Количество комплектов

SlotsListModel::SlotsListModel(QObject * parent) :
    QAbstractListModel(parent),
    m_amountOfSlots(AMOUNT_OF_SLOTS),
    m_plcLink(false),
    m_colorTable({
                 {NoSlotState, "#778899"},
                 {WaitingStartSlotState, "#90ee90" /*"#fff0f5"*/ /*"#ffd700"*/},
                 {HardwareErrorSlotState, "#ff0000"/*"#f08080"*/},
                 {NormalSlotState, "#32cd32"},
                 {PauseSlotState, "#d3d3d3"},
                 //{WarningSlotState, "#ff7f00"},
                 {AlarmSlotState, "#ff7f00"},
                 {TestCompletedSlotState, "#008b8b"}})
{
}

SlotsListModel::~SlotsListModel()
{
    m_rows.clear();
}

int SlotsListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return AMOUNT_OF_SLOTS;
}

QVariant SlotsListModel::data(const QModelIndex &index, int role) const
{
    if ((!index.isValid()) || (index.row() >= AMOUNT_OF_SLOTS))
        return QVariant();

    QSharedPointer<SlotData> emptySlot( new SlotData(index.row(), QStringLiteral(""), QStringLiteral(""), QDateTime::currentDateTime(),  QDateTime::currentDateTime(), NoSlotState) );

    switch(role)
    {
    case SlotNumberRole:
        return m_rows.value(index.row(), emptySlot)->slotNumber;
    case NameRole:
        return m_rows.value(index.row(), emptySlot)->name;
    case BatchRole:
        return m_rows.value(index.row(), emptySlot)->batch;
    case DateInputRole:
        return m_rows.value(index.row(), emptySlot)->dateInput.toString(DB_DATETIME_FORMAT);
    case DateOutputRole:
        return m_rows.value(index.row(), emptySlot)->dateOutput.toString(DB_DATETIME_FORMAT);
    case EmergencyStopTimeRole:
        return m_rows.value(index.row(), emptySlot)->emergencyStopTime.toString(DB_DATETIME_FORMAT);

    case PowerSupplyCount:
        return m_rows.value(index.row(), emptySlot)->powerSupplyNumberArray.size();
    case PowerSupplyNumber:
        return QVariant::fromValue(  m_rows.value(index.row(), emptySlot)->powerSupplyNumberArray  );

    case VoltageArrayRole:
        return QVariant::fromValue(  m_rows.value(index.row(), emptySlot)->voltageArray  );
    case VoltageMinArrayRole:
        return QVariant::fromValue(  m_rows.value(index.row(), emptySlot)->voltageMinArray  );
    case VoltageMaxArrayRole:
        return QVariant::fromValue(  m_rows.value(index.row(), emptySlot)->voltageMaxArray  );

    case CurrentArrayRole:
        return QVariant::fromValue(  m_rows.value(index.row(), emptySlot)->currentArray  );
    case CurrentMinArrayRole:
        return QVariant::fromValue(  m_rows.value(index.row(), emptySlot)->currentMinArray  );
    case CurrentMaxArrayRole:
        return QVariant::fromValue(  m_rows.value(index.row(), emptySlot)->currentMaxArray  );

    case GeneratorAmplitudeRole:
        return (m_rows.value(index.row(), emptySlot)->isGeneratorValid) ? (m_rows.value(index.row(), emptySlot)->generatorAmplitude) : (QVariant(QStringLiteral("n/a")));
    case GeneratorFrequencyRole:
        return (m_rows.value(index.row(), emptySlot)->isGeneratorValid) ? (m_rows.value(index.row(), emptySlot)->generatorFrequency) : (QVariant(QStringLiteral("n/a")));
    case GeneratorDutycycleRole:
        return (m_rows.value(index.row(), emptySlot)->isGeneratorValid) ? (m_rows.value(index.row(), emptySlot)->generatorDutycycle) : (QVariant(QStringLiteral("n/a")));
    case GeneratorImpedanceRole: {
        // сюда из сервиса будет прилетать либо 1000000.0 либо -1
        const QString &result((m_rows.value(index.row(), emptySlot)->generatorImpedance == DEFAULT_IMPEDANCE) ?
                                  QStringLiteral("высокоомный") :
                                  QStringLiteral("низкоомный"));
        return (m_rows.value(index.row(), emptySlot)->isGeneratorValid) ?
                    (result) :
                    (QVariant(QStringLiteral("n/a")));
    }
    case DIOOkRole:
        return (m_rows.value(index.row(), emptySlot)->isDioValid) ? (m_rows.value(index.row(), emptySlot)->dioOk) : (QVariant(QStringLiteral("n/a")));
    case DIOVoltageLogicRole:
        return (m_rows.value(index.row(), emptySlot)->isDioValid) ? (m_rows.value(index.row(), emptySlot)->dioVoltageLogic) : (QVariant(QStringLiteral("n/a")));

    case SMUCurrentRole:
        return (m_rows.value(index.row(), emptySlot)->isSmuValid) ? (m_rows.value(index.row(), emptySlot)->smuCurrent) : (QVariant(QStringLiteral("n/a")));
    case SMUVoltageRole:
        return (m_rows.value(index.row(), emptySlot)->isSmuValid) ? (m_rows.value(index.row(), emptySlot)->smuVoltage) : (QVariant(QStringLiteral("n/a")));

    case ChipTemperatureRole:
        return (m_rows.value(index.row(), emptySlot)->isChipTemperatureValid) ? (m_rows.value(index.row(), emptySlot)->chipTemperature) : (QVariant(QStringLiteral("n/a")));
    case ChipTemperatureMaxRole:
        return (m_rows.value(index.row(), emptySlot)->isChipTemperatureValid) ? (m_rows.value(index.row(), emptySlot)->chipTemperatureMax) : (QVariant(QStringLiteral("n/a")));
    case ChipTemperatureSensorOkRole:
        return (m_rows.value(index.row(), emptySlot)->isChipTemperatureValid) ?
                    ((m_rows.value(index.row(), emptySlot)->chipTemperatureSensorOk) ? (QVariant(QStringLiteral("исправен"))) : (QVariant(QStringLiteral("поврежден")))) :
                    (QVariant(QStringLiteral("n/a")));

    case ColorRole:
        return m_colorTable.value(m_rows.value(index.row(), emptySlot)->state);
    case StateRole:
        return m_rows.value(index.row(), emptySlot)->state;
    case IsActiveRole:
        return m_rows.contains(index.row());

    default:
        return QVariant();
    }

    return QVariant();
}

QHash<int, QByteArray> SlotsListModel::roleNames() const
{
    const QHash<int, QByteArray> roles
            ({
                 {SlotNumberRole, "channelNumber"},
                 {NameRole, "name"},
                 {BatchRole, "batch"},
                 {DateInputRole, "dateInput"},
                 {DateOutputRole, "dateOutput"},
                 {EmergencyStopTimeRole, "emergencyStopTime"},

                 {PowerSupplyCount, "powerSupplyCount"}, // простое количество активных источников
                 {PowerSupplyNumber, "powerSupplyNumber"}, // массив номеров активных источниклв

                 {VoltageArrayRole, "voltageArray"},
                 {VoltageMinArrayRole, "voltageMinArray"},
                 {VoltageMaxArrayRole, "voltageMaxArray"},

                 {CurrentArrayRole, "currentArray"},
                 {CurrentMinArrayRole, "currentMinArray"},
                 {CurrentMaxArrayRole, "currentMaxArray"},

                 {GeneratorAmplitudeRole, "generatorAmplitude"},
                 {GeneratorFrequencyRole, "generatorFrequency"},
                 {GeneratorDutycycleRole, "generatorDutycycle"},
                 {GeneratorImpedanceRole, "generatorImpedance"},

                 {DIOOkRole, "dioOk"},
                 {DIOVoltageLogicRole, "dioVoltageLogic"},

                 {SMUCurrentRole, "smuCurrent"},
                 {SMUVoltageRole, "smuVoltage"},

                 {ChipTemperatureRole, "chipTemperature"},
                 {ChipTemperatureMaxRole, "chipTemperatureMax"},
                 {ChipTemperatureSensorOkRole, "chipTemperatureSensorOk"},
                 {ColorRole, "billetColor"},
                 {StateRole, "stateRole"},
                 {IsActiveRole, "isActive"}
             });
    return roles;
}

// Для того что бы Charts отображали правильную "легенду"
QVariant SlotsListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(orientation)
    Q_UNUSED(role)
    return data( createIndex(section, 0), SlotNumberRole );
}

void SlotsListModel::updateModel(const QJsonObject &rootObject)
{
    //beginResetModel();
    m_rows.clear();
    for (auto i = rootObject.value("Slots").toArray().constBegin(); i != rootObject.value("Slots").toArray().constEnd(); ++i) {
        QSharedPointer<SlotData> insertedData(new SlotData(
                (*i).toObject().value("slotNumber").toInt(),
                (*i).toObject().value("name").toString(),
                (*i).toObject().value("batch").toString(),
                QDateTime::fromString( (*i).toObject().value("dateInput").toString(), DB_DATETIME_FORMAT),
                QDateTime::fromString( (*i).toObject().value("dateOutput").toString(), DB_DATETIME_FORMAT),
                (SlotState)(*i).toObject().value("state").toInt()));

        if ( !(*i).toObject().value("emergencyStopTime").isUndefined() )
            insertedData->emergencyStopTime = QDateTime::fromString( (*i).toObject().value("emergencyStopTime").toString(), DB_DATETIME_FORMAT);

        for (const auto & ps : (*i).toObject().value("powerSupplies").toArray()) {
            if (!ps.toObject().value("powerSupplyNumber").isUndefined())
                insertedData->powerSupplyNumberArray.append(ps.toObject().value("powerSupplyNumber").toInt());

            if (!ps.toObject().value("voltage").isUndefined()) {
                insertedData->voltageArray.append(ps.toObject().value("voltage").toDouble());
                insertedData->voltageMinArray.append(ps.toObject().value("voltageMin").toDouble());
                insertedData->voltageMaxArray.append(ps.toObject().value("voltageMax").toDouble());
            }

            if (!ps.toObject().value("current").isUndefined()) {
                insertedData->currentArray.append(ps.toObject().value("current").toDouble());
                insertedData->currentMinArray.append(ps.toObject().value("currentMin").toDouble());
                insertedData->currentMaxArray.append(ps.toObject().value("currentMax").toDouble());
            }
        }

        if (!(*i).toObject().value("generatorAmplitude").isUndefined()) {
            insertedData->setGeneratorInfo((*i).toObject().value("generatorAmplitude").toDouble(),
                                           (*i).toObject().value("generatorFrequency").toDouble(),
                                           (*i).toObject().value("generatorDutycycle").toDouble(),
                                           (*i).toObject().value("generatorImpedance").toDouble());
        } else
            insertedData->isGeneratorValid = false;

        if (!(*i).toObject().value("dioOk").isUndefined()) {
            insertedData->dioOk = (*i).toObject().value("dioOk").toBool();
            insertedData->dioVoltageLogic = (*i).toObject().value("dioVoltageLogic").toBool();
        } else
            insertedData->isDioValid = false;

        if (!(*i).toObject().value("smuCurrent").isUndefined()) {
            insertedData->smuCurrent = (*i).toObject().value("smuCurrent").toDouble();
            insertedData->smuVoltage = (*i).toObject().value("smuVoltage").toDouble(); // "0 Вольт" будет означать, что это режим без подачи напряжения
            insertedData->isSmuValid = true;
        } else
            insertedData->isSmuValid = false;

        if (!(*i).toObject().value("chipTemperature").isUndefined()) {
            insertedData->setChipTemperature((*i).toObject().value("chipTemperature").toDouble(),
                                             (*i).toObject().value("chipTemperatureSensorOk").toBool(),
                                             (*i).toObject().value("chipTemperatureMax").toDouble());
        } else
            insertedData->isChipTemperatureValid = false;

        m_rows.insert((*i).toObject().value("slotNumber").toInt(), insertedData);

//        qDebug() << (*i).toObject().value("slotNumber").toInt() << " - " << (*i).toObject().value("state").toInt();
//        qDebug() << "name" << m_rows.last()->name;
//        qDebug() << "batch" << m_rows.last()->batch;
//        qDebug() << "DateInput" << m_rows.last()->dateInput;
//        qDebug() << "V" << m_rows.last()->voltage;
//        qDebug() << "I" << m_rows.last()->current;
//        qDebug() << "temp" << m_rows.last()->temperature;
//        qDebug() << "------------------------------";
    }

    update_pauseOn( rootObject.value("pauseOn").toBool() );
    update_plcLink( rootObject.value("plcLink").toBool() );
    update_thermoprofileState( rootObject.value("thermoprofileState").toInt() );

//    set_chamberParamTempHH(rootObject.value("chamberParamTempHH").toDouble());
//    set_chamberParamTempLL(rootObject.value("chamberParamTempLL").toDouble());
//    set_chamberParamAutostop(rootObject.value("chamberParamAutostop").toBool());
//    set_chamberParamTempAutostop(rootObject.value("chamberParamTempAutostop").toDouble());
//    set_chamberParamRackSP(rootObject.value("chamberParamRackSP").toDouble());
//    set_chamberParamRackHH(rootObject.value("chamberParamRackHH").toDouble());
//    set_chamberParamRackH(rootObject.value("chamberParamRackH").toDouble());
//    set_chamberParamRackL(rootObject.value("chamberParamRackL").toDouble());
//    set_chamberParamRackLL(rootObject.value("chamberParamRackLL").toDouble());
    setChamberMainParameters(rootObject);

    if (m_plcLink) {
        update_plcLink(true);

        setAlarms(rootObject.value("modulesAlarms").toArray());

        setExt(rootObject.value("extDI").toInt(), rootObject.value("extDO").toInt());
        setCalculatedRackTemerature(rootObject.value("calculatedRackTemerature").toDouble());
        setFan(rootObject.value("fanErrorCode").toInt(), rootObject.value("fanFrequency").toDouble());

        //! Остальная куча параметров UPS сервисом пробрасывается, но здесь за ненадобностью не парсится
        update_ups220( !(rootObject.value("bitsStatusUPS").toInt() & 0x0012) );

        update_upsLink( (bool)rootObject.value("upsLink").toInt() );
        update_cameraLink( (bool)rootObject.value("cameraLink").toInt() );


        setThermalChamberParameters(
                    rootObject.value("thermalChamberErrorCode").toInt(),
                    rootObject.value("mainTemperature").toDouble(),
                    rootObject.value("thermalChamberSlidedTemperatureSet").toDouble(),
                    rootObject.value("thermalChamberMode").toInt(),
                    rootObject.value("thermalChamberTemperatureSet").toDouble(),
                    rootObject.value("thermalChamberTemperatureSpeed").toDouble());


        setPeripheralTemperature(rootObject.value("peripheralTemperature").toArray());
    }
    emit dataChanged(createIndex(0, 0), createIndex(AMOUNT_OF_SLOTS, 0));
    //endResetModel();
}

void SlotsListModel::setThermalChamberParameters(qint16 cameraOk, double mainTemp, double slideTempSet, qint16 mode, double tempSet, double tempSpeed)
{
    update_thermalChamberErrorCode(cameraOk);
    update_isThermalChamberOk(cameraOk == PLC_THERMAL_CHAMBER_ERROR_CODE_OK);
    update_mainTemperature(mainTemp);
    update_thermalChamberSlidedTemperatureSet(slideTempSet);
    update_thermalChamberMode(mode);
    update_isThermalChamberOn(mode == PLC_CHAMBER_MANUAL_MODE);
    update_thermalChamberTemperatureSet(tempSet);
    update_thermalChamberTemperatureSpeed(tempSpeed);
}

void SlotsListModel::setAlarms(const QJsonArray & alarmArray)
{
    m_alarmDataList.clear();

    for (auto i = alarmArray.constBegin(); i != alarmArray.constEnd(); ++i) {
        const QJsonObject & objectRef = (*i).toObject();
        m_alarmDataList.append(
                    QVariant::fromValue(AlarmData( objectRef.value("moduleId").toInt(),
                                                   objectRef.value("instanceNumber").toInt(),
                                                   objectRef.value("errorCode").toInt(),
                                                   objectRef.value("parameter").toDouble())));
    }

    emit alarmDataListChanged(m_alarmDataList);
}

void SlotsListModel::setExt(int extDO, int extDI)
{
    update_extDO(extDO);
    update_extDI(extDI);
}

void SlotsListModel::setCalculatedRackTemerature(double temp)
{
    update_calculatedRackTemerature(temp);
}

void SlotsListModel::setFan(qint16 fanOk, double freq)
{
    update_fanFrequency(freq);
    update_isFanOk(fanOk == PLC_FAN_STATUS_OK);
}

void SlotsListModel::setChamberParameterValue(int index, QVariant value)
{
    ((ChamberMainParametersData *)(m_chamberMainParameters[index].data()))->m_value = value;

    // тоже нельзя здесь посылать сигнал, потому что обновлять в цикле в qml будет много параметров
    //emit chamberMainParametersChanged(m_chamberMainParameters);
}

void SlotsListModel::setPeripheralTemperature(const QJsonArray &peripheralArray)
{
    m_peripheralTemperature.clear();

    for (auto i = peripheralArray.constBegin(); i != peripheralArray.constEnd(); ++i) {
        const QJsonObject & objectRef = (*i).toObject();
        m_peripheralTemperature.append(
                    QVariant::fromValue(PeripheralTemperatureData(
                                            objectRef.value("number").toInt(),
                                            objectRef.value("temperature").toDouble(),
                                            objectRef.value("sensorOk").toInt())));
    }

    emit peripheralTemperatureChanged(m_peripheralTemperature);
}

void SlotsListModel::setChamberMainParameters(const QJsonObject &rootObject)
{
    m_chamberMainParameters.clear();
    m_chamberMainParameters.append(QVariant::fromValue(ChamberMainParametersData("Уставка защиты по перегреву", rootObject.value("chamberParamTempHH").toDouble())));
    m_chamberMainParameters.append(QVariant::fromValue(ChamberMainParametersData("Уставка защиты по переохлаждению", rootObject.value("chamberParamTempLL").toDouble())));
    m_chamberMainParameters.append(QVariant::fromValue(ChamberMainParametersData("Разрешение останова холодильника", rootObject.value("chamberParamAutostop").toBool())));
    m_chamberMainParameters.append(QVariant::fromValue(ChamberMainParametersData("Температура отключения холодильника", rootObject.value("chamberParamTempAutostop").toDouble())));
    m_chamberMainParameters.append(QVariant::fromValue(ChamberMainParametersData("Температура стойки (для вентилятора)", rootObject.value("chamberParamRackSP").toDouble())));
    m_chamberMainParameters.append(QVariant::fromValue(ChamberMainParametersData("Задание порога HH температуры стойки", rootObject.value("chamberParamRackHH").toDouble())));
    m_chamberMainParameters.append(QVariant::fromValue(ChamberMainParametersData("Задание порога H температуры стойки", rootObject.value("chamberParamRackH").toDouble())));
    m_chamberMainParameters.append(QVariant::fromValue(ChamberMainParametersData("Задание порога L температуры стойки", rootObject.value("chamberParamRackL").toDouble())));
    m_chamberMainParameters.append(QVariant::fromValue(ChamberMainParametersData("Задание порога LL температуры стойки", rootObject.value("chamberParamRackLL").toDouble())));

    // не посылакть сигнал обновления, потому что тогда окно Settings постоянно будет возвращаться к показу текущего значения
    //emit chamberMainParametersChanged(m_chamberMainParameters);
}
