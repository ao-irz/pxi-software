import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Component {
    Item {
        width: slotsGrid.cellWidth
        height: slotsGrid.cellHeight
        scale: (rootItem.state === "detailed") ? ((GridView.isCurrentItem) ? (0.9) : (1.0)) : 1.0

        ItemDelegate {
            checkable: true
            anchors.fill: parent
            anchors.margins: 5
            background: Rectangle {
                radius: 10
                border.color: "gray"
                gradient: Gradient {
                        orientation: Gradient.Horizontal
                        GradientStop { position: 0; color: billetColor }
                        GradientStop { position: 1; color: "white" }
                }

                Text {
                    anchors.centerIn: parent
                    opacity: 0.5
                    font.pixelSize: parent.height * 0.7
                    text: index+1
                }
            }

            UISlotContent { }

            onClicked: {
                //if (!model.isActive)
                //    return;

                if (superIndex === index) {
                    (rootItem.state === "standard") ? (rootItem.state = "detailed") : (rootItem.state = "standard");
                }
                else {
                    superIndex = index;
                    slotsGrid.currentIndex = index
                    rootItem.state = "standard";
                    rootItem.state = "detailed";
                }
            }
        }

        Behavior on scale {
            PropertyAnimation {
                duration: 100
            }
        }
    }
}
