﻿import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Dialogs 1.2
import "../SimpleControls"
import "../ScreenComponents"

Item {
    anchors.fill: parent
    anchors.margins: 10

    Column {
        anchors.fill: parent
        visible: model.isActive

        UINameValue { value: model.name; pixelSize: (rootItem.state === "standard") ? 20 : 16 }
        Rectangle { width: parent.width; height: 2; visible: (rootItem.state === "standard")}
        UINameValue { name: "Загрузка:"; value: model.dateInput; pixelSize: 16 }
        UINameValue { name: "Выгрузка:"; value: model.dateOutput; pixelSize: 16; visible: (rootItem.state === "standard") }
        UINameValue { name: "Термопара:"; value: model.chipTemperature; pixelSize: 16; visible: (rootItem.state === "standard") }

        UIProgressBar {
            width: parent.width
            visible: (rootItem.state === "standard")
        }

        Item {
            visible: (rootItem.state === "standard") && (!model.isActive)
            height: parent.height / 4
            width: parent.width
        }
    }

    FileDialog {
        id: openDialog
        nameFilters: ["Text files (*.json)"]
        onAccepted: networkInterface.sendRunCyclogramCommand(openDialog.fileUrl, model.channelNumber)
    }

    Button {
        visible: (rootItem.state === "standard")
                 && (!model.isActive)
                 && slotsListModel.isThermalChamberOk
                 && slotsListModel.isThermalChamberOn
                 && (!slotsListModel.pauseOn)
                 && slotsListModel.plcLink
                 && Math.abs(slotsListModel.mainTemperature - slotsListModel.thermalChamberTemperatureSet) < 2
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        text: "Открыть циклограмму"
        onClicked: openDialog.open()
    }
}
