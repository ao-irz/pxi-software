import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Item {
    width: parent.width
    height: 17
    property string title

    Column {
        width: parent.width-20
        spacing: 2

        Text{ id: textItem; text: title; font.bold: true; font.pixelSize: 16; color: "gray"; }
        Rectangle { width: parent.width; height: 2; border.color: "white" }
    }
}
