import QtQuick 2.0
import QtCharts 2.0

Rectangle {    
//    anchors.fill: parent
//    width: parent.width
//    height: parent.height
    //width: 600
    //height: 400

    ChartView {
        id: chartView
        title: "Диаграмма"
        height: parent.height / 4 * 3
        anchors.fill: parent
        legend.alignment: Qt.AlignRight
        antialiasing: true
        backgroundColor: "transparent"

        BarCategoryAxis {
            id: barCategoriesAxis
            titleText: "Date"
        }

        ValueAxis{
            id: valueAxisY2
            min: 0
            max: 10
            titleText: "Current [Amper]"
        }

        ValueAxis {
            id: valueAxisX
            visible: false
            min: 0
            max: 5
        }

        ValueAxis{
            id: valueAxisY
            min: 0
            max: 15
            titleText: "Temperature [&deg;C]"
        }

        LineSeries {
            id: maxTempSeries
            axisX: valueAxisX
            axisY: valueAxisY
            name: "Max. temperature"
        }

        LineSeries {
            id: minTempSeries
            axisX: valueAxisX
            axisY: valueAxisY
            name: "Min. temperature"
            XYPoint { x: 0; y: 0 }
            XYPoint { x: 1.1; y: 2.1 }
            XYPoint { x: 1.9; y: 3.3 }
            XYPoint { x: 2.1; y: 2.1 }
            XYPoint { x: 2.9; y: 4.9 }
            XYPoint { x: 3.4; y: 3.0 }
            XYPoint { x: 4.1; y: 3.3 }
        }

        BarSeries {
            id: myBarSeries
            axisX: barCategoriesAxis
            axisYRight: valueAxisY2
            BarSet {
                id: rainfallSet
                label: "Current"
                values: [5, 0, 1, 5]
            }
        }
    }

}
