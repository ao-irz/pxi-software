#include <QDebug>
#include <QApplication>
#include <QQmlContext>
#include <QQmlApplicationEngine>
#include <QDir>
#include "NetworkInterface/NetworkInterface.h"
#include "model/SlotsListModel.h"
#include "../../PXI-common/led-color.h"
#include "../../PXI-common/common-enumerations.h"

int main(int argc, char *argv[])
{
    printf("Starting program...\n");
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);
    app.setOrganizationName("IRZ");
    app.setOrganizationDomain("irz.ru");
    app.setApplicationName("pxi-ui");

    //! TODO: Возможно все emum вынести в отдельный Namespace, хотя будет ли это работать....
    qmlRegisterUncreatableType<NetworkInterface>("PXIUIApplication", 1, 0, "NetworkInterface", "Not creatable as it is an enum type");
    qmlRegisterUncreatableType<SlotsListModel>("PXIUIApplication", 1, 0, "SlotsListModel", "Not creatable as it is an enum type");

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("networkInterface", NetworkInterface::getInstance(qobject_cast<QObject *>(&app)));
    engine.rootContext()->setContextProperty("slotsListModel", SlotsListModel::getInstance(qobject_cast<QObject *>(&app)));
    engine.rootContext()->setContextProperty("currentApplicationPath", QString(QDir::currentPath()));

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
