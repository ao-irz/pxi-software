import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12 as QtQuickControl
import QtQuick.Controls.Material 2.12
import QtQuick.Dialogs 1.2
import PXIUIApplication 1.0
import "SimpleControls"
import "ScreenComponents"

QtQuickControl.Dialog {
    id: root    
    width: parent.width/2
    height: parent.height/3*2
    title: qsTr("Технологические операции")
    modal: true
    anchors.centerIn: parent
    standardButtons: Dialog.Close

    property var thermoprofileStates: (new Map([
        [ SlotsListModel.ThermoprofileNoState,      qsTr("Не загружена")],
        [ SlotsListModel.ThermoprofileNormalState,  qsTr("Работает")],
        [ SlotsListModel.ThermoprofilePauseState,   qsTr("На пауза")],
        [ SlotsListModel.ThermoprofileProblemState, qsTr("Ошибка")],
    ]))
    property var thermoprofileStatesColor: (new Map([
        [ SlotsListModel.ThermoprofileNoState,      qsTr("gray")],
        [ SlotsListModel.ThermoprofileNormalState,  qsTr("green")],
        [ SlotsListModel.ThermoprofilePauseState,   qsTr("gray")],
        [ SlotsListModel.ThermoprofileProblemState, qsTr("red")],
    ]))

    function getStateString(stateId) {
        if (thermoprofileStates.has(stateId))
            return thermoprofileStates.get(stateId)
        return "неизвестное состояние"
    }
    function getStateColorString(stateId) {
        if (thermoprofileStatesColor.has(stateId))
            return thermoprofileStatesColor.get(stateId)
        return qsTr("gray")
    }

    FileDialog {
        id: openDialog
        nameFilters: ["Text files (*.json)"]
        onAccepted: networkInterface.sendRunCyclogramCommand(openDialog.fileUrl, slotsListModel.amountOfSlots)
    }

    QtQuickControl.ScrollView {
        anchors.fill: parent
        clip: true

        Column {
            anchors.fill: parent
            spacing: 10
            Item { width: root.width-60; height: 1; }

            UIChamberParagraph { title: qsTr("Изъятие, замена, установка"); }
            UINameValueGray{ name: qsTr("Состояние тех. паузы:"); value: (slotsListModel.pauseOn) ? (qsTr("установлена пауза")) : (qsTr("нет")); valueColor: (slotsListModel.pauseOn) ? ("red") : ("green"); pixelSize: 15 }
            Row {
                spacing: 10
                QtQuickControl.Button {
                    enabled: (!slotsListModel.pauseOn)
                    text: qsTr("Собираюсь открыть крышку термокамеры")
                    onClicked: {
                        networkInterface.sendPauseCommand()
                    }
                }

                QtQuickControl.Button {
                    enabled: slotsListModel.pauseOn
                    text: qsTr("Можно продолжить тесты")
                    onClicked: {
                        networkInterface.sendResumeCommand()
                    }
                }
            }

            UIChamberParagraph { title: qsTr("Загрузка термо-циклограммы"); }
            UINameValueGray{ name: qsTr("Состояние термопрофиля: "); value: getStateString(slotsListModel.thermoprofileState); valueColor: getStateColorString(slotsListModel.thermoprofileState); pixelSize: 15 }
            Row {
                width: parent.width
                spacing: 10

                QtQuickControl.Button {
                    enabled: (!slotsListModel.pauseOn) && (slotsListModel.thermalChamberMode === SlotsListModel.ChamberStandByMode)
                    text: qsTr("Загрузить термо-циклограмму")
                    onClicked: {
                        openDialog.open()
                    }
                }

                QtQuickControl.Button {
                    enabled: (slotsListModel.thermoprofileState != SlotsListModel.ThermoprofileNoState)
                    text: qsTr("Завершить")
                    onClicked: {
                        networkInterface.sendBreakCyclogramCommand(slotsListModel.amountOfSlots)
                    }
                }
            }

        }
    }
}
