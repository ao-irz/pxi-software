import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "SimpleControls"
import "ScreenComponents"

Dialog {
    id: root
    width: parent.width/2
    height: parent.height/3*2
    title: qsTr("Подробности")
    modal: true
    anchors.centerIn: parent
    standardButtons: Dialog.Close

    ScrollView {
        anchors.fill: parent
        clip: true

        Column {
            anchors.fill: parent
            spacing: 10

            Item { width: root.width-60; height: 1; }
            UINameValueGray { name: ""; value: qsTr("Нет связи с ПЛК камеры"); valueColor: "red"; visible: !slotsListModel.plcLink }
            UIChamberParagraph { title: qsTr("Системная информация"); }
            Grid {
                visible: slotsListModel.plcLink
                width: parent.width
                columns: 2
                columnSpacing: 15

                UINameValueGray{ name: qsTr("ID:"); value: slotsListModel.systemInfoId; pixelSize: 15}
                Rectangle { width: 20; height: 20; color: "transparent" }

                UINameValueGray{ name: qsTr("Версия модуля:"); value: slotsListModel.systemInfoVersion; pixelSize: 15 }
                Rectangle { width: 20; height: 20; color: "transparent" }

                UINameValueGray{ name: qsTr("Модель устройства:"); value: slotsListModel.systemInfoDeviceVersion; pixelSize: 15}
                Rectangle { width: 20; height: 20; color: "transparent" }

                UINameValueGray{ name: qsTr("Версия аппартного обеспечения:"); value: slotsListModel.systemInfoHWVersion; pixelSize: 15}
                Rectangle { width: 20; height: 20; color: "transparent" }

                UINameValueGray{ name: qsTr("Версия программого обеспечения:"); value: slotsListModel.systemInfoSWVersion; pixelSize: 15 }
                Rectangle { width: 20; height: 20; color: "transparent" }

                UINameValueGray{ name: qsTr("Серийный номер устройства:"); value: slotsListModel.systemInfoSerialNumber; pixelSize: 15 }
                Rectangle { width: 20; height: 20; color: "transparent" }
            }

            UIChamberParagraph { title: qsTr("Периферийные температурные датчики"); }
            ListView {
                visible: slotsListModel.plcLink
                width: parent.width
                height: slotsListModel.peripheralTemperature.length * 40
                spacing: 10
                model: slotsListModel.peripheralTemperature
                delegate: Grid {
                    width: parent.width
                    columns: 3
                    columnSpacing: 15

                    UINameValueGray{ name: qsTr("Датчик №:") + modelData.number; value: modelData.temperature; units: "°"; pixelSize: 15}
                    UINameValueGray{ name: qsTr("Состояние:"); value: (modelData.isOk) ? (qsTr("исправен")) : (qsTr("не исправен")); valueColor: (modelData.isOk) ? ("green") : ("red"); pixelSize: 15 }
                    Rectangle { width: 20; height: 20; color: "transparent" }
                }
            }

            UIChamberParagraph { title: qsTr("Информация о термокамере"); }
            Grid {
                visible: slotsListModel.plcLink
                width: parent.width
                columns: 2
                columnSpacing: 15

                UINameValueGray{ name: qsTr("ID:"); value: slotsListModel.thermalChamberId; pixelSize: 15 }
                Rectangle { width: 20; height: 20; color: "transparent" }

                UINameValueGray{ name: qsTr("Режим:"); value: slotsListModel.thermalChamberMode; pixelSize: 15}
                Rectangle { width: 20; height: 20; color: "transparent" }

                UINameValueGray{ name: qsTr("Состояние:"); value: slotsListModel.thermalChamberErrorCode; pixelSize: 15}
                Rectangle { width: 20; height: 20; color: "transparent" }

                UINameValueGray{ name: qsTr("Температура:"); value: slotsListModel.mainTemperature; units: "°"; pixelSize: 15}
                Rectangle { width: 20; height: 20; color: "transparent" }

                UINameValueGray{ name: qsTr("Температурная установка:"); value: slotsListModel.thermalChamberTemperatureSet; units: "°"; pixelSize: 15}
                Rectangle { width: 20; height: 20; color: "transparent" }

                UINameValueGray{ name: qsTr("Ограничение скорости изменения температуры:"); value: slotsListModel.thermalChamberTemperatureSpeed; units: "об/мин"; pixelSize: 15}
                Rectangle { width: 20; height: 20; color: "transparent" }
            }

            UIChamberParagraph { title: qsTr("Прочие модули"); }
            Grid {
                visible: slotsListModel.plcLink
                width: parent.width
                columns: 2
                columnSpacing: 15

                UINameValueGray{ name: qsTr("Accessories ID:"); value: slotsListModel.accessoriesChamberId; pixelSize: 15 }
                Rectangle { width: 20; height: 20; color: "transparent" }

                UINameValueGray{ name: qsTr("Options ID:"); value: slotsListModel.optionsChamberId; pixelSize: 15 }
                Rectangle { width: 20; height: 20; color: "transparent" }

                UINameValueGray{ name: qsTr("Alarm ID:"); value: slotsListModel.alarmChamberId; pixelSize: 15 }
                Rectangle { width: 20; height: 20; color: "transparent" }
            }

            UIChamberParagraph { title: qsTr("Дискретные входы/выходы"); }
            Grid {
                visible: slotsListModel.plcLink
                width: parent.width
                columns: 2
                columnSpacing: 15

                UINameValueGray{ name: qsTr("Выход:"); value: slotsListModel.extDI; pixelSize: 15 }
                Rectangle { width: 20; height: 20; color: "transparent" }

                UINameValueGray{ name: qsTr("Вход:"); value: slotsListModel.extDO; pixelSize: 15 }
                Rectangle { width: 20; height: 20; color: "transparent" }
            }

            UIChamberParagraph { title: qsTr("Информация о стойке"); }
            Grid {
                visible: slotsListModel.plcLink
                width: parent.width
                columns: 2
                columnSpacing: 15

                UINameValueGray{ name: qsTr("Усредненная температура стойки:"); value: slotsListModel.calculatedRackTemerature; units: "°"; pixelSize: 15 }
                Rectangle { width: 20; height: 20; color: "transparent" }
            }

            UIChamberParagraph { title: qsTr("Вентилятор"); }
            Grid {
                visible: slotsListModel.plcLink
                width: parent.width
                columns: 2
                columnSpacing: 15

                UINameValueGray{ name: qsTr("Обороты:"); value: slotsListModel.fanFrequency; units: "об/мин"; pixelSize: 15}
                UINameValueGray{ name: qsTr("Состояние:"); value: (slotsListModel.isFanOk) ? (qsTr("исправен")) : (qsTr("не исправен")); valueColor: (slotsListModel.isFanOk) ? ("green") : ("red"); pixelSize: 15 }
            }


            UIChamberParagraph { title: qsTr("Питание"); }
            Grid {
                visible: slotsListModel.plcLink
                width: parent.width
                columns: 2
                columnSpacing: 15

                UINameValueGray{ name: qsTr("Питание стойки:"); value: slotsListModel.ups220 ? ("220 В") : ("ИБП"); valueColor: (slotsListModel.ups220) ? ("green") : ("red"); pixelSize: 15}
            }

            UIChamberParagraph { title: qsTr("Связь"); }
            Grid {
                visible: slotsListModel.plcLink
                width: parent.width
                columns: 2
                columnSpacing: 15

                UINameValueGray{ name: qsTr("Связь с ИБП:"); value: slotsListModel.upsLink ? ("есть") : ("нет"); valueColor: (slotsListModel.upsLink) ? ("green") : ("red"); pixelSize: 15}
                UINameValueGray{ name: qsTr("Связь с камерой:"); value: slotsListModel.cameraLink ? ("есть") : ("нет"); valueColor: (slotsListModel.cameraLink) ? ("green") : ("red"); pixelSize: 15}
            }

            //! TODO: Перенести, возможно, в отдельное окно. Сделать расшифровку имен модулей
            UIChamberParagraph { title: qsTr("Аварии модулей"); }
            ListView {
                visible: slotsListModel.plcLink
                width: parent.width
                height: slotsListModel.alarmDataList.length * 40
                spacing: 10
                model: slotsListModel.alarmDataList
                delegate: Grid {
                    width: parent.width
                    columns: 3
                    columnSpacing: 15
                    UINameValueGray{ name: qsTr("Модуль:"); value: modelData.moduleId; pixelSize: 15}
                    UINameValueGray{ name: qsTr("Код:"); value: modelData.errorCode; pixelSize: 15}
                    UINameValueGray{ name: qsTr("Параметер:"); value: modelData.parameter; pixelSize: 15}
                }
            }
        }
    }
}
