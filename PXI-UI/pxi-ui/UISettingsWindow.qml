import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import PXIUIApplication 1.0
//import Qt.labs.settings 1.1
import "SimpleControls"
import "ScreenComponents"

Dialog {
    id: root
    width: parent.width/2
    height: parent.height/3*2
    title: qsTr("Настройки")
    modal: true
    anchors.centerIn: parent
    standardButtons: Dialog.Cancel | Dialog.Ok

    onVisibleChanged: {
        if (visible) {
            // Обновить модель, послать сигнал
            slotsListModel.chamberMainParametersChanged(slotsListModel.chamberMainParameters)
            infoText.color = "transparent"
            infoText.text = " "
            passTextField.clear()
            confirmPassTextField.clear()
        }
    }

    onAccepted: {
        for (let i=0; i<slotsListModel.chamberMainParameters.length; ++i)
            slotsListModel.setChamberParameterValue(i, parametersRepeater.itemAt(i).valueText)

        networkInterface.sendRackTemperatureCommand()
        // посылать сигнал не имеет смысл, потому что окно закрывается
        //slotsListModel.chamberMainParametersChanged(slotsListModel.chamberMainParameters)
    }

    ScrollView {
        anchors.fill: parent
        clip: true

        Column {
            anchors.fill: parent
            spacing: 10
            Item { width: root.width-60; height: 1; }

            UINameValueGray { name: ""; value: qsTr("Нет связи с ПЛК камеры"); valueColor: "red"; visible: !slotsListModel.plcLink }
            UIChamberParagraph { title: qsTr("Аварии"); }
            ListView {
                visible: slotsListModel.plcLink
                width: parent.width
                height: slotsListModel.alarmDataList.length * 40
                spacing: 10
                model: slotsListModel.alarmDataList
                delegate: Grid {
                    width: parent.width
                    columns: 3
                    columnSpacing: 15
                    UINameValueGray{ name: qsTr("Модуль:"); value: modelData.moduleId; pixelSize: 15}
                    UINameValueGray{ name: qsTr("Код:"); value: modelData.errorCode; pixelSize: 15}
                    UINameValueGray{ name: qsTr("Параметер:"); value: modelData.parameter; pixelSize: 15}
                }
            }

            Button {
                text: qsTr("Сбросить аварии")
                onClicked: networkInterface.sendAlarmResetCommand()
            }

            UIChamberParagraph { title: qsTr("Подстветка"); }
            Grid {
                visible: slotsListModel.plcLink
                width: parent.width
                columns: 3
                columnSpacing: 15
                Button {
                    text: qsTr("Нет")
                    onClicked: networkInterface.sendLedColorCommand(NetworkInterface.NoLed)
                }
                Button {
                    text: qsTr("Белый")
                    onClicked: networkInterface.sendLedColorCommand(NetworkInterface.WhiteLed)
                }
                Button {
                    text: qsTr("Синий")
                    onClicked: networkInterface.sendLedColorCommand(NetworkInterface.BlueLed)
                }

                Button {
                    text: qsTr("Зеленый")
                    onClicked: networkInterface.sendLedColorCommand(NetworkInterface.GreenLed)
                }

                Button {
                    text: qsTr("Желтый")
                    onClicked: networkInterface.sendLedColorCommand(NetworkInterface.YellowLed)
                }
                Button {
                    text: qsTr("Красный")
                    onClicked: networkInterface.sendLedColorCommand(NetworkInterface.RedLed)
                }
                Button {
                    text: qsTr("Моргающий красный")
                    onClicked: networkInterface.sendLedColorCommand(NetworkInterface.BlinkRedLed)
                }

            }

            UIChamberParagraph { title: qsTr("Параметры термокамеры"); }

            Repeater {
                id: parametersRepeater
                model: slotsListModel.chamberMainParameters
                width: parent.width

                Row {
                    property alias valueText: valueField.text
                    width: parent.width
                    spacing: 20
                    Label { font.pixelSize: 16; anchors.verticalCenter: parent.verticalCenter; text: modelData.name; width: 300; height: 40; }
                    TextField {
                        id: valueField
                        height: 40;
                        //anchors.right: parent.right
                        anchors.verticalCenter: parent.verticalCenter;
                        color: "white"
                        selectByMouse: true
                        text: (typeof modelData.value === 'number') ?
                                   (modelData.value.toFixed(2)) :
                                   (modelData.value)
                    }
                }
            }

            UIChamberParagraph { title: qsTr("Смена пароля пользователя"); visible: authWindow.isAdminActive}

            Grid {
                visible: authWindow.isAdminActive
                verticalItemAlignment: Grid.AlignVCenter
                width: parent.width
                columns: 2
                columnSpacing: 15

                Text {
                    font.pixelSize: 15
                    color: "white"
                    text: qsTr("Пароль")
                }

                TextField {
                    id: passTextField
                    echoMode: TextInput.Password
                    selectByMouse: true
                }

                Text {
                    font.pixelSize: 15
                    color: "white"
                    text: qsTr("Подтверждение")
                }

                TextField {
                    id: confirmPassTextField
                    echoMode: TextInput.Password
                    selectByMouse: true
                }
            }

            Text {
                id: infoText
                color: "transparent"
                text: " "
                font.pixelSize: 15
            }

            Button {
                visible: authWindow.isAdminActive
                text: qsTr("Сохранить пароль")
                onClicked: {
                    if (passTextField.text === confirmPassTextField.text) {
                        infoText.color = "green"; infoText.text = qsTr("Новый пароль пользователя сохранен")
                        settings.userPassword = Qt.md5(passTextField.text)
                        settings.sync()
                    } else {
                        infoText.color = "red"; infoText.text = qsTr("Пароли не совпадают")
                    }
                }
            }
        }
    }
}
