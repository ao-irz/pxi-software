import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
//import Qt.labs.settings 1.1
import "SimpleControls"
import "ScreenComponents"

Dialog {
    id: __rootItem

    property real windowWidth: 500
    property real windowHeight: 300
    property alias currentLogin: loginComboBox.currentText
    property bool isAdminActive: (!__rootItem.visible) && (loginComboBox.currentText === loginList.get(1).text)

    width: 0
    height: 0
    padding: 0
    modal: true
    closePolicy: Popup.NoAutoClose
    x: parent.width/2 - windowWidth/2
    y: parent.height/2 - windowHeight/2
    focus: true

    onVisibleChanged: {
        if (visible) {
            passwordTextField.clear()
        }
    }

    Rectangle {
        opacity: 0.9
        width: windowWidth
        height: windowHeight
        color: Material.background
        radius: 10
        //border.color: "white"

        ColumnLayout {
            anchors.fill: parent

            Text {
                font.pixelSize: 20
                Layout.alignment: Qt.AlignCenter
                color: "white"
                text: qsTr("Авторизация")
            }

            Text {
                id: incorrectPasswordText
                color: "transparent"
                Layout.alignment: Qt.AlignCenter
                font.pixelSize: 20
                text: qsTr("Неверный пароль")
            }

            Grid {
                id: gridLayout
                property real layoutMargins: 20

                columns: 2
                rows: 2
                spacing: 10
                Layout.fillWidth: true
                Layout.margins: layoutMargins
                Layout.alignment: Qt.AlignVCenter
                verticalItemAlignment: Grid.AlignVCenter

                Text {
                    id: loginHit
                    font.pixelSize: 20
                    color: "lightgray"
                    text: qsTr("Логин")
                }

                ComboBox {
                    id: loginComboBox
                    opacity: 0.8
                    width: parent.width - loginHit.width - gridLayout.spacing - gridLayout.layoutMargins
                    model: loginList
                    Component.onCompleted: {
                        currentIndex = loginComboBox.find( settings.currentLogin )
                    }
                }

                Text {
                    id: passHit
                    font.pixelSize: 20
                    color: "lightgray"
                    text: qsTr("Пароль")
                }

                TextField {
                    id: passwordTextField
                    width: parent.width - loginHit.width - gridLayout.spacing - gridLayout.layoutMargins
                    echoMode: TextInput.Password
                    selectByMouse: true
                    focus: true
                    Keys.onPressed: {
                        if (event.key === Qt.Key_Return)
                        { okButton.clicked() }
                    }
                }
            }


            Button {
                id: okButton
                text: qsTr("ok")
                Layout.alignment: Qt.AlignCenter
                onClicked: {
                    settings.currentLogin = loginComboBox.currentText
                    var str = ""
                    if (loginComboBox.currentText === loginList.get(0).text)
                        str = settings.userPassword
                    else if (loginComboBox.currentText === loginList.get(1).text)
                        str = settings.adminPassword

                    if (Qt.md5(passwordTextField.text) === str) {
                        incorrectPasswordText.color = "transparent"
                        accept()
                    }
                    else {
                        incorrectPasswordText.color = "red"
                        //reject()
                    }
                }
            }
        }
    }
}
